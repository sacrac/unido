from django.db import models
from django.contrib.auth.models import User
from cooperativas.models import Cooperativa
from .constants_coop import ( CHOICES_CICLO,
                              CHOICES_PERSONAL,
                              CHOICES_PERSONAL_PAGADO,
                              CHOICES_PERSONAL_PAGADO_COOP,
                              CHOICES_PROCESOS,
                              CHOICES_SOCIOS_PRODUCCION,
                              CHOICES_PROVEEDORES_PRODUCCION,
                              CHOICES_PLAN_NEGOCIO,
                              CHOICES_RENTABILIDAD,
                              CHOICES_RENTABILIDAD_ORGANICO,
                              CHOICES_CUMPLIMIENTO,
                              CHOICES_AVANCE_PLANES,
                              CHOICES_CICLO_12,
                              CHOICES_COBERTURA_COOP)

from catalogos.models import Evaluador, TemasCapacitaciones, ProductosCoop

# Create your models here.

class FichaAnualCooperativa(models.Model):
    cooperativa = models.ForeignKey(Cooperativa, on_delete=models.CASCADE)
    ciclo = models.IntegerField('Ciclo de evaluación', choices=CHOICES_CICLO)
    fecha = models.DateField('Fecha de evaluación')
    evaluador = models.ForeignKey(Evaluador, on_delete=models.CASCADE)

    year = models.IntegerField(editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.year = self.fecha.year
        super(FichaAnualCooperativa, self).save(*args, **kwargs)

    def __str__(self):
        return self.cooperativa.siglas

    class Meta:
        verbose_name = 'Ficha anual cooperativa'
        verbose_name_plural = 'Fichas anuales de cooperativas'
        ordering = ['cooperativa']


class PersonalTotal(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    personal = models.IntegerField(choices=CHOICES_PERSONAL)
    hombres = models.IntegerField('Cant. Hombres')
    mujeres = models.IntegerField('Cant. Mujeres')
    jovenes = models.IntegerField('Cant. H. Jóvenes')
    jovenas = models.IntegerField('Cant. M. Jóvenes')

    class Meta:
        verbose_name='1. Personal total'
        verbose_name_plural='1. Personal total'

class PersonalPagado(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    personal = models.IntegerField(choices=CHOICES_PERSONAL_PAGADO)
    hombres = models.IntegerField('Cant. Hombres')
    mujeres = models.IntegerField('Cant. Mujeres')
    jovenes = models.IntegerField('Cant. H. Jóvenes')
    jovenas = models.IntegerField('Cant. M. Jóvenes')

    class Meta:
        verbose_name='2. Personal pagado por PROCACAO II'
        verbose_name_plural='2. Personal pagado por PROCACAO II'

class PersonalPagadoCoop(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    personal = models.IntegerField(choices=CHOICES_PERSONAL_PAGADO_COOP)
    hombres = models.IntegerField('Cant. Hombres')
    mujeres = models.IntegerField('Cant. Mujeres')
    jovenes = models.IntegerField('Cant. H. Jóvenes')
    jovenas = models.IntegerField('Cant. M. Jóvenes')

    class Meta:
        verbose_name='3. Personal pagado por la Cooperativa'
        verbose_name_plural='3. Personal pagado por la Cooperativa'

class Procesos(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    procesos = models.IntegerField(choices=CHOICES_PROCESOS)
    semanal = models.IntegerField()
    quincenal = models.IntegerField()
    mensual = models.IntegerField()
    trimestral = models.IntegerField()
    semestral = models.IntegerField()
    anual = models.IntegerField()
    evanto_anual = models.IntegerField('Cant. de eventos realizado durante el año')


    class Meta:
        verbose_name='4. Proceso'
        verbose_name_plural='4. Procesos'


class Capacitaciones(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    temas = models.ForeignKey(TemasCapacitaciones, on_delete=models.CASCADE)
    cantidad= models.IntegerField('Número de personas capacitadas (fuente registros)')

    class Meta:
        verbose_name='5. Temas de capacitaciones'
        verbose_name_plural='5. temas de capacitaciones'


class ProdManejadoCoop(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    datos = models.ForeignKey(ProductosCoop, on_delete=models.CASCADE)
    socios = models.IntegerField('Número de socios involucrados')
    proveedores = models.IntegerField('Número de Proveedores involucrados')
    volumen = models.FloatField('Volumen de producto manejado durante el ciclo')

    class Meta:
        verbose_name='6.Productos  acopiados, procesados y comercializados por la Cooperativa'
        verbose_name_plural='6.Productos  acopiados, procesados y comercializados por la Cooperativa'


class ProduccionSocios(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    datos = models.IntegerField(choices=CHOICES_SOCIOS_PRODUCCION)
    ciclo = models.FloatField('Ciclo total')
    comercio_justo = models.FloatField('Ciclo certificado RA')
    convencional = models.FloatField('Ciclo certificado convencional')
    organico = models.FloatField('Ciclo certificado Orgánico')

    class Meta:
        verbose_name='7-1. Producción de cacao socios'
        verbose_name_plural='7-1. Producción de cacao socios'

class ProduccionProveedores(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    datos = models.IntegerField(choices=CHOICES_PROVEEDORES_PRODUCCION)
    ciclo = models.FloatField('Ciclo total')
    comercio_justo = models.FloatField('Ciclo certificado RA')
    convencional = models.FloatField('Ciclo certificado convencional')
    organico = models.FloatField('Ciclo certificado Orgánico')

    class Meta:
        verbose_name='7-2. Producción de cacao proveedores'
        verbose_name_plural='7-2. Producción de cacao proveedores'

class PlanNegocioCacao(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    datos = models.IntegerField(choices=CHOICES_PLAN_NEGOCIO)
    ciclo = models.FloatField('Ciclo total')
    certificado = models.FloatField('Ciclo certificado RA')
    convencional = models.FloatField('Ciclo convencional')

    class Meta:
        verbose_name='8. Plan de negocio de cacao'
        verbose_name_plural='8. Plan de negocio de cacao'

class RentabilidadFermentadoRA(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    datos = models.IntegerField(choices=CHOICES_RENTABILIDAD)
    ciclo = models.FloatField('Ciclo actual')

    class Meta:
        verbose_name='9. Rentabilidad cadena de cacao fermentado RA'
        verbose_name_plural='9. Rentabilidad cadena de cacao fermentado RA'


class RentabilidadFermentadoOrganico(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    datos = models.IntegerField(choices=CHOICES_RENTABILIDAD_ORGANICO)
    ciclo = models.FloatField('Ciclo actual')

    class Meta:
        verbose_name='10. Rentabilidad cadena de cacao fermentado Orgánico'
        verbose_name_plural='10. Rentabilidad cadena de cacao fermentado Orgánico'

class CumplimientoCoop(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    datos = models.IntegerField(choices=CHOICES_CUMPLIMIENTO)
    ciclo = models.FloatField('Ciclo actual')
    porcentaje = models.FloatField('%')

    class Meta:
        verbose_name='11. Cumplimiento de la Cooperativa'
        verbose_name_plural='11. Cumplimiento de la Cooperativa'

class AvancePlanesCoop(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    datos = models.IntegerField(choices=CHOICES_AVANCE_PLANES)
    ciclo = models.IntegerField(choices=CHOICES_CICLO_12, verbose_name="Ciclo actual")
    porcentaje = models.FloatField('%')

    class Meta:
        verbose_name='12. Avance de planes de la Cooperativa'
        verbose_name_plural='12. Avance de planes de la Cooperativa'

class CoberturaCoop(models.Model):
    ficha = models.ForeignKey(FichaAnualCooperativa, on_delete=models.CASCADE)
    datos = models.IntegerField(choices=CHOICES_COBERTURA_COOP)
    ciclo = models.FloatField('Ciclo actual')

    class Meta:
        verbose_name='13. Cobertura de la Cooperativa'
        verbose_name_plural='13. Cobertura de la Cooperativa'




        