

CHOICES_CICLO = (

        (1, '2020 (Enero - Diciembre)'),
        (2, '2021 (Enero - Diciembre)'),
        (3, '2022 (Enero - Diciembre)'),
        (4, '2023 (Enero - Diciembre)'),
        (5, '2024 (Enero - Diciembre)'),
        (6, '2025 (Enero - Diciembre)'),

    )


CHOICES_PERSONAL = (

        (1, 'Socios y socias'),
        (2, 'Proveedores'),
        (3, 'Trabajadores empleados en la coop'),
        (4, 'Miembros de la Junta Directiva'),
        (5, 'Miembros de la Junta de Vigilancia'),
        (6, 'Miembros de comité de educación y promoción al cooperativismo'),
        (7, 'Miembros de Comité de género'),
        (8, 'Miembros de Comité de comercialización'),
        (9, 'Gerentes'),
        (10, 'Contadores'),
        (11, 'Técnicos de campo'),
        (12, 'Acopiadores'),
        (13, 'Trabajadores Centro de Acopio'),
        (14, 'Trabajadores Procesamiento'),
        (15, 'Inspectores internos Certificación'),
        (16, 'Inspectores externos Certificación'),
        (17, 'Agentes de crédito'),
        (18, 'Prestadores de servicio'),

    )


CHOICES_PERSONAL_PAGADO = (

        (1, 'Gerentes'),
        (2, 'Contadores'),
        (3, 'Técnicos de campo'),
        (4, 'Acopiadores'),
        (5, 'Trabajadores Centro de Acopio'),
        (6, 'Inspectores internos Certificación'),
        (7, 'Inspectores externos Certificación'),
        (8, 'Agentes de crédito'),
        (9, 'Prestadores de servicio'),

    )


CHOICES_PERSONAL_PAGADO_COOP = (

        (1, 'Gerentes'),
        (2, 'Contadores'),
        (3, 'Técnicos de campo'),
        (4, 'Acopiadores'),
        (5, 'Trabajadores Centro de Acopio'),
        (6, 'Inspectores internos Certificación'),
        (7, 'Inspectores externos Certificación'),
        (8, 'Agentes de crédito'),
        (9, 'Prestadores de servicio'),

    )


CHOICES_PROCESOS = (

        (1, 'Reunión de Socios'),
        (2, 'Sesiones de la asamblea'),
        (3, 'Sesiones de Junta Directiva'),
        (4, 'Sesiones Junta de Vigilancia'),
        (5, 'Sesiones comité de educación y promoción'),
        (6, 'Sesiones comité de género'),
        (7, 'Sesiones de Gerencia'),
        (8, 'Sesiones administrativas'),
        (9, 'Sesiones técnicas'),
        (10, 'Visitas de parcelas'),
        (11, 'Capacitación a productores/as'),
        (12, 'Inspectoría de certificación interna'),
        (13, 'Inspectoría de certificación externa'),
        (14, 'Aprobación de créditos'),
        (15, 'Acopio de cacao'),
        (16, 'Acopio de granos básicos'),
        (17, 'Pago de cacao a Socios'),
        (18, 'Pago de granos básicos'),
        (19, 'Comercialización de cacao seco'),
        (20, 'Distribución de excedente a socios y socias'),

    )


CHOICES_SOCIOS_PRODUCCION = (

        (1, 'Número de socios varones con Cacao'),
        (2, 'Número de socias mujeres con Cacao'),
        (3, 'Área de cacao de los socios varones Mz'),
        (4, 'Área de cacao de las socias mujeres Mz'),
        (5, 'Producción de cacao de los socios varones QQ baba'),
        (6, 'Producción de cacao de las socias mujeres QQ baba'),

    )

CHOICES_PROVEEDORES_PRODUCCION = (

        (1, 'Número de proveedores varones con Cacao'),
        (2, 'Número de proveedoras mujeres con Cacao'),
        (3, 'Área de cacao de los proveedores varones Mz'),
        (4, 'Área de cacao de las proveedoras mujeres Mz'),
        (5, 'Producción de cacao de los proveedores varones QQ baba'),
        (6, 'Producción de cacao de las proveedoras mujeres QQ baba'),

    )

CHOICES_PLAN_NEGOCIO = (

        (1, 'Volumen anual de acopio de cacao de socios (QQ baba)'),
        (2, 'Volumen anual de acopio de cacao de proveedores (QQ baba)'),
        (3, 'Volumen anual de Cacao fermentado vendido (QQ seco)'),
        (4, 'Volumen anual de Cacao rojo vendido (QQ seco)'),
        (5, 'Número de contratos establecidos para la venta de cacao fermentado'),
        (6, 'Número de contratos establecidos para la venta de Cacao rojo'),
        (7, 'Número de contratos establecidos para la exportación de cacao fermentado'),
        (8, 'Número de contratos establecidos para la exportación de Cacao rojo')

    )

CHOICES_RENTABILIDAD = (

        (1, 'Volumen total de acopio (QQ baba)'),
        (2, 'Precio pagado para acopio (C$/QQ baba)'),
        (3, 'Precio pagado para insumos indirectos (C$/QQ baba)'),
        (4, 'Costos de procesamiento (C$/QQ baba)'),
        (5, 'Depreciación de centro de acopio y beneficiado de cacao'),
        (6, 'Volumen total de cacao seco procesado (QQ Seco)'),
        (7, 'Gastos de comercialización (QQ seco)'),
        (8, 'Precio recibido con venta de Cacao procesado (C$/QQ seco)'),
        (9, 'Cantidad de préstamo para acopio y procesamiento (C$)'),
        (10, 'Tasa anual de interés sobre el préstamo (%)')

    )


CHOICES_RENTABILIDAD_ORGANICO = (

        (1, 'Volumen total de acopio (QQ baba)'),
        (2, 'Precio pagado para acopio (C$/QQ baba)'),
        (3, 'Precio pagado para insumos indirectos (C$/QQ baba)'),
        (4, 'Costos de procesamiento (C$/QQ baba)'),
        (5, 'Depreciación de centro de acopio y beneficiado de cacao'),
        (6, 'Volumen total de cacao seco procesado (QQ Seco)'),
        (7, 'Gasto de empaque (C$/QQ seco)'),
        (8, 'Gasto de transporte para cacao seco (C$/QQ seco)'),
        (9, 'Precio recibido con venta de cacao procesado (C$/QQ seco)'),
        (10, 'Cantidad de préstamo para acopio y procesamiento (C$)'),
        (11, 'Tasa anual de interés sobre el préstamo (%)')

    )

CHOICES_CUMPLIMIENTO = (

        (1, 'Número de inconsistencia en la auditoria anual (#)'),
        (2, 'Socios satisfechos con manejo de recursos financieros (%)'),
        (3, 'Socios satisfechos con redición de cuenta (%)'),
        (4, 'Proveedores satisfechos con manejo de recursos financieros (%)'),
        (5, 'Proveedores satisfechos con redición de cuentas (%)'),
        (6, 'Disponibilidad de equipamiento básico al personal contratado (%)'),

    )

CHOICES_AVANCE_PLANES = (

        (1, 'Avance de implementación de plan de trabajo anual'),
        (2, 'Avance de elaboración de metodología de organización comunitaria'),
        (3, 'Avance de elaboración de metodología de visitas técnicas a productores'),
        (4, 'Avance de elaboración e implementación de Plan anual de capacitación'),
        (5, 'Avance de contrato de subvención suscritos con la ONUDI'),
        (6, 'Avance de registro de operaciones económicas con de normas de procesos administrativos financieros con sistemas contables'),
        (7, 'Avance de la capacidad de gestión incluyendo planificación'),
        (8, 'Avance de implementación de política y normativas de transparencia y anticorrupción'),
        (9, 'Avance de implementación de prácticas para mejorar la calidad del cacao')

    )


CHOICES_CICLO_12 = (

        (1, 'No iniciado'),
        (2, 'Iniciado'),
        (3, 'Avanzado'),
        (4, 'Cumplido '),

    )

CHOICES_COBERTURA_COOP = (

        (1, 'Número de comunidades en la zona de influencia de la cooperativa (#)'),
        (2, 'Número de comunidades cubierto por la Cooperativa (#)'),
        (3, 'Número de grupos comunitarios organizados en la Cooperativa (#)'),
        (4, 'Número de productores cubiertos con el plan de visita de AT (#) '),
        (5, 'Número total de visitas de Asistencia técnica realizadas durante el año (#)'),
        (6, 'Número de socios que establecieron áreas nuevas de SAF Cacao durante al año (#)'),
        (7, 'Cantidad de áreas nuevas de SAF Cacao establecidas durante al año por los socios (# mz) '),
        (8, 'Número de proveedores estableciendo área nueva de SAF Cacao durante el año (#)'),
        (9, 'Cantidad de áreas nuevas de SAF Cacao establecidas durante al año por los proveedores (# mz)'),
        (10, 'Número de grupos de jóvenes brindados servicios de injertación, poda, fertilización, venta de insumos y material genético a los productores (#)'),
        (11, 'Número de jóvenes varones organizados y brindados servicios a los productores (#)'),
        (12, 'Número de jóvenes mujeres organizados y brindados servicios a los productores (#)'),

    )




