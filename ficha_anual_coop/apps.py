from django.apps import AppConfig


class FichaAnualCoopConfig(AppConfig):
    name = 'ficha_anual_coop'
    verbose_name = "Ficha Anual Cooperativas"
