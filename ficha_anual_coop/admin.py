from django.contrib import admin
from .models import *

# Register your models here.
class InlinePersonalTotal(admin.TabularInline):
    model = PersonalTotal
    extra = 1
    max_num = 17

class InlinePersonalPagado(admin.TabularInline):
    model = PersonalPagado
    extra = 1
    max_num = 9

class InlinePersonalPagadoCoop(admin.TabularInline):
    model = PersonalPagadoCoop
    extra = 1
    max_num = 9

class InlineProcesos(admin.TabularInline):
    model = Procesos
    extra = 1
    max_num = 20

class InlineCapacitaciones(admin.TabularInline):
    model = Capacitaciones
    extra = 1
    
class InlineProdManejadoCoop(admin.TabularInline):
    model = ProdManejadoCoop
    extra = 1

class InlineProduccionSocios(admin.TabularInline):
    model = ProduccionSocios
    extra = 1
    max_num = 6

class InlineProduccionProveedores(admin.TabularInline):
    model = ProduccionProveedores
    extra = 1
    max_num = 6

class InlinePlanNegocioCacao(admin.TabularInline):
    model = PlanNegocioCacao
    extra = 1
    max_num = 8

class InlineRentabilidadFermentadoRA(admin.TabularInline):
    model = RentabilidadFermentadoRA
    extra = 1
    max_num = 10

class InlineRentabilidadFermentadoOrganico(admin.TabularInline):
    model = RentabilidadFermentadoOrganico
    extra = 1
    max_num = 11

class InlineCumplimientoCoop(admin.TabularInline):
    model = CumplimientoCoop
    extra = 1
    max_num = 6

class InlineAvancePlanesCoop(admin.TabularInline):
    model = AvancePlanesCoop
    extra = 1
    max_num = 9

class InlineCoberturaCoop(admin.TabularInline):
    model = CoberturaCoop
    extra = 1
    max_num = 12

class FichaAnualCooperativaAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(FichaAnualCooperativaAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(FichaAnualCooperativaAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    autocomplete_fields = ['cooperativa','evaluador']
    list_display = ('cooperativa','fecha','evaluador','user')
    list_filter = ('cooperativa__nombre','cooperativa__siglas',
                   'year')
    search_fields = ('cooperativa__nombre','evaluador__nombre')
    inlines = [InlinePersonalTotal,InlinePersonalPagado,
               InlinePersonalPagadoCoop,InlineProcesos,
               InlineCapacitaciones,InlineProdManejadoCoop,
               InlineProduccionSocios,InlineProduccionProveedores,
               InlinePlanNegocioCacao,InlineRentabilidadFermentadoRA,
               InlineRentabilidadFermentadoOrganico,InlineCumplimientoCoop,
               InlineAvancePlanesCoop,InlineCoberturaCoop]


admin.site.register(FichaAnualCooperativa, FichaAnualCooperativaAdmin)

