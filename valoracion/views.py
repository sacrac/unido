from django.shortcuts import render
from django.db.models import Avg, Sum, Count,  Value as V
from .models import FichaValoracion, ValoracionesCacaoDesarrollo
from catalogos.models import Clones
from .constants import CHOICES_LEYENDAS, CHOICES_SURCO
from ficha.views import _queryset_filtrado

from collections import OrderedDict

# Create your views here.
def salida25x25(request, template='valoracion/salida25.html'):
    filtro = _queryset_filtrado(request)
    filtro_productor = [ obj.productor_id for obj in filtro ]
    filtro_valoracion = FichaValoracion.objects.filter(productor_id__in=filtro_productor)
    print(filtro_valoracion)
    valoracion = ValoracionesCacaoDesarrollo.objects.all()

    tabla1 = OrderedDict()
    for obj in CHOICES_SURCO:
        excelente = valoracion.filter(ficha__in=filtro_valoracion,surco=obj[0]).aggregate(t=Sum('total_excelente'))['t']
        buenas = valoracion.filter(ficha__in=filtro_valoracion,surco=obj[0]).aggregate(t=Sum('total_buenas'))['t']
        malas = valoracion.filter(ficha__in=filtro_valoracion,surco=obj[0]).aggregate(t=Sum('total_malas'))['t']
        muertas = valoracion.filter(ficha__in=filtro_valoracion,surco=obj[0]).aggregate(t=Sum('total_muertas'))['t']
        no = valoracion.filter(ficha__in=filtro_valoracion,surco=obj[0]).aggregate(t=Sum('total_no'))['t']
        tabla1[obj[1]] = [excelente,buenas,malas,muertas,no]

    tabla2 = OrderedDict()
    for obj in Clones.objects.all():
        excelente = valoracion.filter(ficha__in=filtro_valoracion,clon=obj).aggregate(t=Sum('total_excelente'))['t']
        buenas = valoracion.filter(ficha__in=filtro_valoracion,clon=obj).aggregate(t=Sum('total_buenas'))['t']
        malas = valoracion.filter(ficha__in=filtro_valoracion,clon=obj).aggregate(t=Sum('total_malas'))['t']
        muertas = valoracion.filter(ficha__in=filtro_valoracion,clon=obj).aggregate(t=Sum('total_muertas'))['t']
        no = valoracion.filter(ficha__in=filtro_valoracion,clon=obj).aggregate(t=Sum('total_no'))['t']
        tabla2[obj.nombre] = [excelente,buenas,malas,muertas,no]

    return render(request, template,{'tabla1':tabla1, 'tabla2':tabla2})
