from django.contrib import admin
from .models import FichaValoracion, ValoracionesCacaoDesarrollo
from import_export import resources
from import_export.admin import ImportExportModelAdmin

class ValoracionCacaoResource(resources.ModelResource):

    class Meta:
        model = ValoracionesCacaoDesarrollo


class ValoracionInlines(admin.TabularInline):
    model = ValoracionesCacaoDesarrollo
    extra = 1
    #max_num = 25

class FichaValoracionAdmin(ImportExportModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(FichaValoracionAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(FichaValoracionAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    autocomplete_fields = ['productor']

    list_display = ['productor','tecnico','fecha','visita',
                            'numero_plantas','conteo_excelente',
                            'conteo_buenas','conteo_malas',
                            'conteo_muertas','conteo_no']
    list_filter = ('valoracionescacaodesarrollo__clon',)
    inlines = [ValoracionInlines]

    def conteo_excelente(self, obj):
        total = sum(obj.valoracionescacaodesarrollo_set.values_list('total_excelente', flat=True))
        return total
    conteo_excelente.short_description = 'Excelente'
    #conteo_excelente.admin_order_field = 'conteo_excelente'
    def conteo_buenas(self, obj):
        total = sum(obj.valoracionescacaodesarrollo_set.values_list('total_buenas', flat=True))
        return total
    conteo_buenas.short_description = 'Buenas'
    #conteo_excelente.admin_order_field = 'conteo_excelente'
    def conteo_malas(self, obj):
        total = sum(obj.valoracionescacaodesarrollo_set.values_list('total_malas', flat=True))
        return total
    conteo_malas.short_description = 'Malas'
    #conteo_excelente.admin_order_field = 'conteo_excelente'
    def conteo_muertas(self, obj):
        total = sum(obj.valoracionescacaodesarrollo_set.values_list('total_muertas', flat=True))
        return total
    conteo_muertas.short_description = 'Muertas'
    #conteo_excelente.admin_order_field = 'conteo_excelente'
    def conteo_no(self, obj):
        total = sum(obj.valoracionescacaodesarrollo_set.values_list('total_no', flat=True))
        return total
    conteo_no.short_description = 'No muestras'
    #conteo_excelente.admin_order_field = 'conteo_excelente'


# Register your models here.
admin.site.register(FichaValoracion, FichaValoracionAdmin)

class ValoracionCacaoAdmin(ImportExportModelAdmin):
    resource_class = ValoracionCacaoResource

admin.site.register(ValoracionesCacaoDesarrollo,ValoracionCacaoAdmin)
