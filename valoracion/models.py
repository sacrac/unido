# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from productores.models import Productor
from ficha.models import Tecnico
from .constants import (CHOICES_TIPO_CACAO,
                                            CHOICES_VISITAS,
                                            CHOICES_LEYENDAS,
                                            CHOICES_SURCO,
                                            CHOICES_NUMERO_PARCELAS)
from catalogos.models import Clones
# Create your models here.

class FichaValoracion(models.Model):
    """docstring for FichaValoracion"""
    productor = models.ForeignKey(Productor, verbose_name='Productor/a', on_delete=models.CASCADE)
    tecnico = models.ForeignKey(Tecnico, verbose_name='Técnico', on_delete=models.CASCADE)
    fecha = models.DateField('Fecha de visita')
    visita = models.IntegerField(choices=CHOICES_VISITAS, verbose_name="Número de visita")
    area = models.FloatField(verbose_name="Área de cacao en mz")
    edad = models.FloatField("Edad de la plantación")
    tipo_cacao = models.IntegerField(choices=CHOICES_TIPO_CACAO)
    numero_plantas = models.IntegerField('N. de plantas')
    n_parcela = models.IntegerField('Número de parcela', choices=CHOICES_NUMERO_PARCELAS,
                                                            null=True, blank=True)

    year = models.IntegerField(editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.year = self.fecha.year
        super(FichaValoracion, self).save(*args, **kwargs)

    def __str__(self):
        return self.productor.nombre

    class Meta:
        verbose_name = 'Ficha Valoración'
        verbose_name_plural = 'Ficha Valoraciones'

class ValoracionesCacaoDesarrollo(models.Model):
    ficha = models.ForeignKey(FichaValoracion, on_delete=models.CASCADE)
    clon = models.ForeignKey(Clones, on_delete=models.CASCADE)
    surco = models.IntegerField(choices=CHOICES_SURCO, verbose_name='# de Surco')
    campo1 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="1")
    campo2 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="2")
    campo3 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="3")
    campo4 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="4")
    campo5 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="5")
    campo6 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="6")
    campo7 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="7")
    campo8 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="8")
    campo9 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="9")
    campo10 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="10")
    campo11 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="11")
    campo12 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="12")
    campo13 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="13")
    campo14 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="14")
    campo15 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="15")
    campo16 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="16")
    campo17 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="17")
    campo18 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="18")
    campo19 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="19")
    campo20 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="20")
    campo21 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="21")
    campo22 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="22")
    campo23 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="23")
    campo24 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="24")
    campo25 = models.IntegerField(choices=CHOICES_LEYENDAS, verbose_name="25")

    total_excelente = models.IntegerField(editable=False)
    total_buenas = models.IntegerField(editable=False)
    total_malas = models.IntegerField(editable=False)
    total_muertas = models.IntegerField(editable=False)
    total_no = models.IntegerField(editable=False)

    def save(self, *args, **kwargs):
        self.total_excelente = 0
        self.total_buenas = 0
        self.total_malas = 0
        self.total_muertas = 0
        self.total_no = 0
        #--------------------------------
        if self.campo1 == 1:
            self.total_excelente += 1
        if self.campo1 == 2:
            self.total_buenas += 1
        if self.campo1 == 3:
            self.total_malas += 1
        if self.campo1 == 4:
            self.total_muertas += 1
        if self.campo1 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo2 == 1:
            self.total_excelente += 1
        if self.campo2 == 2:
            self.total_buenas += 1
        if self.campo2 == 3:
            self.total_malas += 1
        if self.campo2 == 4:
            self.total_muertas += 1
        if self.campo2 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo3 == 1:
            self.total_excelente += 1
        if self.campo3 == 2:
            self.total_buenas += 1
        if self.campo3 == 3:
            self.total_malas += 1
        if self.campo3 == 4:
            self.total_muertas += 1
        if self.campo3 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo4 == 1:
            self.total_excelente += 1
        if self.campo4 == 2:
            self.total_buenas += 1
        if self.campo4 == 3:
            self.total_malas += 1
        if self.campo4 == 4:
            self.total_muertas += 1
        if self.campo4 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo5 == 1:
            self.total_excelente += 1
        if self.campo5 == 2:
            self.total_buenas += 1
        if self.campo5 == 3:
            self.total_malas += 1
        if self.campo5 == 4:
            self.total_muertas += 1
        if self.campo5 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo6 == 1:
            self.total_excelente += 1
        if self.campo6 == 2:
            self.total_buenas += 1
        if self.campo6 == 3:
            self.total_malas += 1
        if self.campo6 == 4:
            self.total_muertas += 1
        if self.campo6 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo7 == 1:
            self.total_excelente += 1
        if self.campo7 == 2:
            self.total_buenas += 1
        if self.campo7 == 3:
            self.total_malas += 1
        if self.campo7 == 4:
            self.total_muertas += 1
        if self.campo7 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo8 == 1:
            self.total_excelente += 1
        if self.campo8 == 2:
            self.total_buenas += 1
        if self.campo8 == 3:
            self.total_malas += 1
        if self.campo8 == 4:
            self.total_muertas += 1
        if self.campo8 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo9 == 1:
            self.total_excelente += 1
        if self.campo9 == 2:
            self.total_buenas += 1
        if self.campo9 == 3:
            self.total_malas += 1
        if self.campo9 == 4:
            self.total_muertas += 1
        if self.campo9 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo10 == 1:
            self.total_excelente += 1
        if self.campo10 == 2:
            self.total_buenas += 1
        if self.campo10 == 3:
            self.total_malas += 1
        if self.campo10 == 4:
            self.total_muertas += 1
        if self.campo10 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo11 == 1:
            self.total_excelente += 1
        if self.campo11 == 2:
            self.total_buenas += 1
        if self.campo11 == 3:
            self.total_malas += 1
        if self.campo11 == 4:
            self.total_muertas += 1
        if self.campo11 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo12 == 1:
            self.total_excelente += 1
        if self.campo12 == 2:
            self.total_buenas += 1
        if self.campo12 == 3:
            self.total_malas += 1
        if self.campo12 == 4:
            self.total_muertas += 1
        if self.campo12 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo13 == 1:
            self.total_excelente += 1
        if self.campo13 == 2:
            self.total_buenas += 1
        if self.campo13 == 3:
            self.total_malas += 1
        if self.campo13 == 4:
            self.total_muertas += 1
        if self.campo13 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo14 == 1:
            self.total_excelente += 1
        if self.campo14 == 2:
            self.total_buenas += 1
        if self.campo14 == 3:
            self.total_malas += 1
        if self.campo14 == 4:
            self.total_muertas += 1
        if self.campo14 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo15 == 1:
            self.total_excelente += 1
        if self.campo15 == 2:
            self.total_buenas += 1
        if self.campo15 == 3:
            self.total_malas += 1
        if self.campo15 == 4:
            self.total_muertas += 1
        if self.campo15 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo16 == 1:
            self.total_excelente += 1
        if self.campo16 == 2:
            self.total_buenas += 1
        if self.campo16 == 3:
            self.total_malas += 1
        if self.campo16 == 4:
            self.total_muertas += 1
        if self.campo16 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo17 == 1:
            self.total_excelente += 1
        if self.campo17 == 2:
            self.total_buenas += 1
        if self.campo17 == 3:
            self.total_malas += 1
        if self.campo17 == 4:
            self.total_muertas += 1
        if self.campo17 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo18 == 1:
            self.total_excelente += 1
        if self.campo18 == 2:
            self.total_buenas += 1
        if self.campo18 == 3:
            self.total_malas += 1
        if self.campo18 == 4:
            self.total_muertas += 1
        if self.campo18 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo19 == 1:
            self.total_excelente += 1
        if self.campo19 == 2:
            self.total_buenas += 1
        if self.campo19 == 3:
            self.total_malas += 1
        if self.campo19 == 4:
            self.total_muertas += 1
        if self.campo19 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo20 == 1:
            self.total_excelente += 1
        if self.campo20 == 2:
            self.total_buenas += 1
        if self.campo20 == 3:
            self.total_malas += 1
        if self.campo20 == 4:
            self.total_muertas += 1
        if self.campo20 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo21 == 1:
            self.total_excelente += 1
        if self.campo21 == 2:
            self.total_buenas += 1
        if self.campo21 == 3:
            self.total_malas += 1
        if self.campo21 == 4:
            self.total_muertas += 1
        if self.campo21 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo22 == 1:
            self.total_excelente += 1
        if self.campo22 == 2:
            self.total_buenas += 1
        if self.campo22 == 3:
            self.total_malas += 1
        if self.campo22 == 4:
            self.total_muertas += 1
        if self.campo22 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo23 == 1:
            self.total_excelente += 1
        if self.campo23 == 2:
            self.total_buenas += 1
        if self.campo23 == 3:
            self.total_malas += 1
        if self.campo23 == 4:
            self.total_muertas += 1
        if self.campo23 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo24 == 1:
            self.total_excelente += 1
        if self.campo24 == 2:
            self.total_buenas += 1
        if self.campo24 == 3:
            self.total_malas += 1
        if self.campo24 == 4:
            self.total_muertas += 1
        if self.campo24 == 5:
            self.total_no += 1
        #----------------------------------
        #--------------------------------
        if self.campo25 == 1:
            self.total_excelente += 1
        if self.campo25 == 2:
            self.total_buenas += 1
        if self.campo25 == 3:
            self.total_malas += 1
        if self.campo25 == 4:
            self.total_muertas += 1
        if self.campo25 == 5:
            self.total_no += 1
        #----------------------------------

        super(ValoracionesCacaoDesarrollo, self).save(*args, **kwargs)

    def __str__(self):
        return self.ficha.productor.nombre

    class Meta:
        verbose_name = "Valoración Cacao Desarrollo"
        verbose_name_plural = "Valoración Cacao Desarrollo"
