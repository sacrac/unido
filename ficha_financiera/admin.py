from django.contrib import admin
from .models import *
# Register your models here.

class InlineActivosCoop(admin.TabularInline):
    model = ActivosCoop
    extra = 1
    max_num = 10

class InlinePasivosCoop(admin.TabularInline):
    model = PasivosCoop
    extra = 1
    max_num = 11

class InlinePatrimonioCoop(admin.TabularInline):
    model = PatrimonioCoop
    extra = 1
    max_num = 8

class InlineIngresosCoop(admin.TabularInline):
    model = IngresosCoop
    extra = 1
    max_num = 8

class InlineCostosCoop(admin.TabularInline):
    model = CostosCoop
    extra = 1
    max_num = 5

class InlineGastosOperacionalesCoop(admin.TabularInline):
    model = GastosOperacionalesCoop
    extra = 1
    max_num = 5

class InlineGastosDirectivosCoop(admin.TabularInline):
    model = GastosDirectivosCoop
    extra = 1
    max_num = 5

class FichaFinancieraCooperativaAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(FichaFinancieraCooperativaAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(FichaFinancieraCooperativaAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    autocomplete_fields = ['cooperativa','evaluador']
    list_display = ('cooperativa','fecha','evaluador','user')
    list_filter = ('cooperativa__nombre','cooperativa__siglas',
                   'year')
    search_fields = ('cooperativa__nombre','evaluador__nombre')
    inlines = [InlineActivosCoop,InlinePasivosCoop,
               InlinePatrimonioCoop,InlineIngresosCoop,
               InlineCostosCoop,InlineGastosOperacionalesCoop,
               InlineGastosDirectivosCoop]


admin.site.register(FichaFinancieraCooperativa, FichaFinancieraCooperativaAdmin)
