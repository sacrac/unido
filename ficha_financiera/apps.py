from django.apps import AppConfig


class FichaFinancieraConfig(AppConfig):
    name = 'ficha_financiera'
    verbose_name = 'Ficha Financiera Cooperativas'
