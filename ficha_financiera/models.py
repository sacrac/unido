from django.db import models
from django.contrib.auth.models import User
from cooperativas.models import Cooperativa
from catalogos.models import Evaluador
from .constants_finanza import *
# Create your models here.

class FichaFinancieraCooperativa(models.Model):
    cooperativa = models.ForeignKey(Cooperativa, on_delete=models.CASCADE)
    mes = models.IntegerField('Mes de evaluación', choices=CHOICES_MESES)
    ciclo = models.IntegerField('Ciclo de evaluación', choices=CHOICES_CICLO)
    fecha = models.DateField('Fecha de evaluación')
    evaluador = models.ForeignKey(Evaluador, on_delete=models.CASCADE)

    year = models.IntegerField(editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.year = self.fecha.year
        super(FichaFinancieraCooperativa, self).save(*args, **kwargs)

    def __str__(self):
        return self.cooperativa.siglas

    class Meta:
        verbose_name = 'Ficha financiera cooperativa'
        verbose_name_plural = 'Fichas financiera de cooperativas'
        ordering = ['cooperativa']

class ActivosCoop(models.Model):
    ficha = models.ForeignKey(FichaFinancieraCooperativa, on_delete=models.CASCADE)
    cuentas = models.IntegerField(choices=CHOICES_ACTIVOS)
    total = models.FloatField('Total / Valor Monetario')

    class Meta:
        verbose_name='Estado financiero activos'
        verbose_name_plural='Estado financiero activos'

class PasivosCoop(models.Model):
    ficha = models.ForeignKey(FichaFinancieraCooperativa, on_delete=models.CASCADE)
    cuentas = models.IntegerField(choices=CHOICES_PASIVOS)
    total = models.FloatField('Total / Valor Monetario')

    class Meta:
        verbose_name='Estado financiero pasivos'
        verbose_name_plural='Estado financiero pasivos'

class PatrimonioCoop(models.Model):
    ficha = models.ForeignKey(FichaFinancieraCooperativa, on_delete=models.CASCADE)
    cuentas = models.IntegerField(choices=CHOICES_PATRIMONIO)
    total = models.FloatField('Total / Valor Monetario')

    class Meta:
        verbose_name='Estado financiero patrimonio'
        verbose_name_plural='Estado financiero patrimonio'


class IngresosCoop(models.Model):
    ficha = models.ForeignKey(FichaFinancieraCooperativa, on_delete=models.CASCADE)
    cuentas = models.IntegerField(choices=CHOICES_INGRESOS)
    total = models.FloatField('Total / Valor Monetario')

    class Meta:
        verbose_name='Estado financiero ingresos'
        verbose_name_plural='Estado financiero ingresos'

class CostosCoop(models.Model):
    ficha = models.ForeignKey(FichaFinancieraCooperativa, on_delete=models.CASCADE)
    cuentas = models.IntegerField(choices=CHOICES_COSTOS)
    total = models.FloatField('Total / Valor Monetario')

    class Meta:
        verbose_name='Estado financiero costos'
        verbose_name_plural='Estado financiero costos'

class GastosOperacionalesCoop(models.Model):
    ficha = models.ForeignKey(FichaFinancieraCooperativa, on_delete=models.CASCADE)
    cuentas = models.IntegerField(choices=CHOICES_GASTOS_OPERACIONALES)
    total = models.FloatField('Total / Valor Monetario')

    class Meta:
        verbose_name='Estado financiero gastos operacionales'
        verbose_name_plural='Estado financiero gastos operacionales'

class GastosDirectivosCoop(models.Model):
    ficha = models.ForeignKey(FichaFinancieraCooperativa, on_delete=models.CASCADE)
    cuentas = models.IntegerField(choices=CHOICES_GASTO_ORGANO_DIRECTIVOS)
    total = models.FloatField('Total / Valor Monetario')

    class Meta:
        verbose_name='Estado financiero gastos directivos'
        verbose_name_plural='Estado financiero gastos directivos'

