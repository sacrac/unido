
CHOICES_MESES = (

        (1, 'Enero'),
        (2, 'Febrero'),
        (3, 'Marzo'),
        (4, 'Abril'),
        (5, 'Mayo'),
        (6, 'Junio'),
        (7, 'Julio'),
        (8, 'Agosto'),
        (9, 'Septiembre'),
        (10, 'Octubre'),
        (11, 'Noviembre'),
        (12, 'Diciembre'),
    )


CHOICES_CICLO = (

        (1, '2020 (Enero - Diciembre)'),
        (2, '2021 (Enero - Diciembre)'),
        (3, '2022 (Enero - Diciembre)'),
        (4, '2023 (Enero - Diciembre)'),
        (5, '2024 (Enero - Diciembre)'),
        (6, '2025 (Enero - Diciembre)'),

    )

CHOICES_ACTIVOS = (

        (1, 'Efectivo en Caja'),
        (2, 'Efectivo en Banco'),
        (3, 'Cuentas por Cobrar'),
        (4, 'Inventarios'),
        (5, 'Propiedad Planta y Equipo'),
        (6, 'Depreciación Acumulada'),
        (7, 'Activos Intangibles'),
        (8, 'Amortización acumulada'),
        (9, 'Pagos Anticipados'),
        (10, 'Activo Biológicos'),

    )


CHOICES_PASIVOS = (

        (1, 'Proveedores'),
        (2, 'Acreedores por Pagar'),
        (3, 'Gastos Acumulados Por Pagar'),
        (4, 'Cuentas por Pagar a Corto Plazo'),
        (5, 'Hipotecas'),
        (6, 'Préstamos por Pagar'),
        (7, 'Gastos Financieros por Pagar'),
        (8, 'Cuentas por Pagar a Largo Plazo'),
        (9, 'Fondos por Rendir (Programas)'),
        (10, 'Arrendamiento Financiero'),
        (11, 'Otras Cuentas por Pagar')

    )


CHOICES_PATRIMONIO = (

        (1, 'Capital Social'),
        (2, 'Excedentes Acumulados'),
        (3, 'Excedentes del Período'),
        (4, 'Donaciones'),
        (5, 'Reservas de Ley'),
        (6, 'Déficit Acumulado'),
        (7, 'Revalorización de Activos'),
        (8, 'Déficit del Período'),

    )


CHOICES_INGRESOS = (

        (1, 'Comercialización de Cacao'),
        (2, 'Tienda Campesina'),
        (3, 'Jardín Clonal'),
        (4, 'Servicio Trillado'),
        (5, 'Ingresos Servicio de Beneficiado'),
        (6, 'Bonos e Incentivos'),
        (7, 'Otros ingresos'),

    )


CHOICES_COSTOS = (

        (1, 'Costos de Beneficiado de Cacao'),
        (2, 'Costos de Tienda Campesina'),
        (3, 'Costos de Jardín Clonal'),
        (4, 'Costos de servicio trillado'),
        (5, 'Otros Costos'),

    )

CHOICES_GASTOS_OPERACIONALES = (

        (1, 'Gastos de Administración'),
        (2, 'Gastos de Venta'),
        (3, 'Gastos Financieros'),
        (4, 'Otros Gastos'),

    )


CHOICES_GASTO_ORGANO_DIRECTIVOS = (

        (1, 'Asamblea General'),
        (2, 'Consejo de Administración'),
        (3, 'Junta de Vigilancia'),
        (4, 'Comité de Educación'),
        (5, 'Comité de Comercialización'),

    )










