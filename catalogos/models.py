from django.db import models

# Create your models here.

class DisponibilidadEnergia(models.Model):
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Tipos de Energia'

class FuenteAgua(models.Model):
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Fuentes de agua'

CHOICES_TIERRA = (
                (1, 'Área total'),
                (2, 'Bosque primario'),
                (3, 'Bosque secundario'),
                (4, 'Cultivos anuales'),
                (5, 'Plantaciones  forestal'),
                (6, 'Áreas de pastos abierto'),
                (7, 'Áreas de pastos con árboles'),
                (8, 'Cultivos perennes'),
    )

CHOICES_TRABAJADORES = (
                (1, 'Hombres Adultos'),
                (2, 'Mujeres Adultas'),
                (3, 'Hombres Jóvenes'),
                (4, 'Mujeres Jóvenes'),
    )

class Actvidades(models.Model):
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Actividades de fuerza de trabajo'

CHOICES_SI_NO = (
                (1, 'Sí'),
                (2, 'No'),
    )

CHOICES_UNIDAD_MEDIDA = (
                (1, 'mz'),
                (2, 'plantas'),
                (3, 'lb'),
                (4, 'vacas'),
                (5, 'gallinas'),
                (6, 'animales')
    )

CHOICES_UNIDAD_MEDIDA_PRODUCCION = (
                (1, 'qq baba'),
                (2, 'cien'),
                (3, 'qq'),
                (4, 'racimo'),
                (5, 'lt'),
                (6, 'doc'),
                (7, 'animales'),
                (8, 'C$'),
                (9, 'lb'),
    )

CHOICES_DESTINO_PRODUCCION = (
                (1, 'Autoconsumo'),
                (2, 'Venta'),
                (3, 'Ambos'),
    )

CHOICES_CATEGORIA_PRODUCCION = (
                (1, 'Granos básicos'),
                (2, 'Agroforestal y frutales'),
                (3, 'Musaceas, raíces y tubérculos'),
                (4, 'Ganadería mayor y menor'),
    )

CHOICES_MESES = ( (1, 'Meses'),)

class RubrosProduccion(models.Model):
    nombre = models.CharField(max_length=150)
    unidad = models.IntegerField(choices=CHOICES_UNIDAD_MEDIDA)
    categoria = models.IntegerField(choices=CHOICES_CATEGORIA_PRODUCCION)

    def __str__(self):
        return "%s - %s" % (self.nombre, self.get_unidad_display())

    class Meta:
        verbose_name_plural = 'Rubros para la producción'

class RubrosIngresos(models.Model):
    nombre = models.CharField(max_length=150)
    unidad = models.IntegerField(choices=CHOICES_MESES)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Rubros para Ingresos'

CHOICES_ESTADO = ( (1, 'D'),(2,'P'),(3,'D-P'),)

CHOICES_CALIDAD = (
                (1, 'Bueno'),
                (2, 'Regular'),
                (3, 'Mal'),
                (4, 'No realiza'),
        )

CHOICES_USO_MANEJO = (
                (1, 'En todos los plantios'),
                (2, 'En varios platios'),
                (3, 'Solamente en algunos plantíos'),
                (4, 'En ninguno plantío'),
        )

CHOICE_MESES = (
                ('A', 'Enero'),
                ('B', 'Febrero'),
                ('C', 'Marzo'),
                ('D', 'Abril'),
                ('E', 'Mayo'),
                ('F', 'Junio'),
                ('G', 'Julio'),
                ('H', 'Agosto'),
                ('I', 'Septiembre'),
                ('J', 'Octubre'),
                ('K', 'Noviembre'),
                ('L', 'Diciembre'),
              )

CHOICES_OPCIONES_AGRO = (
                (1, 'Biofertilzantes'),
                (2, 'Compost'),
                (3, 'Roca Minerales'),
                (4, 'Insecticida natural'),
                (5, 'Fungicida natural'),
                (6, 'Cerca viva'),
                (7, 'Cortina rompe viento'),
                (8, 'Abonos verdes'),
                (9, 'Siembra en Curva a nivel'),
                (10, 'Acequia'),
                (11, 'Barrera viva'),
                (12, 'Barrera muerta'),
                (13, 'Cosecha de agua'),
                (14, 'Incorporación de rastrojo'),
                (15, 'Manejo selectivo de malas hierbas'),
                (16, 'Siembra de coberturas'),
                (17, 'Siembra de plantas injertadas'),
                (18, 'Diversificación de los cacaotales'),
                (19, 'Eliminación de mazorcas enfermas'),
        )

CHOICES_CORTES = (
                (1, 'Con machete'),
                (2, 'Con tijera'),
                (3, 'Con media luna'),
                (4, 'Con dejarretadora'),
            )

CHOICES_SEPARAN_MAZORCA = (
                (1, 'Si'),
                (2, 'No'),
                (3, 'Algunas veces'),
            )

CHOICES_QUIEBRAN_MAZORCA = (
                (1, 'Con Cutacha'),
                (2, 'Con mazo'),
                (3, 'Con piedra'),
                (4, 'Con machete'),
            )

CHOICES_OPCION_CADENA = (
        ('A','Cosecha – Seca en finca- Entrega cacao seco no fermentado a Empresa/Cooperativa'),
        ('B','Cosecha- Seca en finca – Entrega cacao seco no fermentado a Intermediarios'),
        ('C', 'Cosecha- Entrega cacao  baba a Empresa'),
        ('D', 'Cosecha- Entrega cacao  baba a Cooperativa'),
        ('E', 'Cosecha- Entrega cacao  baba a Intermediarios'),
        ('F', 'Cosecha -  Fermenta en finca – Entrega cacao seco fermentado a Empresa'),
        ('G', 'Cosecha -  Fermenta en finca – Entrega cacao seco fermentado a Cooperativa'),
        ('H', 'Cosecha -  Fermenta en finca – Entrega cacao seco fermentado a Intermediario'),
    )

CHOICES_TEMAS_CONOCIMIENTO = (
                (1, 'Pobre'),
                (2, 'Algo'),
                (3, 'Regular'),
                (4, 'Bueno'),
                (5, 'Excelente'),
            )

class Temas(models.Model):
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Temas de conocimiento y habilidades'

CHOICES_ESCALAS = (
                (1, 'No utiliza'),
                (2, 'En pequeño escala'),
                (3, 'En mayor escala'),
                (4, 'En toda la finca'),
            )

CHOICES_COMERCIALIZACION = (
                (1, 'Cacao baba'),
                (2, 'Cacao seco fermentado'),
                (3, 'Cacao seco no fermentado'),
                (4, 'Cacao en mazorca'),
            )

CHOICES_CADA_CUANTO = (
            (1,'Mensual'),
            (2,'Cada 3 meses'),
            (3,'Cada 6 meses'),
            (4,'Anual'),
    )

CHOICES_COMO_REALIZA = (
            (1,'Observaciones'),
            (2,'Recuentos'),
    )

CHOICES_LLEVA_REGISTROS = (
            (1,'Si'),
            (2,'No'),
            (3, 'Si y Procesa y usa los datos')
    )

CHOICES_FALTA_RECURSO = (
            (1,'Insumos'),
            (2,'Pago de Mano de obra'),
            (3, 'Gastos operativos'),
            (4, 'Inversiones'),
    )

CHOICES_TIPO_CERTIFICADO = (
            ('A','CJ'),
            ('B','ORG'),
            ('C', 'RFA'),
            ('D', 'UTZ'),
            ('E', 'SPP'),
            ('F', 'No Certificado'),
    )

CHOICES_APOYA_PLANES = (
            ('A','Propio'),
            ('B','Contrate AT'),
            ('C', 'ONG'),
            ('D', 'Empresas'),
            ('E', 'Cooperativa'),
            ('F', 'Asociación'),
            ('G', 'Banco o Micro-finanza'),
            ('H', 'Nadie apoya'),
    )

class Clones(models.Model):
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre

    class Meta:
        unique_together = ('nombre',)
        verbose_name = "Clon"
        verbose_name_plural = "Clones"
        ordering = ['nombre']

#productos de la fichas de cierre del año
CHOICE_UNIDAD_PRODUCTOS_PARCELA_FINCA = (
        (1, 'qq'),
        (2, 'Cabeza/Racimo'),
        (3, 'Lb'),
        (4, 'Kg'),
        (5, 'Docena'),
        (6, 'Cien'),
        (7, 'Sacos de 100'),
        (8, 'Sacos'),
        (9, 'M3'),
        (10, 'Pt'),
        (11, 'Unidad'),
        (12, 'Carga'),
        (13, 'Lt'),
        (14, 'Cabeza')
    )

class ProductosParcela(models.Model):
    nombre = models.CharField(max_length=150)
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_PRODUCTOS_PARCELA_FINCA)

    def __str__(self):
        return '{0} - {1}'.format(self.nombre, self.get_unidad_display())

    class Meta:
        unique_together = ('nombre',)
        verbose_name = "Producto parcela saf"
        verbose_name_plural = "Productos parcelas saf"
        ordering = ['nombre']

class ProductosFinca(models.Model):
    nombre = models.CharField(max_length=150)
    unidad = models.IntegerField(choices=CHOICE_UNIDAD_PRODUCTOS_PARCELA_FINCA)

    def __str__(self):
        return '{0} - {1}'.format(self.nombre, self.get_unidad_display())

    class Meta:
        unique_together = ('nombre',)
        verbose_name = "Producto de la finca"
        verbose_name_plural = "Productos de las fincas"
        ordering = ['nombre']

class PlantasMusaceas(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return '{0}'.format(self.nombre)

    class Meta:
        unique_together = ('nombre',)
        verbose_name = "Planta musácea"
        verbose_name_plural = "Plantas musáceas"
        ordering = ['nombre']

class PlantasFrutales(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return '{0}'.format(self.nombre)

    class Meta:
        unique_together = ('nombre',)
        verbose_name = "Planta frutal"
        verbose_name_plural = "Plantas frutales"
        ordering = ['nombre']

class PlantasMaderables(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return '{0}'.format(self.nombre)

    class Meta:
        unique_together = ('nombre',)
        verbose_name = "Planta maderable"
        verbose_name_plural = "Plantas maderables"
        ordering = ['nombre']

class ArbolesServicios(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return '{0}'.format(self.nombre)

    class Meta:
        unique_together = ('nombre',)
        verbose_name = "Árboles servicios"
        verbose_name_plural = "Árboles servicios"
        ordering = ['nombre']


class ProductosInversion(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return '{0}'.format(self.nombre)

    class Meta:
        unique_together = ('nombre',)
        verbose_name = "Productos para Inversiones"
        verbose_name_plural = "Productos para Inversiones"
        ordering = ['nombre']


#apps para ficha anual cooperativas
class Evaluador(models.Model):
    """docstring for Evaluador"""
    nombre = models.CharField(max_length=250)
    correo = models.EmailField(null=True, blank=True)
    telefono = models.CharField('Teléfono ', max_length=50, null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Evaluador'
        verbose_name_plural = 'Evaluadores'
        unique_together = ('nombre',)
        ordering = ['nombre']


class TemasCapacitaciones(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Temas de capacitaciones'
        verbose_name_plural = 'Temas de capacitaciones'
        unique_together = ('nombre',)
        ordering = ['nombre']



class ProductosCoop(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Productos cooperativas'
        verbose_name_plural = 'Productos cooperativas'
        unique_together = ('nombre',)
        ordering = ['nombre']

