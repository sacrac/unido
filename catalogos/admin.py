from django.contrib import admin
from .models import *

class RubrosProduccionAdmin(admin.ModelAdmin):
    list_display = ('nombre','unidad','categoria')

class ProductosParcelaAdmin(admin.ModelAdmin):
    search_fields = ('nombre',)
    list_display = ('nombre', 'unidad')

class ProductosFincaAdmin(admin.ModelAdmin):
    search_fields = ('nombre',)
    list_display = ('nombre', 'unidad')

# Register your models here.

class EvaluadorAdmin(admin.ModelAdmin):

    search_fields = ('nombre',)


admin.site.register(DisponibilidadEnergia)
admin.site.register(FuenteAgua)
admin.site.register(Actvidades)
admin.site.register(RubrosProduccion, RubrosProduccionAdmin)
admin.site.register(RubrosIngresos)
admin.site.register(Temas)
admin.site.register(Clones)
admin.site.register(ProductosInversion)
admin.site.register(ProductosFinca,ProductosFincaAdmin)
admin.site.register(ProductosParcela,ProductosParcelaAdmin)
admin.site.register(PlantasMusaceas)
admin.site.register(PlantasFrutales)
admin.site.register(PlantasMaderables)
admin.site.register(ArbolesServicios)
admin.site.register(Evaluador, EvaluadorAdmin)
admin.site.register(TemasCapacitaciones)
admin.site.register(ProductosCoop)