# Generated by Django 2.2.5 on 2021-10-04 00:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogos', '0005_auto_20210615_1834'),
    ]

    operations = [
        migrations.CreateModel(
            name='TemasCapacitaciones',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=250)),
            ],
            options={
                'verbose_name': 'Temas de capacitaciones',
                'verbose_name_plural': 'Temas de capacitaciones',
                'ordering': ['nombre'],
                'unique_together': {('nombre',)},
            },
        ),
        migrations.CreateModel(
            name='ProductosCoop',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=250)),
            ],
            options={
                'verbose_name': 'Productos cooperativas',
                'verbose_name_plural': 'Productos cooperativas',
                'ordering': ['nombre'],
                'unique_together': {('nombre',)},
            },
        ),
        migrations.CreateModel(
            name='Evaluador',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=250)),
                ('correo', models.EmailField(blank=True, max_length=254, null=True)),
                ('telefono', models.CharField(blank=True, max_length=50, null=True, verbose_name='Teléfono ')),
            ],
            options={
                'verbose_name': 'Evaluador',
                'verbose_name_plural': 'Evaluadores',
                'ordering': ['nombre'],
                'unique_together': {('nombre',)},
            },
        ),
    ]
