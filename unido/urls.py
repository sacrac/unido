"""unido URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
import debug_toolbar
from django.views.generic.base import TemplateView

admin.site.site_title = "Sitio Administrativo de UNIDO"
admin.site.site_header = "UNIDO Administración"

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^login/$', auth_views.LoginView.as_view(template_name='ficha/login.html'), name='login' ),
    re_path(r'^logout/$', auth_views.LogoutView.as_view(next_page='/')),
    re_path(r'', include('ficha.urls')),
    re_path(r'^cierre/', include('ficha_cierre.urls')),
    re_path(r'^visita/', include('ficha_visitas.urls')),
    re_path(r'^productores/', include('productores.urls')),
    re_path(r'^valoracion/', include('valoracion.urls')),
    re_path(r'^robots.txt/', TemplateView.as_view(template_name="robots.txt", content_type="text/plain")),
]

if settings.DEBUG:
    urlpatterns.append(path('__debug__/', include(debug_toolbar.urls)),)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL , document_root = settings.STATIC_ROOT )
    urlpatterns += static(settings.MEDIA_URL , document_root = settings.MEDIA_ROOT )


