from django.db import models
from django.contrib.auth.models import User
from cooperativas.models import Cooperativa
from catalogos.models import Evaluador, ProductosCoop
from .constants_mensual import *
# Create your models here.

class FichaMensualCooperativa(models.Model):
    cooperativa = models.ForeignKey(Cooperativa, on_delete=models.CASCADE)
    mes = models.IntegerField('Mes de evaluación', choices=CHOICES_MESES)
    ciclo = models.IntegerField('Ciclo de evaluación', choices=CHOICES_CICLO)
    fecha = models.DateField('Fecha de evaluación')
    evaluador = models.ForeignKey(Evaluador, on_delete=models.CASCADE)

    year = models.IntegerField(editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.year = self.fecha.year
        super(FichaMensualCooperativa, self).save(*args, **kwargs)

    def __str__(self):
        return self.cooperativa.siglas

    class Meta:
        verbose_name = 'Ficha mensual cooperativa'
        verbose_name_plural = 'Fichas mensuales de cooperativas'
        ordering = ['cooperativa']

class PersonalActivo(models.Model):
    ficha = models.ForeignKey(FichaMensualCooperativa, on_delete=models.CASCADE)
    personal = models.IntegerField(choices=CHOICES_PERSONAL)
    hombres = models.IntegerField('Cant. Hombres')
    mujeres = models.IntegerField('Cant. Mujeres')
    jovenes = models.IntegerField('Cant. H Jóvenes')
    jovenas = models.IntegerField('Cant. M Jóvenes')

    class Meta:
        verbose_name='1. Personal activa durante el mes'
        verbose_name_plural='1. Personal activa durante el mes'


class ProcesosActivos(models.Model):
    ficha = models.ForeignKey(FichaMensualCooperativa, on_delete=models.CASCADE)
    procesos = models.IntegerField(choices=CHOICES_PROCESOS)
    evanto_anual = models.IntegerField('# de eventos')


    class Meta:
        verbose_name='2. Proceso activos durante el mes'
        verbose_name_plural='2. Proceso activos durante el mes'


class ProdManejadoCoopMensual(models.Model):
    ficha = models.ForeignKey(FichaMensualCooperativa, on_delete=models.CASCADE)
    datos = models.ForeignKey(ProductosCoop, on_delete=models.CASCADE)
    socios = models.IntegerField('Número de socios involucrados')
    proveedores = models.IntegerField('Número de Proveedores involucrados')
    volumen = models.FloatField('Volumen de producto manejado durante el mes QQ')
    precio = models.FloatField('Precio de venta de productos (C$/QQ)')

    class Meta:
        verbose_name='3. Productos manejados por la Cooperativa durante el mes'
        verbose_name_plural='3. Productos manejados por la Cooperativa durante el mes'


class ProduccionSocios(models.Model):
    ficha = models.ForeignKey(FichaMensualCooperativa, on_delete=models.CASCADE)
    datos = models.IntegerField(choices=CHOICES_SOCIOS_PRODUCCION_MENSUAL)
    total = models.FloatField('Total durante el mes')
    certificado = models.FloatField('Ciclo certificado RA')
    ciclo = models.FloatField('Ciclo convencional')
    ciclo_organico = models.FloatField('Ciclo certificado Orgánico')

    class Meta:
        verbose_name='4-1. Acopio de Cacao en baba para cacao fermentado'
        verbose_name_plural='4-1. Acopio de Cacao en baba para cacao fermentado'

class ProduccionProveedores(models.Model):
    ficha = models.ForeignKey(FichaMensualCooperativa, on_delete=models.CASCADE)
    datos = models.IntegerField(choices=CHOICES_PROVEEDORES_PRODUCCION_MENSUAL)
    total = models.FloatField('Total durante el mes')
    certificado = models.FloatField('Ciclo certificado RA')
    ciclo = models.FloatField('Ciclo convencional')
    ciclo_organico = models.FloatField('Ciclo certificado Orgánico')

    class Meta:
        verbose_name='4-2. Acopio de Cacao en baba para cacao fermentado'
        verbose_name_plural='4-2. Acopio de Cacao en baba para cacao fermentado'


class PlanNegocioCacao(models.Model):
    ficha = models.ForeignKey(FichaMensualCooperativa, on_delete=models.CASCADE)
    datos = models.IntegerField(choices=CHOICES_PLAN_NEGOCIO_MENSUAL)
    total = models.FloatField()
    certificado = models.FloatField()
    

    class Meta:
        verbose_name='5. Plan de negocio de cacao del mes'
        verbose_name_plural='5. Plan de negocio de cacao del mes'

class CumplimientoCoop(models.Model):
    ficha = models.ForeignKey(FichaMensualCooperativa, on_delete=models.CASCADE)
    datos = models.IntegerField(choices=CHOICES_CUMPLIMIENTO_MENSUAL)
    durante = models.IntegerField(choices=CHOICES_CICLO_12_MES)
    porcentaje = models.FloatField('%')

    class Meta:
        verbose_name='6. Cumplimiento de la Cooperativa'
        verbose_name_plural='6. Cumplimiento de la Cooperativa'

class CoberturaCoop(models.Model):
    ficha = models.ForeignKey(FichaMensualCooperativa, on_delete=models.CASCADE)
    datos = models.IntegerField(choices=CHOICES_COBERTURA_MENSUAL)
    durante = models.IntegerField('Durante el mes')

    class Meta:
        verbose_name='7. Cobertura de la Cooperativa'
        verbose_name_plural='7. Cobertura de la Cooperativa'


