from django.contrib import admin
from .models import *
# Register your models here.

class InlinePersonalActivo(admin.TabularInline):
    model = PersonalActivo
    extra = 1
    max_num = 11

class InlineProcesosActivos(admin.TabularInline):
    model = ProcesosActivos
    extra = 1
    max_num = 18

class InlineProdManejadoCoopMensual(admin.TabularInline):
    model = ProdManejadoCoopMensual
    extra = 1
    #max_num = 18

class InlineProduccionSocios(admin.TabularInline):
    model = ProduccionSocios
    extra = 1
    max_num = 5

class InlineProduccionProveedores(admin.TabularInline):
    model = ProduccionProveedores
    extra = 1
    max_num = 5

class InlinePlanNegocioCacao(admin.TabularInline):
    model = PlanNegocioCacao
    extra = 1
    max_num = 8

class InlineCumplimientoCoop(admin.TabularInline):
    model = CumplimientoCoop
    extra = 1
    max_num = 5

class InlineCoberturaCoop(admin.TabularInline):
    model = CoberturaCoop
    extra = 1
    max_num = 5

class FichaMensualCooperativaAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(FichaMensualCooperativaAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(FichaMensualCooperativaAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    autocomplete_fields = ['cooperativa','evaluador']
    list_display = ('cooperativa','fecha','evaluador','user')
    list_filter = ('cooperativa__nombre','cooperativa__siglas',
                   'year')
    search_fields = ('cooperativa__nombre','evaluador__nombre')
    inlines = [InlinePersonalActivo,InlineProcesosActivos,
               InlineProdManejadoCoopMensual,InlineProduccionSocios,
               InlineProduccionProveedores,InlinePlanNegocioCacao,
               InlineCumplimientoCoop,InlineCoberturaCoop]


admin.site.register(FichaMensualCooperativa, FichaMensualCooperativaAdmin)