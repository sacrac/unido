
CHOICES_MESES = (

        (1, 'Enero'),
        (2, 'Febrero'),
        (3, 'Marzo'),
        (4, 'Abril'),
        (5, 'Mayo'),
        (6, 'Junio'),
        (7, 'Julio'),
        (8, 'Agosto'),
        (9, 'Septiembre'),
        (10, 'Octubre'),
        (11, 'Noviembre'),
        (12, 'Diciembre'),
    )

CHOICES_CICLO = (

        (1, '2020 (Enero - Diciembre)'),
        (2, '2021 (Enero - Diciembre)'),
        (3, '2022 (Enero - Diciembre)'),
        (4, '2023 (Enero - Diciembre)'),
        (5, '2024 (Enero - Diciembre)'),
        (6, '2025 (Enero - Diciembre)'),

    )


CHOICES_PERSONAL = (

        (1, 'Socios'),
        (2, 'Proveedores'),
        (3, 'Gerentes'),
        (4, 'Contadores'),
        (5, 'Técnicos de campo'),
        (6, 'Acopiadores'),
        (7, 'Trabajadores Centro de Acopio'),
        (8, 'Trabajadores Procesamiento'),
        (9, 'Inspectores internos Certificación'),
        (10, 'Inspectores externos Certificación'),
        (11, 'Agentes de crédito'),
        (12, 'Prestadores de servicio'),

    )

CHOICES_PROCESOS = (

        (1, 'Reunión de Socios'),
        (2, 'Sesiones de la asamblea'),
        (3, 'Sesiones de Junta Directiva'),
        (4, 'Sesiones Junta de Vigilancia'),
        (5, 'Sesiones Comisión de formación'),
        (6, 'Sesiones Comisión de género'),
        (7, 'Sesiones de Gerencia'),
        (8, 'Sesiones administrativas'),
        (9, 'Sesiones técnicas'),
        (10, 'Visitas de parcelas'),
        (11, 'Capacitación a productores/as'),
        (12, 'Inspectoría de certificación'),
        (13, 'Aprobación de créditos'),
        (14, 'Acopio de productos'),
        (15, 'Pago de productos a Socios'),
        (16, 'Entrega a compradores'),
        (17, 'Pago de compradores'),
        (18, 'Pago de excedente a Socios'),

    )


CHOICES_SOCIOS_PRODUCCION_MENSUAL = (

        (1, 'Número de socios varones entregando Cacao'),
        (2, 'Número de socias mujeres entregando Cacao'),
        (3, 'Cantidad de cacao entregada por los socios varones QQ baba'),
        (4, 'Cantidad de cacao entregada por las socias mujeres QQ baba'),

    )

CHOICES_PROVEEDORES_PRODUCCION_MENSUAL = (

        (1, 'Número de proveedores varones entregando Cacao'),
        (2, 'Número de proveedoras mujeres entregando Cacao'),
        (3, 'Cantidad de cacao entregada por los proveedores varones QQ baba'),
        (4, 'Cantidad de cacao entregada por las proveedoras mujeres QQ baba'),

    )


CHOICES_PLAN_NEGOCIO_MENSUAL = (

        (1, 'Volumen mensual de acopio de cacao de socios hombres y mujeres (QQ baba)'),
        (2, 'Volumen mensual de acopio de cacao de proveedores hombres y mujeres (QQ baba)'),
        (3, 'Precio de compra de cacao en baba durante el mes (C$/QQ)'),
        (4, 'Precio de compra de cacao rojo durante el mes (C$/QQ)'),
        (5, 'Volumen mensual de Cacao fermentado vendido a los compradores o exportadores (QQ seco)'),
        (6, 'Volumen mensual de Cacao rojo vendido a compradores o exportadores (QQ seco)'),
        (7, 'Precio de venta de cacao fermentado durante el mes (C$/QQ)'),
        (8, 'Precio de venta de cacao rojo durante el mes (C$/QQ)')

    )

CHOICES_CUMPLIMIENTO_MENSUAL = (

        (1, 'Avance de implementación de plan de trabajo mensual de la Cooperativa'),
        (2, 'Avance de implementación de plan mensual de capacitación'),
        (3, 'Avance de registro de operaciones económicas con de normas de procesos administrativos financieros con sistemas contables'),
        (4, 'Avance de implementación de prácticas para mejorar la calidad del cacao fermentado'),
        (5, 'Avance de implementación de prácticas para mejorar la calidad del cacao rojo'),

    )

CHOICES_CICLO_12_MES = (

        (1, 'No iniciado'),
        (2, 'Iniciado'),
        (3, 'Avanzado'),
        (4, 'Cumplido '),

    )

CHOICES_COBERTURA_MENSUAL = (

        (1, 'Número de productores cubiertos con el plan de visita de Asistencia técnica durante el mes (#)'),
        (2, 'Número total de visitas de Asistencia técnica realizadas durante el año (#)'),
        (3, 'Número de grupos de jóvenes que brindaron servicios a los productores (#)'),
        (4, 'Número de jóvenes varones que brindaron servicios a los productores (#)'),
        (5, 'Número de jóvenes mujeres que brindaron servicios a los productores (#)'),

    )




