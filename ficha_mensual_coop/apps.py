from django.apps import AppConfig


class FichaMensualCoopConfig(AppConfig):
    name = 'ficha_mensual_coop'
    verbose_name = "Ficha Mensual Cooperativas"
