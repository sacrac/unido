from django.contrib import admin
from .models import *

# import import_export
# from import_export import resources
# from import_export.fields import Field
# from import_export.admin import ImportExportModelAdmin
# import tablib
# from datetime import datetime, timezone

class InlineComposicion(admin.TabularInline):
    model = Composicion
    fields = (('adultos_varones', 'adultas_mujeres','jovenes_varones','jovenes_mujeres'),
                     ('adolecentes_varones','adolescentes_mujeres','ninos','ninas'),
                     )
    extra = 1
    max_num = 1

class InlineServiciosBasicos(admin.TabularInline):
    model = ServiciosBasicos
    extra = 1
    max_num = 1

class InlineUsoTierra(admin.TabularInline):
    model = UsoTierra
    extra = 1
    max_num = 8

class InlineTrabajadoresPermanentes(admin.TabularInline):
    model = TrabajadoresPermanentes
    extra = 1
    max_num = 4

class InlineTrabajadoresTemporales(admin.TabularInline):
    model = TrabajadoresTemporales
    extra = 1
    max_num = 4

class InlineTrabajadoresFamiliares(admin.TabularInline):
    model = TrabajadoresFamiliares
    extra = 1
    max_num = 4

class InlineUsoFuerzaTrabajo(admin.TabularInline):
    model = UsoFuerzaTrabajo
    extra = 1

class InlineProduccionFinca(admin.TabularInline):
    model = ProduccionFinca
    extra = 1

class InlineIngresoFinca(admin.TabularInline):
    model = IngresoFinca
    extra = 1

class InlineInventarioSafCacao(admin.TabularInline):
    model = InventarioSafCacao
    extra = 1

class InlineInformacionCacao(admin.TabularInline):
    model = InformacionCacao
    extra = 1

class InlineHistoriaProduccion(admin.TabularInline):
    model = HistoriaProduccion
    extra = 1

class InlineCalidadManejo(admin.TabularInline):
    model = CalidadManejo
    extra = 1
    max_num = 1

class InlineNivelManejo(admin.TabularInline):
    model = NivelManejo
    extra = 1
    max_num = 1

class InlineMesLabores(admin.TabularInline):
    model = MesLabores
    extra = 1
    max_num = 1

class InlineOpcionesAgroecologicas(admin.TabularInline):
    model = OpcionesAgroecologicas
    extra = 1

class InlineCosecha(admin.TabularInline):
    model = Cosecha
    extra = 1
    max_num = 1

class InlineOpcionesCadena(admin.TabularInline):
    model = OpcionesCadena
    extra = 1
    max_num = 1

class InlineDatosCosecha(admin.TabularInline):
    model = DatosCosecha
    extra = 1
    max_num = 4

class InlineConocimientos(admin.TabularInline):
    model = Conocimientos
    extra = 1

class InlineMitigacionRiesgo(admin.StackedInline):
    model = MitigacionRiesgo
    extra = 1
    max_num = 1

# class FichaResource(resources.ModelResource):
#     tecnico = Field(attribute='tecnico', column_name='tecnico')
#     productor = Field(attribute='productor', column_name='productor')
#     user = Field(attribute='user', column_name='usuario')
#     class Meta:
#         model = Ficha

#     def export(self, queryset=None, *args, **kwargs):
#         if queryset is None:
#             queryset = self.get_queryset()
#         ds = tablib.Dataset()
#         data = []
#         for ficha in queryset:
#             for item in ficha.composicion_set.all():
#                 row = {
#                     'ficha_id': ficha.id,
#                     "tecnico": ficha.tecnico.nombre,
#                     "productor": ficha.productor.nombre,
#                     "usuario": ficha.user.username,
#                     "fecha": ficha.fecha.strftime("%d %b %Y"),
#                     "composicion_id": item.id,
#                     "adultos_varones": item.adultos_varones,
#                     "adultas_mujeres": item.adultas_mujeres,
#                     "jovenes_varones": item.jovenes_varones,
#                     "jovenes_mujeres": item.jovenes_mujeres,
#                     "adolecentes_varones": item.adolecentes_varones,
#                     "adolescentes_mujeres": item.adolescentes_mujeres,
#                     "ninos": item.ninos,
#                     "ninas": item.ninas,
#                 }
#                 data.append(row)
#             for item in ficha.serviciosbasicos_set.all():
#                 row1 = {
#                     "servicios_basico_id": item.id,
#                     "energia": item.energia,
#                     "fuente_agua": item.fuente_agua,
#                 }
#                 data.append(row1)
#         ds.dict = data
#         return ds
    # _choice_fields = [
    # 'escalidad','legalidad','sexo','estatus_productor'
    # ]
    # for _field_name in _choice_fields:
    #     locals()[_field_name] = import_export.fields.Field(
    #         attribute='get_%s_display' % _field_name,
    #         column_name=Productor._meta.get_field(_field_name).verbose_name
    #     )
    # pais = Field(attribute='pais', column_name='pais')
    # departamento = Field(attribute='departamento', column_name='departamento')
    # municipio = Field(attribute='municipio', column_name='municipio')
    # comunidad = Field(attribute='comunidad', column_name='comunidad')
    # cooperativa = Field(attribute='cooperativa', column_name='cooperativa')

class FichaAdmin(admin.ModelAdmin):
    #resource_class = FichaResource
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(FichaAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(FichaAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    autocomplete_fields = ['productor','tecnico']
    list_display = ('productor','fecha','tecnico','user')
    list_filter = ('productor__sexo','productor__departamento',
                   'productor__municipio','productor__cooperativa',
                   'year','historiaproduccion__anio')
    search_fields = ('productor__nombre','tecnico__nombre')
    inlines = [InlineComposicion,InlineServiciosBasicos,
                    InlineUsoTierra,InlineTrabajadoresPermanentes,
                    InlineTrabajadoresTemporales,InlineTrabajadoresFamiliares,
                    InlineUsoFuerzaTrabajo,InlineProduccionFinca,
                    InlineIngresoFinca,InlineInventarioSafCacao,
                    InlineInformacionCacao,InlineHistoriaProduccion,
                    InlineCalidadManejo,InlineNivelManejo,
                    InlineMesLabores,InlineOpcionesAgroecologicas,InlineCosecha,
                    InlineOpcionesCadena,InlineDatosCosecha,
                    InlineConocimientos,InlineMitigacionRiesgo]


class TecnicoAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(TecnicoAdmin, self).save_model(request, obj, form, change)

    # def get_queryset(self, request):
    #     qs = super(TecnicoAdmin, self).get_queryset(request)
    #     if request.user.is_superuser:
    #         return qs
    #     return qs.filter(user=request.user)

    search_fields = ('nombre',)
# Register your models here.
admin.site.register(Ficha, FichaAdmin)
admin.site.register(Tecnico, TecnicoAdmin)
