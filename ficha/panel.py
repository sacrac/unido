# -*- coding: utf-8 -*-
from django.shortcuts import render, render_to_response
from django.contrib.auth.decorators import login_required
from django.db.models import Avg, Sum, Count,  Value as V, Case, When, IntegerField, Q
from .models import *
from productores.models import (Productor, CHOICES_ESCOLARIDAD,
                                                            CHOICES_LEGALIDAD)
from catalogos.models import *
from lugar.models import Departamento, Municipio, Comunidad
from cooperativas.models import Cooperativa


@login_required
def panel_lb(request, template="ficha/panel.html"):
    ''' panel de linea base '''
    productores = Productor.objects.all()
    cooperativas = Cooperativa.objects.all()
    query_all = RubrosProduccion.objects.all()
    filtro = Ficha.objects.all()

    total = productores.count()
    hombres = productores.filter(sexo=2).count()
    mujeres = productores.filter(sexo=1).count()

    grafo_familia_cooperativa = {}
    for obj in cooperativas:
        conteo = productores.filter(cooperativa=obj).count()
        grafo_familia_cooperativa[obj.siglas] = conteo

    #Ingresos
    grafo_ingresos = {}
    for coop in cooperativas:
        total_productor = productores.filter(cooperativa=coop).count()
        ingreso_bruto_granos_basicos = 0
        for obj in query_all.filter(categoria=1):
            produccion =  filtro.filter(productor__cooperativa=coop,
                                    produccionfinca__rubros=obj).aggregate(prod=Sum('produccionfinca__cantidad'))['prod'] or 0
            precio = filtro.filter(productor__cooperativa=coop,
                           produccionfinca__rubros=obj, produccionfinca__precio__gt=0).aggregate(precio=Avg('produccionfinca__precio'))['precio'] or 0
            if produccion > 0 and precio > 0:
                ingreso_bruto = produccion * precio
                ingreso_bruto_granos_basicos += ingreso_bruto

        ingreso_bruto_agro_fruta = 0
        for obj in query_all.filter(categoria=2):
            produccion =  filtro.filter(productor__cooperativa=coop,
                                     produccionfinca__rubros=obj).aggregate(prod=Sum('produccionfinca__cantidad'))['prod'] or 0
            precio = filtro.filter(productor__cooperativa=coop,
                                     produccionfinca__rubros=obj, produccionfinca__precio__gt=0).aggregate(precio=Avg('produccionfinca__precio'))['precio'] or 0
            if produccion > 0 and precio > 0:
                ingreso_bruto = produccion * precio
                ingreso_bruto_agro_fruta += ingreso_bruto

        ingreso_bruto_musaceas_raices = 0
        for obj in query_all.filter(categoria=3):
            produccion =  filtro.filter(productor__cooperativa=coop,
                                      produccionfinca__rubros=obj).aggregate(prod=Sum('produccionfinca__cantidad'))['prod'] or 0
            precio = filtro.filter(productor__cooperativa=coop,
                                      produccionfinca__rubros=obj, produccionfinca__precio__gt=0).aggregate(precio=Avg('produccionfinca__precio'))['precio'] or 0
            if produccion > 0 and precio > 0:
                ingreso_bruto = produccion * precio
                ingreso_bruto_musaceas_raices += ingreso_bruto

        ingreso_bruto_ganaderia_mayor = 0
        for obj in query_all.filter(categoria=4):
            produccion =  filtro.filter(productor__cooperativa=coop,
                                      produccionfinca__rubros=obj).aggregate(prod=Sum('produccionfinca__cantidad'))['prod'] or 0
            precio = filtro.filter(productor__cooperativa=coop,
                                      produccionfinca__rubros=obj, produccionfinca__precio__gt=0).aggregate(precio=Avg('produccionfinca__precio'))['precio'] or 0
            if produccion > 0 and precio > 0:
                ingreso_bruto = produccion * precio
                ingreso_bruto_ganaderia_mayor += ingreso_bruto

        ingreso_bruto_anual = ingreso_bruto_granos_basicos + ingreso_bruto_agro_fruta + \
                                            ingreso_bruto_musaceas_raices + ingreso_bruto_ganaderia_mayor

        try:
            agricola_familia =ingreso_bruto_anual / total_productor
        except:
            pass

        try:
            ingreso_granos_basicos_familia = ingreso_bruto_agro_fruta / total_productor
        except:
            pass
        try:
            ingresos_musaceas_familia = ingreso_bruto_musaceas_raices / total_productor
        except:
            pass

        grafo_ingresos[coop.siglas] = [ingreso_bruto_anual,agricola_familia,
                                                              ingreso_bruto_agro_fruta,
                                                              ingreso_granos_basicos_familia,
                                                              ingreso_bruto_musaceas_raices,
                                                              ingresos_musaceas_familia
                                                              ]

    #empleos
    grafo_empleos = {}
    for coop in cooperativas:
        suma_permanentes = 0
        suma_temporal = 0
        suma_familiar = 0
        for obj in CHOICES_TRABAJADORES:
            conteo = filtro.filter(productor__cooperativa=coop,
                            trabajadorespermanentes__categoria=1,trabajadorespermanentes__numeros__gt=0, ).count()
            avg_trabajador = filtro.filter(productor__cooperativa=coop,
                            trabajadorespermanentes__categoria=obj[0]).aggregate(pro=Avg('trabajadorespermanentes__numeros'))['pro'] or 0
            avg_meses = filtro.filter(productor__cooperativa=coop,
                            trabajadorespermanentes__categoria=obj[0]).aggregate(pro=Avg('trabajadorespermanentes__meses'))['pro'] or 0
            if avg_trabajador > 0 and avg_meses > 0:
                empleo = avg_trabajador * avg_meses / 12
                suma_permanentes += empleo

            conteo_t = filtro.filter(productor__cooperativa=coop,
                                trabajadorestemporales__categoria=1,trabajadorestemporales__numeros__gt=0).count()
            avg_trabajador_t = filtro.filter(productor__cooperativa=coop,
                                trabajadorestemporales__categoria=obj[0]).aggregate(pro=Avg('trabajadorestemporales__numeros'))['pro'] or 0
            avg_meses_t = filtro.filter(productor__cooperativa=coop,
                                trabajadorestemporales__categoria=obj[0]).aggregate(pro=Avg('trabajadorestemporales__meses'))['pro'] or 0
            if avg_trabajador_t > 0 and avg_meses_t > 0:
                empleo_t = avg_trabajador_t * avg_meses_t / 312
                suma_temporal += empleo_t

            conteo_f = filtro.filter(productor__cooperativa=coop,
                                trabajadoresfamiliares__categoria=1,trabajadoresfamiliares__numeros__gt=0).count()
            avg_trabajador_f = filtro.filter(productor__cooperativa=coop,
                                trabajadoresfamiliares__categoria=obj[0]).aggregate(pro=Avg('trabajadoresfamiliares__numeros'))['pro'] or 0
            avg_meses_f = filtro.filter(productor__cooperativa=coop,
                                trabajadoresfamiliares__categoria=obj[0]).aggregate(pro=Avg('trabajadoresfamiliares__meses'))['pro'] or 0
            if avg_trabajador_f > 0 and avg_meses_f > 0:
                empleo_f = avg_trabajador_f * avg_meses_f / 12
                suma_familiar += empleo_f


        grafo_empleos[coop.siglas] = [suma_permanentes*conteo,
                                                              suma_temporal*conteo_t,
                                                              suma_familiar*conteo_f,
                                                               ]

    #certificados y productividad
    grafo_certficado = {}
    grafo_productividad_2019 = {}
    grafo_productividad_2018 = {}
    grafo_productividad_2017 = {}
    for coop in cooperativas:
        total_productor = productores.filter(cooperativa=coop).count()
        valor = filtro.filter(productor__cooperativa=coop,
                                        mitigacionriesgo__certificado=1).count()
        try:
            grafo_certficado[coop.siglas] = (valor / total_productor)*100
        except:
            pass

        produccion_2019 = filtro.filter(productor__cooperativa=coop,
                                historiaproduccion__anio=2019).aggregate(t=Sum('historiaproduccion__area_produccion'))['t'] or 0
        produccion_total_2019 = filtro.filter(productor__cooperativa=coop,
                                historiaproduccion__anio=2019).aggregate(t=Sum('historiaproduccion__produccion_kg'))['t'] or 0
        try:
            productividad_2019 = produccion_total_2019 / float(produccion_2019)
        except:
            productividad_2019 = 0
        grafo_productividad_2019[coop.siglas] = productividad_2019

        produccion_2018 = filtro.filter(productor__cooperativa=coop,
                                historiaproduccion__anio=2018).aggregate(t=Sum('historiaproduccion__area_produccion'))['t'] or 0
        produccion_total_2018 = filtro.filter(productor__cooperativa=coop,
                                historiaproduccion__anio=2018).aggregate(t=Sum('historiaproduccion__produccion_kg'))['t'] or 0
        try:
            productividad_2018 = produccion_total_2018 / float(produccion_2018)
        except:
            productividad_2018 = 0
        grafo_productividad_2018[coop.siglas] = productividad_2018

        produccion_2017 = filtro.filter(productor__cooperativa=coop,
                                historiaproduccion__anio=2017).aggregate(t=Sum('historiaproduccion__area_produccion'))['t'] or 0
        produccion_total_2017 = filtro.filter(productor__cooperativa=coop,
                                historiaproduccion__anio=2017).aggregate(t=Sum('historiaproduccion__produccion_kg'))['t'] or 0
        try:
            productividad_2017 = produccion_total_2017 / float(produccion_2017)
        except:
            productividad_2017 = 0
        grafo_productividad_2017[coop.siglas] = productividad_2017

    #areas de cacaos
    grafo_area = {}
    for coop in cooperativas:
        area_total_saf_cacao = filtro.filter(productor__cooperativa=coop).aggregate(a=Sum('inventariosafcacao__area'))['a'] or 0
        area_total_saf_desarrollo = filtro.filter(productor__cooperativa=coop,
                                                        inventariosafcacao__estado_cacao=1).aggregate(a=Sum('inventariosafcacao__area'))['a'] or 0
        area_total_saf_produccion = filtro.filter(productor__cooperativa=coop,
                                                        inventariosafcacao__estado_cacao=2).aggregate(a=Sum('inventariosafcacao__area'))['a'] or 0
        area_total_saf_dp = filtro.filter(productor__cooperativa=coop,
                                                        inventariosafcacao__estado_cacao=3).aggregate(a=Sum('inventariosafcacao__area'))['a'] or 0
        grafo_area[coop.siglas] = (area_total_saf_cacao,area_total_saf_desarrollo,
                                                        area_total_saf_produccion,area_total_saf_dp)

    return render(request,template,locals())
