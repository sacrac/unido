from django.apps import AppConfig


class FichaConfig(AppConfig):
    name = 'ficha'
    verbose_name = "Ficha linea base"
