# -*- coding: utf-8 -*-
from django import forms
#from productores.models import CHOICE_SEXO
from .models import  Ficha, Tecnico
from lugar.models import (Pais, Departamento,
                                                 Municipio, Comunidad)
from cooperativas.models import Cooperativa

CHOICE_SEXO1 = (
    ('', '-------'),
    (1, 'Mujer'),
    (2, 'Hombre')
)

def fecha_choice():
    years = []
    for en in Ficha.objects.order_by('fecha').values_list('fecha', flat=True):
      years.append((en.year,en.year))

    return list(sorted(set(years)))

class ConsultaForm(forms.Form):
    def __init__(self, *args, **kwargs):
          super(ConsultaForm, self).__init__(*args, **kwargs)
          self.fields['fecha'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['productor'].widget.attrs.update({'class': 'form-control'})
          self.fields['departamento'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['municipio'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['comunidad'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['sexo'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['cooperativa'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['tecnico'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
    fecha = forms.ChoiceField(choices=fecha_choice(), label="Años", required=False)
    productor = forms.CharField(max_length=250, required=False)
    departamento = forms.ModelChoiceField(queryset=Departamento.objects.all(), required=False)
    municipio = forms.ModelChoiceField(queryset=Municipio.objects.all(), required=False)
    comunidad = forms.ModelChoiceField(queryset=Comunidad.objects.all(), required=False)
    sexo = forms.ChoiceField(choices=CHOICE_SEXO1, required=False)
    cooperativa = forms.ModelChoiceField(queryset=Cooperativa.objects.all(), required=False)
    tecnico = forms.ModelChoiceField(queryset=Tecnico.objects.all(), required=False)
