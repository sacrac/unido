# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator

from cooperativas.models import Cooperativa
from productores.models import Productor
from catalogos.models import *

import datetime
from multiselectfield import MultiSelectField
# Create your models here.

class Tecnico(models.Model):
    nombre = models.CharField('Nombre del técnico/a que llena ficha', max_length=250)

    user = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Técnico'
        verbose_name_plural = 'Técnicos'

class Ficha(models.Model):
    fecha = models.DateField('Fecha de recolección')
    tecnico = models.ForeignKey(Tecnico, verbose_name='Técnico', on_delete=models.CASCADE)
    productor = models.ForeignKey(Productor, verbose_name='Productor/a', on_delete=models.CASCADE)

    year = models.IntegerField(editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.year = self.fecha.year
        super(Ficha, self).save(*args, **kwargs)

    def __str__(self):
        return self.productor.nombre

    class Meta:
        verbose_name = 'Ficha'
        verbose_name_plural = 'Fichas'

class Composicion(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    adultos_varones = models.IntegerField('Número de adultos varones (más de 30)')
    adultas_mujeres = models.IntegerField('Número de adultas mujeres (más de 30)')
    jovenes_varones = models.IntegerField('Número de Jóvenes varones (17-30años)')
    jovenes_mujeres = models.IntegerField('Número de Jóvenes mujeres (17-30 años)')
    adolecentes_varones = models.IntegerField('Número de adolescentes varones (11-16 años)')
    adolescentes_mujeres = models.IntegerField('Número de adolescentes mujeres (11-16años)')
    ninos = models.IntegerField('Número de niños (1-10 años)')
    ninas = models.IntegerField('Número de niñas (1-10 años)')

    class Meta:
        verbose_name='1.2 Composición del grupo familiar'
        verbose_name_plural='1.2 Composición del grupo familiar'


class ServiciosBasicos(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    energia = models.ManyToManyField(DisponibilidadEnergia)
    fuente_agua = models.ManyToManyField(FuenteAgua)

    class Meta:
        verbose_name='1.3 Servicios Básicos de la finca'
        verbose_name_plural='1.3 Servicios Básicos de la finca'

class UsoTierra(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    tierra = models.IntegerField(choices=CHOICES_TIERRA)
    manzanas = models.FloatField()

    class Meta:
        verbose_name='1.4 Uso de la tierra de la finca'
        verbose_name_plural='1.4 Uso de la tierra de la finca'

class TrabajadoresPermanentes(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    categoria = models.IntegerField(choices=CHOICES_TRABAJADORES)
    numeros = models.IntegerField('Números')
    meses = models.IntegerField('Meses trabajado(meses/trabajador)',
                                                        validators=[MinValueValidator(1), MaxValueValidator(12)])
    pago = models.FloatField('Pago por día de trabajo')

    class Meta:
        verbose_name='2.1-1 Número trabajadores que laboran Permanentemente'
        verbose_name_plural='2.1-1 Número trabajadores que laboran Permanentemente'

class TrabajadoresTemporales(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    categoria = models.IntegerField(choices=CHOICES_TRABAJADORES)
    numeros = models.IntegerField('Números')
    meses = models.IntegerField('Dias trabajado(dias/trabajador)',
                                                        validators=[MinValueValidator(1), MaxValueValidator(312)])
    pago = models.FloatField('Pago por día de trabajo')

    class Meta:
        verbose_name='2.1-2 Número trabajadores que laboran Temporalmente'
        verbose_name_plural='2.1-2 Número trabajadores que laboran Temporalmente'

class TrabajadoresFamiliares(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    categoria = models.IntegerField(choices=CHOICES_TRABAJADORES)
    numeros = models.IntegerField('Números')
    meses = models.IntegerField('Meses trabajado(meses/trabajador)',
                                                        validators=[MinValueValidator(1), MaxValueValidator(12)])
    pago = models.FloatField('Pago por día de trabajo')

    class Meta:
        verbose_name='2.1-3 Número trabajadores Familiares'
        verbose_name_plural='2.1-3 Número trabajadores Familiares'

class UsoFuerzaTrabajo(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    actividades = models.ForeignKey(Actvidades, on_delete=models.CASCADE)
    contratados = models.IntegerField('Trabajadores contratados', choices=CHOICES_SI_NO)
    hombres = models.IntegerField('Hombres Adultos', choices=CHOICES_SI_NO)
    mujeres = models.IntegerField('Mujeres Adultas', choices=CHOICES_SI_NO)
    hombres_jovenes = models.IntegerField('Hombres jóvenes', choices=CHOICES_SI_NO)
    mujeres_jovenes = models.IntegerField('Mujeres jóvenes', choices=CHOICES_SI_NO)

    class Meta:
        verbose_name='2.2 Uso de fuerza de trabajo en SAF Cacao'
        verbose_name_plural='2.2 Uso de fuerza de trabajo en SAF Cacao'

class ProduccionFinca(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    rubros = models.ForeignKey(RubrosProduccion, on_delete=models.CASCADE)
    area = models.FloatField('Área o cantidad')
    cantidad = models.FloatField('Cantidad producción anual')
    unidad = models.IntegerField(choices=CHOICES_UNIDAD_MEDIDA_PRODUCCION)
    precio = models.FloatField('Precio/unidad de medida en C$')
    destino = models.IntegerField(choices=CHOICES_DESTINO_PRODUCCION)

    class Meta:
        verbose_name='3.1 Producción e ingreso de la finca'
        verbose_name_plural='3.1 Producción e ingreso de la finca'

class IngresoFinca(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    rubros = models.ForeignKey(RubrosIngresos, on_delete=models.CASCADE)
    cantidad = models.FloatField('cantidad')
    tiempo = models.IntegerField(choices=CHOICES_MESES)
    ingreso = models.FloatField('Ingreso C$/ por mes')

    class Meta:
        verbose_name='3.2 Otros ingreso de la finca'
        verbose_name_plural='3.2 Otros ingreso de la finca'

class InventarioSafCacao(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    plantio = models.CharField('Nombre de plantio SAF', max_length=150)
    area = models.FloatField('Área en mz')
    numero_semilla = models.IntegerField('Número de plantas de cacao por semilla')
    numero_injerto = models.IntegerField('Número de plantas injertadas de cacao')
    numero_musaceas = models.IntegerField('Número  Musáceas')
    numero_frutales = models.IntegerField('Número Frutales')
    numero_maderables = models.IntegerField('Número Maderables')
    numero_arboles = models.IntegerField('Número de árboles de servicio')
    estado_cacao = models.IntegerField(choices=CHOICES_ESTADO)
    anios = models.FloatField('Edad en años')
    produccion = models.FloatField('Producción de cacao último ciclo qq baba')
    sombra = models.FloatField("% sombra")

    class Meta:
        verbose_name='4.1 Inventario e información SAF Cacao'
        verbose_name_plural='4.1 Inventario e información SAF Cacao'

class InformacionCacao(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    plantio = models.CharField('Nombre de plantio SAF', max_length=150)
    area = models.FloatField('Área en mz')
    numero_produccion = models.IntegerField('Número total de plantas de cacao en producción')
    numero_desarrollo = models.IntegerField('Número de plantas en desarrollo')
    numero_faltantes = models.IntegerField('Número de plantas faltantes')
    numero_frutos_91 = models.IntegerField('Número de plantas de cacao con  más de 91 frutos por año')
    numero_frutos_51 = models.IntegerField('Número de plantas de cacao de 51 a 90 frutos por año')
    numero_frutos_31 = models.IntegerField('Número de plantas de cacao de 31 a 50 frutos por año')
    numero_frutos_0 = models.IntegerField('Número de plantas de cacao de 0 a 30 frutos por año')

    class Meta:
        verbose_name='4.2 Información estado productivo cacao'
        verbose_name_plural='4.2 Información estado productivo cacao'

class HistoriaProduccion(models.Model):
    ANO_CHOICES_HISTORIA=[]
    for i in range (datetime.date.today().year,2016,-1):
        ANO_CHOICES_HISTORIA.append((i,i))
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    anio = models.IntegerField(choices=ANO_CHOICES_HISTORIA)
    area_total = models.FloatField('Área total de cacao en mz')
    area_desarrollo = models.FloatField('Área de cacao en desarrollo en mz')
    area_produccion = models.FloatField('Área de cacao en producción en mz')
    #unidad = models.IntegerField(choices=CHOICE_UNIDAD_TIERRA2, verbose_name='Unidad de medida')
    produccion_baba = models.FloatField('Producción total de cacao en qq baba')
    produccion_seco = models.FloatField('Producción total de cacao en qq seco')

    produccion_kg = models.FloatField(editable=False, null=True, blank=True) #quintal

    def save(self, *args, **kwargs):
        self.produccion_kg = (self.produccion_baba * 0.363) + (self.produccion_seco)
        super(HistoriaProduccion, self).save(*args, **kwargs)

    class Meta:
        verbose_name='4.3 Historia de producción Cacaotera'
        verbose_name_plural='4.3 Historia de producción Cacaotera'

class CalidadManejo(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    poda = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Poda de cacao')
    sombra = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de sombra' )
    maleza = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de malezas')
    fertilidad = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de Fertilidad de Suelo')
    enfermedad = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de Enfermeda des (Hongos)')
    plagas_insectos = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de Plagas (Insectos)')
    plagas_nematodos = models.IntegerField(choices=CHOICES_CALIDAD,verbose_name='Manejo de Plagas (Nematodos)')

    class Meta:
        verbose_name_plural = '4.4 .1- Calidad de manejo'

class NivelManejo(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    poda = models.IntegerField(choices=CHOICES_USO_MANEJO, verbose_name='Poda de cacao')
    sombra = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de sombra' )
    maleza = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de malezas')
    fertilidad = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de Fertilidad de Suelo')
    enfermedad = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de Enfermeda des (Hongos)')
    plagas_insectos = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de Plagas (Insectos)')
    plagas_nematodos = models.IntegerField(choices=CHOICES_USO_MANEJO,verbose_name='Manejo de Plagas (Nematodos)')

    class Meta:
        verbose_name_plural = '4.4 .2- Nivel de uso de manejo'


class MesLabores(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    poda = MultiSelectField(choices=CHOICE_MESES,verbose_name='Poda de cacao', null=True, blank=True)
    sombra = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de sombra', null=True, blank=True)
    maleza = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de malezas', null=True, blank=True)
    fertilidad = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de Fertilidad de Suelo', null=True, blank=True)
    enfermedad = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de Enfermeda des (Hongos)', null=True, blank=True)
    plagas_insectos = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de Plagas (Insectos)', null=True, blank=True)
    plagas_nematodos = MultiSelectField(choices=CHOICE_MESES,verbose_name='Manejo de Plagas (Nematodos)', null=True, blank=True)

    class Meta:
        verbose_name_plural = '4.4 .3- Mes que realizaron los labores'

class OpcionesAgroecologicas(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    opciones = models.IntegerField(choices=CHOICES_OPCIONES_AGRO)
    nivel = models.IntegerField(choices=CHOICES_ESCALAS)

    class Meta:
        verbose_name_plural = '5. Uso de opciones agroecológicas que implementa en SAF Cacao'

class Cosecha(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    cortes = models.IntegerField(choices=CHOICES_CORTES,
                                                        verbose_name='¿Cómo realizaron los cortes?')
    separan_mazorca = models.IntegerField(choices=CHOICES_SEPARAN_MAZORCA,
                                        verbose_name='¿Separan diferentes tipos de mazorcas?')
    quiebran_mazorca = models.IntegerField(choices=CHOICES_QUIEBRAN_MAZORCA,
                                                                            verbose_name='¿Cómo quiebran las mazorcas?')
    calidad  = models.IntegerField(choices=CHOICES_SI_NO,
                                                            verbose_name='¿Conoce la calidad de su cacao?')
    determina_calidad = models.CharField('¿Quién determino la calidad de su cacao?',
                                                                        max_length=250, null=True, blank=True)
    precio  = models.IntegerField(choices=CHOICES_SI_NO,
                                                        verbose_name='¿Reciben un sobre precio para su cacao?')
    cuanto = models.FloatField('¿Cuánto sobre precio recibe por qq baba?', null=True, blank=True)

    class Meta:
        verbose_name_plural = '6.1 Cosecha'

class OpcionesCadena(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    opcion = MultiSelectField(choices=CHOICES_OPCION_CADENA)

    class Meta:
        verbose_name_plural = '6.2 Comercialización'

class DatosCosecha(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    opcion = models.IntegerField(choices=CHOICES_COMERCIALIZACION)
    cantidad_total = models.FloatField('Cantidad en qq Total')
    intermediario = models.FloatField('Intermediarios Cantidad en qq')
    intermediario_precio = models.FloatField('Intermediarios Precio pagado por qq')
    coop_cantidad = models.FloatField('Cooperativa/Empresa Cantidad en qq')
    coop_precio = models.FloatField('Cooperativa/Empresa Precio pagado en qq')

    class Meta:
        verbose_name_plural = '6.3 Datos de cosecha anual de último ciclo'

class Conocimientos(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    temas = models.ForeignKey(Temas, on_delete=models.CASCADE)
    nivel_conocimiento = models.IntegerField(choices=CHOICES_TEMAS_CONOCIMIENTO)
    habilidad_tema = models.IntegerField(choices=CHOICES_TEMAS_CONOCIMIENTO)

    class Meta:
        verbose_name_plural = '7 Conocimientos y habilidades que dispone para el cultivo de cacao'

class MitigacionRiesgo(models.Model):
    ficha = models.ForeignKey(Ficha, on_delete=models.CASCADE)
    monitoreo_plaga = models.IntegerField(choices=CHOICES_SI_NO, verbose_name='¿Realiza monitoreo de plagas y enfermedades?')
    cada_cuanto = models.IntegerField(choices=CHOICES_CADA_CUANTO,
                                verbose_name='¿Cada cuánto realiza monitoreo de plagas y enfermedades?',
                                null=True, blank=True)
    como_realiza = models.IntegerField(choices=CHOICES_COMO_REALIZA,
                                verbose_name='¿Cómo realiza monitoreo de plagas y enfermedades?',
                                null=True, blank=True)
    lleva_registro = models.IntegerField(choices=CHOICES_LLEVA_REGISTROS,
                                verbose_name='¿Si lleva registro de monitoreo de plagas y enfermedades?',
                                null=True, blank=True)
    cuenta_recursos = models.IntegerField(choices=CHOICES_SI_NO,
                                        verbose_name='¿Cuenta con suficiente recursos para manejo de finca?')
    falta_recurso = MultiSelectField(choices=CHOICES_FALTA_RECURSO,
                                verbose_name='¿Para qué cosas hace falta los recursos?',
                                null=True, blank=True)
    obras_almacenamiento = models.IntegerField(choices=CHOICES_SI_NO,
                                        verbose_name='¿Cuenta con obras para almacenamiento de agua?')
    venta_organizada = models.IntegerField(choices=CHOICES_SI_NO,
                                        verbose_name='¿Vende su Cacao en forma organizada?')
    contrato_venta = models.IntegerField(choices=CHOICES_SI_NO,
                                        verbose_name='¿Cuenta con un contrato para la venta de cacao?')
    certificado = models.IntegerField(choices=CHOICES_SI_NO,
                                        verbose_name='¿Está certificado los cacaotales?')
    tipo_certificado = MultiSelectField(choices=CHOICES_TIPO_CERTIFICADO,
                                verbose_name='¿Qué tipo de certificación?')
    calidad_cacao = models.IntegerField(choices=CHOICES_SI_NO,
                                        verbose_name='¿La calidad de su cacao en reconocida y monitoreada?')
    plan_manejo = models.IntegerField(choices=CHOICES_SI_NO,
                                        verbose_name='¿Cuenta con plan de manejo de la finca?')
    plan_negocio = models.IntegerField(choices=CHOICES_SI_NO,
                                        verbose_name='¿Cuenta con un plan de negocio para el cultivo de cacao?')
    plan_inversion = models.IntegerField(choices=CHOICES_SI_NO,
                                        verbose_name='¿Cuenta con un plan de inversión para el cultivo de cacao?')
    apoya_planes = MultiSelectField(choices=CHOICES_APOYA_PLANES,
                                verbose_name='¿Quién apoyo para elaborar estos planes?')

    class Meta:
        verbose_name_plural = '8 Mitigación de los riesgos que aplica'
