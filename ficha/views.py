# -*- coding: utf-8 -*-
from django.shortcuts import render, render_to_response
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.db.models import Avg, Sum, Count,  Value as V
from django.db.models.functions import Coalesce
from django.core.exceptions import ViewDoesNotExist

import json as simplejson
from collections import OrderedDict
import numpy as np

from .forms import ConsultaForm
from .models import *
from productores.models import (Productor, CHOICES_ESCOLARIDAD,
                                CHOICES_LEGALIDAD, UserExtend)
from catalogos.models import *
from lugar.models import Departamento, Municipio, Comunidad
from cooperativas.models import Cooperativa

# Create your views here.

def _queryset_filtrado(request):
    params = {}

    if 'fecha' in request.session:
        params['year'] = request.session['fecha']

    if 'productor' in request.session:
        params['productor__nombre'] = request.session['productor']

    if 'departamento' in request.session:
        params['productor__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['productor__municipio'] = request.session['municipio']

    if 'comunidad' in request.session:
        params['productor__comunidad'] = request.session['comunidad']

    if 'sexo' in request.session:
        params['productor__sexo'] = request.session['sexo']

    if 'cooperativa' in request.session:
        params['productor__cooperativa'] = request.session['cooperativa']

    if 'tecnico' in request.session:
        params['tecnico'] = request.session['tecnico']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    return Ficha.objects.filter(**params)


@login_required
def consulta(request, template='ficha/consulta.html'):
    conteo_encuesta = 0
    if request.method == 'POST':
        form = ConsultaForm(request.POST)
        if form.is_valid():
            request.session['fecha'] = form.cleaned_data['fecha']
            request.session['productor'] = form.cleaned_data['productor']
            request.session['departamento'] = form.cleaned_data['departamento']
            request.session['municipio'] = form.cleaned_data['municipio']
            request.session['comunidad'] = form.cleaned_data['comunidad']
            request.session['sexo'] = form.cleaned_data['sexo']
            request.session['cooperativa'] = form.cleaned_data['cooperativa']
            request.session['tecnico'] = form.cleaned_data['tecnico']
            centinela = 1
            filtro = _queryset_filtrado(request)
            conteo_encuesta = filtro.count()

        else:
            centinela = 0
    else:
        form = ConsultaForm()
        centinela = 0

        if 'fecha' in request.session:
            try:
                del request.session['fecha']
                del request.session['productor']
                del request.session['departamento']
                del request.session['municipio']
                del request.session['comunidad']
                del request.session['sexo']
                del request.session['cooperativa']
                del request.session['tecnico']
            except:
                pass

    return render(request, template, {'form': form, 'centinela': centinela,
                                    'conteo_encuesta':conteo_encuesta})

#salidas educacion
@login_required
def composicion(request, template='ficha/composicion_familiar.html'):
    filtro1 = _queryset_filtrado(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro =filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    #grafico de educacion
    grafo_educacion={}
    for obj in CHOICES_ESCOLARIDAD:
        conteo = filtro.filter(productor__escalidad=obj[0]).count()
        grafo_educacion[obj[1]] = conteo

    #grafico de legalidad de la tierra
    grafo_legalidad={}
    for obj in CHOICES_LEGALIDAD:
        conteo = filtro.filter(productor__legalidad=obj[0]).count()
        grafo_legalidad[obj[1]] = conteo

    #grafico energia
    grafo_energia={}
    for obj in DisponibilidadEnergia.objects.all():
        conteo = filtro.filter(serviciosbasicos__energia=obj).count()
        grafo_energia[obj.nombre] = conteo

    #grafico agua
    grafo_agua={}
    for obj in FuenteAgua.objects.all():
        conteo = filtro.filter(serviciosbasicos__fuente_agua=obj).count()
        grafo_agua[obj.nombre] = conteo

    #composcion de la familia
    conteo_adultos_varones = filtro.aggregate(valor=Avg('composicion__adultos_varones'))['valor'] or 0
    conteo_adultos_mujeres = filtro.aggregate(valor=Avg('composicion__adultas_mujeres'))['valor'] or 0
    conteo_varones_jovenes = filtro.aggregate(valor=Avg('composicion__jovenes_varones'))['valor'] or 0
    conteo_mujeres_jovenes = filtro.aggregate(valor=Avg('composicion__jovenes_mujeres'))['valor'] or 0
    conteo_adolecentes_varones = filtro.aggregate(valor=Avg('composicion__adolecentes_varones'))['valor'] or 0
    conteo_adolecentes_mujeres = filtro.aggregate(valor=Avg('composicion__adolescentes_mujeres'))['valor'] or 0
    conteo_ninos = filtro.aggregate(valor=Avg('composicion__ninos'))['valor'] or 0
    conteo_ninas = filtro.aggregate(valor=Avg('composicion__ninas'))['valor'] or 0

    total_familias = conteo_adultos_varones + conteo_adultos_mujeres + conteo_varones_jovenes + \
                                conteo_mujeres_jovenes + conteo_adolecentes_varones + conteo_adolecentes_mujeres + \
                                conteo_ninos + conteo_ninas
    try:
        dependencia = (conteo_adolecentes_varones + conteo_adolecentes_mujeres +  conteo_ninos + conteo_ninas) / (conteo_adultos_varones + conteo_adultos_mujeres + conteo_varones_jovenes + conteo_mujeres_jovenes)
    except:
        dependencia = 0
    return render(request, template, locals())

@login_required
def tierra(request, template='ficha/tierra.html'):
    filtro1 = _queryset_filtrado(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro =filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    total_familiar = filtro.count()
    total_manzanas_familias = UsoTierra.objects.filter(ficha__in=filtro).exclude(tierra=1).aggregate(t=Sum('manzanas'))['t']
    total_manzanas_lista = UsoTierra.objects.filter(ficha__in=filtro, tierra=1).values_list('manzanas', flat=True)

    tabla_uso_tierra= OrderedDict()
    for obj in CHOICES_TIERRA[1:]:
        query = filtro.filter(usotierra__tierra = obj[0])
        conteo = query.count()
        porcentaje = saca_porcentajes(conteo, total_familiar)
        manzana = query.aggregate(t=Sum('usotierra__manzanas'))['t']
        porcentaje_mz = saca_porcentajes(manzana,total_manzanas_familias)
        tabla_uso_tierra[obj[1]] = (conteo, porcentaje,manzana,porcentaje_mz)

    total= 0
    porcentaje = 0
    for k, v in tabla_uso_tierra.items():
        if v[2] is not None:
            total += v[2]
            porcentaje += v[3]

    promedio_ha = np.mean(total_manzanas_lista)

    #calculando los promedios
    lista = []
    cero = 0
    rango1 = 0
    rango2 = 0
    rango3 = 0
    rango4 = 0
    rango5 = 0
    for x in filtro:
        query = UsoTierra.objects.filter(ficha=x, tierra=1).aggregate(AreaSuma=Coalesce(Sum('manzanas'),V(0)))
        lista.append([x.id,query])

    for nose in lista:
        if nose[1]['AreaSuma'] == 0:
            cero += 1
        if nose[1]['AreaSuma'] >= 0.1 and  nose[1]['AreaSuma'] <= 10:
            rango1 += 1
        if nose[1]['AreaSuma'] >= 11 and nose[1]['AreaSuma'] <= 25:
            rango2 += 1
        if nose[1]['AreaSuma'] >= 26 and nose[1]['AreaSuma'] <= 35:
            rango3 += 1
        if nose[1]['AreaSuma'] >= 36 and nose[1]['AreaSuma'] <= 45:
            rango4 += 1
        if nose[1]['AreaSuma'] >=46:
            rango5 += 1
    total_rangos = cero + rango1 + rango2 + rango3 + rango4 + rango5
    por_cero = round(saca_porcentajes(cero,total_rangos),2)
    por_rango1 = round(saca_porcentajes(rango1,total_rangos),2)
    por_rango2 = round(saca_porcentajes(rango2,total_rangos),2)
    por_rango3 = round(saca_porcentajes(rango3,total_rangos),2)
    por_rango4 = round(saca_porcentajes(rango4,total_rangos),2)
    por_rango5 = round(saca_porcentajes(rango5,total_rangos),2)
    total_porcentajes = round((por_cero + por_rango1 + por_rango2 + por_rango3 + por_rango4 + por_rango5),1)

    bosque_p = filtro.filter(usotierra__tierra=2).aggregate(t=Coalesce(Sum('usotierra__manzanas'), V(0)))['t']
    bosque_s = filtro.filter(usotierra__tierra=3).aggregate(t=Coalesce(Sum('usotierra__manzanas'), V(0)))['t']
    c_anuales = filtro.filter(usotierra__tierra=4).aggregate(t=Coalesce(Sum('usotierra__manzanas'), V(0)))['t']
    p_forestal = filtro.filter(usotierra__tierra=5).aggregate(t=Coalesce(Sum('usotierra__manzanas'), V(0)))['t']
    a_abiertos = filtro.filter(usotierra__tierra=6).aggregate(t=Coalesce(Sum('usotierra__manzanas'), V(0)))['t']
    a_arboles = filtro.filter(usotierra__tierra=7).aggregate(t=Coalesce(Sum('usotierra__manzanas'), V(0)))['t']
    perennes = filtro.filter(usotierra__tierra=8).aggregate(t=Coalesce(Sum('usotierra__manzanas'), V(0)))['t']

    try:
        cobertura_arboles = ((bosque_p * 1) + (bosque_s * 0.5) + (c_anuales * 0) + \
                                          (p_forestal * 0.8) + (a_abiertos * 0) + (a_arboles * 0.3) + \
                                          (perennes * 0.4)) /  total_manzanas_familias
    except:
        cobertura_arboles = 0
    try:
        biodiversidad = ((bosque_p * 0.9) + (bosque_s * 0.6) + (c_anuales * 0) + \
                                          (p_forestal * 0.6) + (a_abiertos * 0.2) + (a_arboles * 0.4) + \
                                          (perennes * 0.6)) /  total_manzanas_familias
    except:
        biodiversidad = 0
    try:
        carbono = ((bosque_p * 1) + (bosque_s * 0.8) + (c_anuales * 0) + \
                                          (p_forestal * 0.7) + (a_abiertos * 0.4) + (a_arboles * 0.7) + \
                                          (perennes * 0.7)) /  total_manzanas_familias
    except:
        carbono = 0
    try:
        indice_suelo = ((bosque_p * 1) + (bosque_s * 1) + (c_anuales * 0.2) + \
                                          (p_forestal * 0.9) + (a_abiertos * 0.4) + (a_arboles * 0.6) + \
                                          (perennes * 0.9)) /  total_manzanas_familias
    except:
        indice_suelo = 0
    return render(request, template, {'tabla_uso_tierra':tabla_uso_tierra,
                                                                'total_manzanas_familias':total,
                                                                'total_porcentaje': porcentaje,
                                                                'promedio_ha': promedio_ha,
                                                                'cero':cero,
                                                                'por_cero':por_cero,
                                                                'rango1':rango1,
                                                                'por_rango1':por_rango1,
                                                                'rango2':rango2,
                                                                'por_rango2':por_rango2,
                                                                'rango3':rango3,
                                                                'por_rango3':por_rango3,
                                                                'rango4':rango4,
                                                                'por_rango4':por_rango4,
                                                                'rango5':rango5,
                                                                'por_rango5':por_rango5,
                                                                'total_rangos':total_rangos,
                                                                'total_porcentajes': total_porcentajes,
                                                                'cobertura_arboles':cobertura_arboles,
                                                                'biodiversidad':biodiversidad,
                                                                'carbono':carbono,
                                                                'indice_suelo':indice_suelo,
                                                                 'num_familias': filtro.count()})

@login_required
def opcion_agroecologica(request, template='ficha/opcion_agro.html'):
    filtro1 = _queryset_filtrado(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro =filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    tabla_opcion_agro = OrderedDict()
    for obj in CHOICES_OPCIONES_AGRO:
        query = filtro.filter(opcionesagroecologicas__opciones=obj[0])
        no_utiliza = query.filter(opcionesagroecologicas__opciones=obj[0],
                            opcionesagroecologicas__nivel=1).count()
        pequena_escala = query.filter(opcionesagroecologicas__opciones=obj[0],
                            opcionesagroecologicas__nivel=2).count()
        mayor_escala = query.filter(opcionesagroecologicas__opciones=obj[0],
                            opcionesagroecologicas__nivel=3).count()
        toda_finca = query.filter(opcionesagroecologicas__opciones=obj[0],
                            opcionesagroecologicas__nivel=4).count()
        try:
            por_mayor_escala = mayor_escala / float(filtro.count()) * 100
        except:
            por_mayor_escala= 0
        try:
            por_toda_finca = toda_finca / float(filtro.count()) * 100
        except:
            por_toda_finca = 0
        suma_total_porcentaje = por_mayor_escala + por_toda_finca
        tabla_opcion_agro[obj[1]] = (no_utiliza,pequena_escala,mayor_escala,toda_finca, suma_total_porcentaje)

    return render(request, template, {'tabla_opcion_agro':tabla_opcion_agro,
                                        'num_familias': filtro.count()})

@login_required
def mitigacion(request, template='ficha/mitigacion.html'):
    filtro1 = _queryset_filtrado(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro =filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    tabla_monitorio_plaga = {}
    for obj in CHOICES_SI_NO:
        valor = filtro.filter(mitigacionriesgo__monitoreo_plaga=obj[0]).count()
        tabla_monitorio_plaga[obj[1]] = valor

    tabla_monitorio_cada_cuanto = {}
    for obj in CHOICES_CADA_CUANTO:
        valor = filtro.filter(mitigacionriesgo__cada_cuanto=obj[0]).count()
        tabla_monitorio_cada_cuanto[obj[1]] = valor

    tabla_monitorio_realiza = {}
    for obj in CHOICES_COMO_REALIZA:
        valor = filtro.filter(mitigacionriesgo__como_realiza=obj[0]).count()
        tabla_monitorio_realiza[obj[1]] = valor

    tabla_lleva_registro = {}
    for obj in CHOICES_LLEVA_REGISTROS:
        valor = filtro.filter(mitigacionriesgo__lleva_registro=obj[0]).count()
        tabla_lleva_registro[obj[1]] = valor

    tabla_suficiente_recurso = {}
    for obj in CHOICES_SI_NO:
        valor = filtro.filter(mitigacionriesgo__cuenta_recursos=obj[0]).count()
        tabla_suficiente_recurso[obj[1]] = valor

    tabla_falta_recurso = {}
    for obj in CHOICES_FALTA_RECURSO:
        valor = filtro.filter(mitigacionriesgo__falta_recurso__contains=obj[0]).count()
        tabla_falta_recurso[obj[1]] = valor

    tabla_obra_almacenamiento = {}
    for obj in CHOICES_SI_NO:
        valor = filtro.filter(mitigacionriesgo__obras_almacenamiento=obj[0]).count()
        tabla_obra_almacenamiento[obj[1]] = valor

    tabla_venta_organizada = {}
    for obj in CHOICES_SI_NO:
        valor = filtro.filter(mitigacionriesgo__venta_organizada=obj[0]).count()
        tabla_venta_organizada[obj[1]] = valor

    tabla_contrato_venta = {}
    for obj in CHOICES_SI_NO:
        valor = filtro.filter(mitigacionriesgo__contrato_venta=obj[0]).count()
        tabla_contrato_venta[obj[1]] = valor

    tabla_certificado = {}
    for obj in CHOICES_SI_NO:
        valor = filtro.filter(mitigacionriesgo__certificado=obj[0]).count()
        tabla_certificado[obj[1]] = valor

    tabla_tipo_certificado = {}
    for obj in CHOICES_TIPO_CERTIFICADO:
        valor = filtro.filter(mitigacionriesgo__tipo_certificado__contains=obj[0]).count()
        tabla_tipo_certificado[obj[1]] = valor

    tabla_calidad_cacao = {}
    for obj in CHOICES_SI_NO:
        valor = filtro.filter(mitigacionriesgo__calidad_cacao=obj[0]).count()
        tabla_calidad_cacao[obj[1]] = valor

    tabla_plan_manejo = {}
    for obj in CHOICES_SI_NO:
        valor = filtro.filter(mitigacionriesgo__plan_manejo=obj[0]).count()
        tabla_plan_manejo[obj[1]] = valor

    tabla_plan_negocio = {}
    for obj in CHOICES_SI_NO:
        valor = filtro.filter(mitigacionriesgo__plan_negocio=obj[0]).count()
        tabla_plan_negocio[obj[1]] = valor

    tabla_plan_inversion = {}
    for obj in CHOICES_SI_NO:
        valor = filtro.filter(mitigacionriesgo__plan_inversion=obj[0]).count()
        tabla_plan_inversion[obj[1]] = valor

    tabla_apoya_planes = {}
    for obj in CHOICES_APOYA_PLANES:
        valor = filtro.filter(mitigacionriesgo__apoya_planes__contains=obj[0]).count()
        tabla_apoya_planes[obj[1]] = valor


    return render(request, template, {'tabla_monitorio_plaga':tabla_monitorio_plaga,
                                                                'tabla_monitorio_cada_cuanto':tabla_monitorio_cada_cuanto,
                                                                'tabla_monitorio_realiza':tabla_monitorio_realiza,
                                                                'tabla_lleva_registro':tabla_lleva_registro,
                                                                'tabla_suficiente_recurso':tabla_suficiente_recurso,
                                                                'tabla_falta_recurso':tabla_falta_recurso,
                                                                'tabla_obra_almacenamiento':tabla_obra_almacenamiento,
                                                                'tabla_venta_organizada':tabla_venta_organizada,
                                                                'tabla_contrato_venta':tabla_contrato_venta,
                                                                'tabla_certificado':tabla_certificado,
                                                                'tabla_tipo_certificado':tabla_tipo_certificado,
                                                                'tabla_calidad_cacao':tabla_calidad_cacao,
                                                                'tabla_plan_manejo':tabla_plan_manejo,
                                                                'tabla_plan_negocio':tabla_plan_negocio,
                                                                'tabla_plan_inversion':tabla_plan_inversion,
                                                                'tabla_apoya_planes':tabla_apoya_planes,
                                                                'num_familias': filtro.count()})

@login_required
def conocimiento_habilidades(request, template='ficha/conocimientos.html'):
    filtro1 = _queryset_filtrado(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro =filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    tabla_conocimiento = OrderedDict()
    for obj in Temas.objects.all():
        pobre = filtro.filter(conocimientos__temas=obj,
                                         conocimientos__nivel_conocimiento=1).count()
        algo = filtro.filter(conocimientos__temas=obj,
                                         conocimientos__nivel_conocimiento=2).count()
        regular = filtro.filter(conocimientos__temas=obj,
                                         conocimientos__nivel_conocimiento=3).count()
        bueno = filtro.filter(conocimientos__temas=obj,
                                         conocimientos__nivel_conocimiento=4).count()
        excelente = filtro.filter(conocimientos__temas=obj,
                                         conocimientos__nivel_conocimiento=5).count()

        tabla_conocimiento[obj] = (pobre,algo,regular,bueno,excelente)

    tabla_habilidad = OrderedDict()
    for obj in Temas.objects.all():
        pobre = filtro.filter(conocimientos__temas=obj,
                                         conocimientos__habilidad_tema=1).count()
        algo = filtro.filter(conocimientos__temas=obj,
                                         conocimientos__habilidad_tema=2).count()
        regular = filtro.filter(conocimientos__temas=obj,
                                         conocimientos__habilidad_tema=3).count()
        bueno = filtro.filter(conocimientos__temas=obj,
                                         conocimientos__habilidad_tema=4).count()
        excelente = filtro.filter(conocimientos__temas=obj,
                                         conocimientos__habilidad_tema=5).count()

        tabla_habilidad[obj] = (pobre,algo,regular,bueno,excelente)

    return render(request, template, {'tabla_conocimiento':tabla_conocimiento,
                                                                'tabla_habilidad':tabla_habilidad,
                                                                'num_familias': filtro.count()})

@login_required
def comercializacion(request, template='ficha/comercializacion.html'):
    filtro1 = _queryset_filtrado(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro =filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    tabla_cadena = OrderedDict()
    for obj in CHOICES_OPCION_CADENA:
        conteo = filtro.filter(opcionescadena__opcion__icontains=obj[0]).count()
        tabla_cadena[obj[1]] = conteo

    familias_totales = filtro.filter(datoscosecha__cantidad_total__gte=0.1).distinct().count()
    familias_produccen_cacao = 0
    tabla_comercializacion = {}
    for obj in CHOICES_COMERCIALIZACION:
        familias =  filtro.filter(datoscosecha__opcion=obj[0], datoscosecha__cantidad_total__gte=0.1).count()
        familias_produccen_cacao += familias
        cantidad_total = filtro.filter(datoscosecha__opcion=obj[0]).aggregate(t=Sum('datoscosecha__cantidad_total'))['t'] or 0
        intermediario = filtro.filter(datoscosecha__opcion=obj[0]).aggregate(t=Sum('datoscosecha__intermediario'))['t'] or 0
        inter_precio = filtro.filter(datoscosecha__opcion=obj[0],
                                                    datoscosecha__cantidad_total__gte=0.1,
                                                    datoscosecha__intermediario_precio__gte=0.1).aggregate(t=Avg('datoscosecha__intermediario_precio'))['t'] or 0
        coop_cantidad = filtro.filter(datoscosecha__opcion=obj[0]).aggregate(t=Sum('datoscosecha__coop_cantidad'))['t'] or 0
        coop_precio = filtro.filter(datoscosecha__opcion=obj[0],
                                                    datoscosecha__cantidad_total__gte=0.1,
                                                    datoscosecha__coop_precio__gte=0.1).aggregate(t=Avg('datoscosecha__coop_precio'))['t'] or 0
        if cantidad_total > 0:
            formula1 = (intermediario * inter_precio) + (coop_cantidad * coop_precio)
            tabla_comercializacion[obj[1]] = (familias,cantidad_total,intermediario,
                                                                    inter_precio,coop_cantidad,coop_precio,
                                                                    formula1)

    ingreso_total = 0
    for k,v in tabla_comercializacion.items():
        if v[6] is not None:
            ingreso_total += v[6]

    area_total_cacao_produccion = HistoriaProduccion.objects.filter(ficha__in=filtro, anio=2019).aggregate(t=Sum('area_produccion'))['t']

    try:
        promedio_ingreso_anual = ingreso_total / area_total_cacao_produccion
    except:
        promedio_ingreso_anual = 0

    promedio_ingreso_neto = promedio_ingreso_anual * 0.4

    #salidas cuadro de historias de produccion cacaotera
    ANO_CHOICES_HISTORIA=[]
    for i in range (datetime.date.today().year,2016,-1):
        ANO_CHOICES_HISTORIA.append((i,i))
    tabla_areas = OrderedDict()
    for year in ANO_CHOICES_HISTORIA:
        numero_fami = HistoriaProduccion.objects.filter(ficha__in=filtro,
                                                        anio=year[0]).count()
        area_total = HistoriaProduccion.objects.filter(ficha__in=filtro,
                                                                          anio=year[0]).aggregate(t=Sum('area_total'))['t'] or 0
        if area_total > 0:
            desarrollo = HistoriaProduccion.objects.filter(ficha__in=filtro,
                                                                              anio=year[0]).aggregate(t=Sum('area_desarrollo'))['t'] or 0
            produccion = HistoriaProduccion.objects.filter(ficha__in=filtro,
                                                                              anio=year[0]).aggregate(t=Sum('area_produccion'))['t'] or 0
            produccion_total = HistoriaProduccion.objects.filter(ficha__in=filtro,
                                                                              anio=year[0]).aggregate(t=Sum('produccion_kg'))['t'] or 0
            try:
                productividad = produccion_total / float(produccion)
            except:
                productividad = 0
            tabla_areas[year[1]] = (numero_fami,area_total,desarrollo,produccion,produccion_total, productividad)


    tabla_certificado = {}
    for obj in CHOICES_SI_NO:
        valor = filtro.filter(mitigacionriesgo__certificado=obj[0]).count()
        tabla_certificado[obj[1]] = valor

    tabla_tipo_certificado = {}
    for obj in CHOICES_TIPO_CERTIFICADO:
        valor = filtro.filter(mitigacionriesgo__tipo_certificado__contains=obj[0]).count()
        tabla_tipo_certificado[obj[1]] = valor

    #aca va todo de cosecha
    tabla_cortes = OrderedDict()
    for obj in CHOICES_CORTES:
        valor = filtro.filter(cosecha__cortes=obj[0]).count()
        tabla_cortes[obj[1]] = valor

    tabla_mazorca = OrderedDict()
    for obj in CHOICES_SEPARAN_MAZORCA:
        valor = filtro.filter(cosecha__separan_mazorca=obj[0]).count()
        tabla_mazorca[obj[1]] = valor

    tabla_quiebran = OrderedDict()
    for obj in CHOICES_QUIEBRAN_MAZORCA:
        valor = filtro.filter(cosecha__quiebran_mazorca=obj[0]).count()
        tabla_quiebran[obj[1]] = valor

    tabla_calidad = OrderedDict()
    for obj in CHOICES_SI_NO:
        valor = filtro.filter(cosecha__calidad=obj[0]).count()
        tabla_calidad[obj[1]] = valor

    tabla_precio = OrderedDict()
    for obj in CHOICES_SI_NO:
        valor = filtro.filter(cosecha__precio=obj[0]).count()
        tabla_precio[obj[1]] = valor

    promedio_precio = Cosecha.objects.filter(ficha__in=filtro).aggregate(p=Avg('cuanto'))['p']


    return render(request, template, {'tabla_comercializacion':tabla_comercializacion,
                                                                'tabla_cadena':tabla_cadena,
                                                                'ingreso_total':ingreso_total,
                                                                'area_total': area_total_cacao_produccion,
                                                                'promedio_ingreso_anual':promedio_ingreso_anual,
                                                                'promedio_ingreso_neto':promedio_ingreso_neto,
                                                                'tabla_cortes':tabla_cortes,
                                                                'tabla_mazorca':tabla_mazorca,
                                                                'tabla_quiebran':tabla_quiebran,
                                                                'tabla_calidad':tabla_calidad,
                                                                'tabla_precio':tabla_precio,
                                                                'promedio_precio':promedio_precio,
                                                                'tabla_areas':tabla_areas,
                                                                'familias_produccen_cacao':familias_produccen_cacao,
                                                                'tabla_certificado': tabla_certificado,
                                                                'tabla_tipo_certificado': tabla_tipo_certificado,
                                                                'familias_totales':familias_totales,
                                                                'num_familias': filtro.count()})

@login_required
def auto_evaluacion(request, template='ficha/evaluacion.html'):
    filtro1 = _queryset_filtrado(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro =filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    #poda
    calidad_poda = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__poda=obj[0]).count()
        calidad_poda[obj[1]] = conteo

    calidad_nivel_poda = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__poda=obj[0]).count()
        calidad_nivel_poda[obj[1]] = conteo

    calidad_mes_poda = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__poda__contains=obj[0]).count()
        calidad_mes_poda[obj[1]] = conteo

    #sombra
    calidad_sombra = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__sombra=obj[0]).count()
        calidad_sombra[obj[1]] = conteo

    calidad_nivel_sombra = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__sombra=obj[0]).count()
        calidad_nivel_sombra[obj[1]] = conteo

    calidad_mes_sombra = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__sombra__contains=obj[0]).count()
        calidad_mes_sombra[obj[1]] = conteo

    #maleza
    calidad_maleza = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__maleza=obj[0]).count()
        calidad_maleza[obj[1]] = conteo

    calidad_nivel_maleza = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__maleza=obj[0]).count()
        calidad_nivel_maleza[obj[1]] = conteo

    calidad_mes_maleza = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__maleza__contains=obj[0]).count()
        calidad_mes_maleza[obj[1]] = conteo

    #fertilidad
    calidad_fertilidad = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__fertilidad=obj[0]).count()
        calidad_fertilidad[obj[1]] = conteo

    calidad_nivel_fertilidad = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__fertilidad=obj[0]).count()
        calidad_nivel_fertilidad[obj[1]] = conteo

    calidad_mes_fertilidad = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__fertilidad__contains=obj[0]).count()
        calidad_mes_fertilidad[obj[1]] = conteo

    #enfermedades
    calidad_enfermedades = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__enfermedad=obj[0]).count()
        calidad_enfermedades[obj[1]] = conteo

    calidad_nivel_enfermedades = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__enfermedad=obj[0]).count()
        calidad_nivel_enfermedades[obj[1]] = conteo

    calidad_mes_enfermedades = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__enfermedad__contains=obj[0]).count()
        calidad_mes_enfermedades[obj[1]] = conteo

    #insectos
    calidad_insectos = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__plagas_insectos=obj[0]).count()
        calidad_insectos[obj[1]] = conteo

    calidad_nivel_insectos = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__plagas_insectos=obj[0]).count()
        calidad_nivel_insectos[obj[1]] = conteo

    calidad_mes_insectos = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__plagas_insectos__contains=obj[0]).count()
        calidad_mes_insectos[obj[1]] = conteo

    #nematodos
    calidad_nematodos = OrderedDict()
    for obj in CHOICES_CALIDAD:
        conteo = filtro.filter(calidadmanejo__plagas_nematodos=obj[0]).count()
        calidad_nematodos[obj[1]] = conteo

    calidad_nivel_nematodos = OrderedDict()
    for obj in CHOICES_USO_MANEJO:
        conteo = filtro.filter(nivelmanejo__plagas_nematodos=obj[0]).count()
        calidad_nivel_nematodos[obj[1]] = conteo

    calidad_mes_nematodos = OrderedDict()
    for obj in CHOICE_MESES:
        conteo = filtro.filter(meslabores__plagas_nematodos__contains=obj[0]).count()
        calidad_mes_nematodos[obj[1]] = conteo


    return render(request, template, {'calidad_poda':calidad_poda,
                                                                'calidad_nivel_poda':calidad_nivel_poda,
                                                                'calidad_mes_poda':calidad_mes_poda,
                                                                'calidad_sombra':calidad_sombra,
                                                                'calidad_nivel_sombra':calidad_nivel_sombra,
                                                                'calidad_mes_sombra':calidad_mes_sombra,
                                                                'calidad_maleza':calidad_maleza,
                                                                'calidad_nivel_maleza':calidad_nivel_maleza,
                                                                'calidad_mes_maleza':calidad_mes_maleza,
                                                                'calidad_fertilidad':calidad_fertilidad,
                                                                'calidad_nivel_fertilidad':calidad_nivel_fertilidad,
                                                                'calidad_mes_fertilidad':calidad_mes_fertilidad,
                                                                'calidad_enfermedades':calidad_enfermedades,
                                                                'calidad_nivel_enfermedades':calidad_nivel_enfermedades,
                                                                'calidad_mes_enfermedades':calidad_mes_enfermedades,
                                                                'calidad_insectos':calidad_insectos,
                                                                'calidad_nivel_insectos':calidad_nivel_insectos,
                                                                'calidad_mes_insectos':calidad_mes_insectos,
                                                                'calidad_nematodos':calidad_nematodos,
                                                                'calidad_nivel_nematodos':calidad_nivel_nematodos,
                                                                'calidad_mes_nematodos':calidad_mes_nematodos,
                                                                'num_familias': filtro.count()})

@login_required
def fuerza_trabajo(request, template='ficha/trabajo.html'):
    filtro1 = _queryset_filtrado(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro =filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    tabla_trabajo = OrderedDict()
    for obj in Actvidades.objects.all():
        contrado = filtro.filter(usofuerzatrabajo__actividades=obj,
                                         usofuerzatrabajo__contratados=1).count()
        hombres_adultos = filtro.filter(usofuerzatrabajo__actividades=obj,
                                         usofuerzatrabajo__hombres=1).count()
        mujeres_adultos = filtro.filter(usofuerzatrabajo__actividades=obj,
                                         usofuerzatrabajo__mujeres=1).count()
        hombres_jovenes = filtro.filter(usofuerzatrabajo__actividades=obj,
                                         usofuerzatrabajo__hombres_jovenes=1).count()
        mujeres_jovenes = filtro.filter(usofuerzatrabajo__actividades=obj,
                                         usofuerzatrabajo__mujeres_jovenes=1).count()

        tabla_trabajo[obj] = [contrado,hombres_adultos,mujeres_adultos,
                                                        hombres_jovenes,mujeres_jovenes]

    tabla_permanentes = OrderedDict()
    for obj in CHOICES_TRABAJADORES:
        conteo = filtro.filter(trabajadorespermanentes__categoria=obj[0],trabajadorespermanentes__numeros__gt=0).count()
        avg_trabajador = filtro.filter(trabajadorespermanentes__categoria=obj[0]).aggregate(pro=Avg('trabajadorespermanentes__numeros'))['pro'] or 0
        avg_meses = filtro.filter(trabajadorespermanentes__categoria=obj[0]).aggregate(pro=Avg('trabajadorespermanentes__meses'))['pro'] or 0
        avg_pago = filtro.filter(trabajadorespermanentes__categoria=obj[0]).aggregate(pro=Avg('trabajadorespermanentes__pago'))['pro'] or 0
        if avg_trabajador > 0 and avg_meses > 0:
            empleo = avg_trabajador * avg_meses / 12
            pago_trabajador = empleo * avg_pago * 312
            tabla_permanentes[obj[1]] = [avg_trabajador,avg_meses,avg_pago,empleo,pago_trabajador,conteo]

    tabla_temporales = OrderedDict()
    for obj in CHOICES_TRABAJADORES:
        conteo = filtro.filter(trabajadorestemporales__categoria=obj[0],trabajadorestemporales__numeros__gt=0).count()
        avg_trabajador = filtro.filter(trabajadorestemporales__categoria=obj[0]).aggregate(pro=Avg('trabajadorestemporales__numeros'))['pro'] or 0
        avg_meses = filtro.filter(trabajadorestemporales__categoria=obj[0]).aggregate(pro=Avg('trabajadorestemporales__meses'))['pro'] or 0
        avg_pago = filtro.filter(trabajadorestemporales__categoria=obj[0]).aggregate(pro=Avg('trabajadorestemporales__pago'))['pro'] or 0
        if avg_trabajador > 0 and avg_meses > 0:
            empleo = avg_trabajador * avg_meses / 312
            pago_trabajador = empleo * avg_pago * 312
            tabla_temporales[obj[1]] = [avg_trabajador,avg_meses,avg_pago,empleo,pago_trabajador,conteo]

    tabla_familiares = OrderedDict()
    for obj in CHOICES_TRABAJADORES:
        conteo = filtro.filter(trabajadoresfamiliares__categoria=obj[0],trabajadoresfamiliares__numeros__gt=0).count()
        avg_trabajador = filtro.filter(trabajadoresfamiliares__categoria=obj[0]).aggregate(pro=Avg('trabajadoresfamiliares__numeros'))['pro'] or 0
        avg_meses = filtro.filter(trabajadoresfamiliares__categoria=obj[0]).aggregate(pro=Avg('trabajadoresfamiliares__meses'))['pro'] or 0
        avg_pago = filtro.filter(trabajadoresfamiliares__categoria=obj[0]).aggregate(pro=Avg('trabajadoresfamiliares__pago'))['pro'] or 0
        if avg_trabajador > 0 and avg_meses > 0:
            empleo = avg_trabajador * avg_meses / 12
            pago_trabajador = empleo * avg_pago * 312
            tabla_familiares[obj[1]] = [avg_trabajador,avg_meses,avg_pago,empleo,pago_trabajador,conteo]

    total_permanente = [sum(i) for i in zip(*list(tabla_permanentes.values()))]
    total_temporal = [sum(i) for i in zip(*list(tabla_temporales.values()))]
    total_familiar = [sum(i) for i in zip(*list(tabla_familiares.values()))]

    return render(request, template, {'tabla_trabajo':tabla_trabajo,
                                                                'tabla_permanentes': tabla_permanentes,
                                                                'tabla_temporales': tabla_temporales,
                                                                'tabla_familiares': tabla_familiares,
                                                                'total_permanente':total_permanente,
                                                                'total_temporal':total_temporal,
                                                                'total_familiar': total_familiar,
                                                                'num_familias': filtro.count()})

@login_required
def ingresos_produccion(request, template='ficha/ingresos.html'):
    filtro1 = _queryset_filtrado(request).select_related('productor')
    query_all = RubrosProduccion.objects.all()

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro =filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    tabla_granos_basica = OrderedDict()
    ingreso_bruto_granos_basicos = 0
    for obj in query_all.filter(categoria=1):
        objeto = ProduccionFinca.objects.filter(rubros=obj, ficha__in=filtro)
        unidad = [ x.get_unidad_display() for x in objeto.filter(rubros=obj)]
        produciendo = objeto.count()
        area = objeto.aggregate(area=Sum('area'))['area'] or 0
        produccion =  objeto.aggregate(prod=Sum('cantidad'))['prod'] or 0
        precio = objeto.filter(precio__gt=0).aggregate(precio=Avg('precio'))['precio'] or 0
        if produccion > 0 and precio > 0:
            ingreso_bruto = produccion * precio
            ingreso_bruto_granos_basicos += ingreso_bruto

            tabla_granos_basica[obj.nombre] = [produciendo,area,obj.get_unidad_display(),produccion,unidad[0],precio,ingreso_bruto]


    tabla_agro_fruta = OrderedDict()
    ingreso_bruto_agro_fruta = 0
    for obj in query_all.filter(categoria=2):
        objeto = ProduccionFinca.objects.filter(rubros=obj, ficha__in=filtro)
        unidad = [ x.get_unidad_display() for x in objeto.filter(rubros=obj)]
        produciendo = objeto.count()
        area = objeto.aggregate(area=Sum('area'))['area'] or 0
        produccion =  objeto.aggregate(prod=Sum('cantidad'))['prod'] or 0
        precio = objeto.filter(precio__gt=0).aggregate(precio=Avg('precio'))['precio'] or 0
        if produccion > 0 and precio > 0:
            ingreso_bruto = produccion * precio
            ingreso_bruto_agro_fruta += ingreso_bruto

            tabla_agro_fruta[obj.nombre] = [produciendo,area,obj.get_unidad_display(),produccion,unidad[0],precio,ingreso_bruto]

    tabla_musaceas_raices = OrderedDict()
    ingreso_bruto_musaceas_raices = 0
    for obj in query_all.filter(categoria=3):
        objeto = ProduccionFinca.objects.filter(rubros=obj, ficha__in=filtro)
        unidad = [ x.get_unidad_display() for x in objeto.filter(rubros=obj)]
        produciendo = objeto.count()
        area = objeto.aggregate(area=Sum('area'))['area'] or 0
        produccion =  objeto.aggregate(prod=Sum('cantidad'))['prod'] or 0
        precio = objeto.filter(precio__gt=0).aggregate(precio=Avg('precio'))['precio'] or 0
        if produccion > 0 and precio > 0:
            ingreso_bruto = produccion * precio
            ingreso_bruto_musaceas_raices += ingreso_bruto

            tabla_musaceas_raices[obj.nombre] = [produciendo,area,obj.get_unidad_display(),produccion,unidad[0],precio,ingreso_bruto]

    tabla_ganaderia_mayor = OrderedDict()
    ingreso_bruto_ganaderia_mayor = 0
    for obj in query_all.filter(categoria=4):
        objeto = ProduccionFinca.objects.filter(rubros=obj, ficha__in=filtro)
        unidad = [ x.get_unidad_display() for x in objeto.filter(rubros=obj)]
        produciendo = objeto.count()
        area = objeto.aggregate(area=Sum('area'))['area'] or 0
        produccion =  objeto.aggregate(prod=Sum('cantidad'))['prod'] or 0
        precio = objeto.filter(precio__gt=0).aggregate(precio=Avg('precio'))['precio'] or 0
        if produccion > 0 and precio > 0:
            ingreso_bruto = produccion * precio
            ingreso_bruto_ganaderia_mayor += ingreso_bruto

            tabla_ganaderia_mayor[obj.nombre] = [produciendo,area,obj.get_unidad_display(),produccion,unidad[0],precio,ingreso_bruto]

    tabla_otros_ingresos = OrderedDict()
    ingreso_bruto_otros_ingresos = 0
    for obj in RubrosIngresos.objects.all():
        objeto = IngresoFinca.objects.filter(rubros=obj, ficha__in=filtro)
        unidad = [ x.get_tiempo_display() for x in objeto.filter(rubros=obj)]
        produciendo = objeto.count()
        cantidad = objeto.aggregate(cant=Sum('cantidad'))['cant'] or 0
        ingreso =  objeto.aggregate(prod=Avg('ingreso'))['prod'] or 0
        if cantidad > 0 and ingreso > 0:
            ingreso_bruto = cantidad * ingreso
            ingreso_bruto_otros_ingresos += ingreso_bruto
            tabla_otros_ingresos[obj.nombre] = [produciendo,cantidad,unidad[0],ingreso,ingreso_bruto]

    ingreso_bruto_anual = ingreso_bruto_granos_basicos + ingreso_bruto_agro_fruta + \
                                            ingreso_bruto_musaceas_raices + ingreso_bruto_ganaderia_mayor
    ingreso_neto_anual = 0.4 * ingreso_bruto_anual
    ingreso_neto_anual_otros_ingresos = 0.8 * ingreso_bruto_otros_ingresos

    try:
        agricola_familia =ingreso_bruto_anual / len(filtro)
    except:
        agricola_familia = 0
    try:
        no_agricola_familia =ingreso_bruto_otros_ingresos / len(filtro)
    except:
        no_agricola_familia = 0
    try:
        ingreso_agri_familia = ingreso_neto_anual / filtro.count()
    except:
        ingreso_agri_familia = 0
    try:
        ingreso_no_agri_familia = ingreso_neto_anual_otros_ingresos / len(filtro)
    except:
        ingreso_no_agri_familia = 0

    tabla_productividad = OrderedDict()
    for obj in query_all:
        objeto = ProduccionFinca.objects.filter(rubros=obj, ficha__in=filtro)
        unidad = [ x.get_unidad_display() for x in objeto.filter(rubros=obj)]
        produciendo = objeto.count()
        area = objeto.aggregate(area=Sum('area'))['area'] or 0
        produccion =  objeto.aggregate(prod=Sum('cantidad'))['prod'] or 0
        if produccion > 0 and area > 0:
            productividad  = produccion / area

            tabla_productividad[obj.nombre] = [produciendo,area,obj.get_unidad_display(),
                                                produccion,unidad[0],productividad]


    return render(request, template, {'tabla_granos_basica':tabla_granos_basica,
                                                                'ingreso_bruto_granos_basicos':ingreso_bruto_granos_basicos,
                                                                'tabla_agro_fruta':tabla_agro_fruta,
                                                                'ingreso_bruto_agro_fruta':ingreso_bruto_agro_fruta,
                                                                'tabla_musaceas_raices': tabla_musaceas_raices,
                                                                'ingreso_bruto_musaceas_raices':ingreso_bruto_musaceas_raices,
                                                                'tabla_ganaderia_mayor':tabla_ganaderia_mayor,
                                                                'ingreso_bruto_ganaderia_mayor':ingreso_bruto_ganaderia_mayor,
                                                                'tabla_otros_ingresos':tabla_otros_ingresos,
                                                                'ingreso_bruto_otros_ingresos':ingreso_bruto_otros_ingresos,
                                                                'ingreso_bruto_anual':ingreso_bruto_anual,
                                                                'ingreso_neto_anual':ingreso_neto_anual,
                                                                'ingreso_neto_anual_otros_ingresos':ingreso_neto_anual_otros_ingresos,
                                                                'agricola_familia':agricola_familia,
                                                                'no_agricola_familia':no_agricola_familia,
                                                                'ingreso_agri_familia':ingreso_agri_familia,
                                                                'ingreso_no_agri_familia':ingreso_no_agri_familia,
                                                                'tabla_productividad':tabla_productividad,
                                                                'num_familias': filtro.count()})

@login_required
def inventarios(request, template='ficha/inventarios.html'):
    filtro1 = _queryset_filtrado(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro = filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()

    numero_plantios = filtro.aggregate(total=Count('inventariosafcacao__id'))['total']
    fincas_plantios = 0
    for obj in filtro:
        conteo = InventarioSafCacao.objects.filter(ficha=obj,area__gte=0.1).count()
        if conteo > 0:
            fincas_plantios += 1

    area_total_saf_cacao = filtro.aggregate(a=Sum('inventariosafcacao__area'))['a']
    area_total_saf_desarrollo = filtro.filter(inventariosafcacao__estado_cacao=1).aggregate(a=Sum('inventariosafcacao__area'))['a'] or 0
    area_total_saf_produccion = filtro.filter(inventariosafcacao__estado_cacao=2).aggregate(a=Sum('inventariosafcacao__area'))['a'] or 0
    area_total_saf_dp = filtro.filter(inventariosafcacao__estado_cacao=3).aggregate(a=Sum('inventariosafcacao__area'))['a'] or 0

    try:
        semilla =  filtro.filter(inventariosafcacao__numero_semilla__gt=0).values_list('inventariosafcacao__numero_semilla',flat=True)
        avg_semilla = np.mean(semilla)
        mediana_semilla = np.median(semilla)
        max_semilla = max(semilla)
        min_semilla = min(semilla)
    except:
        semilla = [0]
        avg_semilla = np.mean(semilla)
        mediana_semilla = np.median(semilla)
        max_semilla = max(semilla)
        min_semilla = min(semilla)
    try:
        injerto = filtro.filter(inventariosafcacao__numero_injerto__gt=0).values_list('inventariosafcacao__numero_injerto',flat=True)
        avg_injerto = np.mean(injerto)
        mediana_injerto = np.median(injerto)
        max_injerto = max(injerto)
        min_injerto = min(injerto)
    except:
        injerto = [0]
        avg_injerto = np.mean(injerto)
        mediana_injerto = np.median(injerto)
        max_injerto = max(injerto)
        min_injerto = min(injerto)
    try:
        musaceas = filtro.filter(inventariosafcacao__numero_musaceas__gt=0).values_list('inventariosafcacao__numero_musaceas',flat=True)
        avg_musaceas = np.mean(musaceas)
        mediana_musaceas = np.median(musaceas)
        max_musaceas = max(musaceas)
        min_musaceas = min(musaceas)
    except:
        musaceas = [0]
        avg_musaceas = np.mean(musaceas)
        mediana_musaceas = np.median(musaceas)
        max_musaceas = max(musaceas)
        min_musaceas = min(musaceas)
    try:
        frutales = filtro.filter(inventariosafcacao__numero_frutales__gt=0).values_list('inventariosafcacao__numero_frutales',flat=True)
        avg_frutales = np.mean(frutales)
        mediana_frutales = np.median(frutales)
        max_frutales = max(frutales)
        min_frutales = min(frutales)
    except:
        frutales = [0]
        avg_frutales = np.mean(frutales)
        mediana_frutales = np.median(frutales)
        max_frutales = max(frutales)
        min_frutales = min(frutales)
    try:
        maderables = filtro.filter(inventariosafcacao__numero_maderables__gt=0).values_list('inventariosafcacao__numero_maderables',flat=True)
        avg_maderables = np.mean(maderables)
        mediana_maderables = np.median(maderables)
        max_maderables = max(maderables)
        min_maderables = min(maderables)
    except:
        maderables = [0]
        avg_maderables = np.mean(maderables)
        mediana_maderables = np.median(maderables)
        max_maderables = max(maderables)
        min_maderables = min(maderables)
    try:
        servicios = filtro.filter(inventariosafcacao__numero_arboles__gt=0).values_list('inventariosafcacao__numero_arboles',flat=True)
        avg_servicios = np.mean(servicios)
        mediana_servicios = np.median(servicios)
        max_servicios = max(servicios)
        min_servicios = min(servicios)
    except:
        servicios = [0]
        avg_servicios = np.mean(servicios)
        mediana_servicios = np.median(servicios)
        max_servicios = max(servicios)
        min_servicios = min(servicios)
    try:
        edad_plantas = filtro.filter(inventariosafcacao__anios__gt=0).values_list('inventariosafcacao__anios',flat=True)
        avg_edad_plantas = np.mean(edad_plantas)
        mediana_edad_plantas = np.median(edad_plantas)
        max_edad_plantas = max(edad_plantas)
        min_edad_plantas = min(edad_plantas)
    except:
        edad_plantas = [0]
        avg_edad_plantas = np.mean(edad_plantas)
        mediana_edad_plantas = np.median(edad_plantas)
        max_edad_plantas = max(edad_plantas)
        min_edad_plantas = min(edad_plantas)
    try:
        produccion = filtro.filter(inventariosafcacao__produccion__gt=0).values_list('inventariosafcacao__produccion',flat=True)
        avg_produccion = np.mean(produccion)
        mediana_produccion = np.median(produccion)
        max_produccion = max(produccion)
        min_produccion = min(produccion)
    except:
        produccion = [0]
        avg_produccion = np.mean(produccion)
        mediana_produccion = np.median(produccion)
        max_produccion = max(produccion)
        min_produccion = min(produccion)
    try:
        sombra = filtro.filter(inventariosafcacao__sombra__gt=0).values_list('inventariosafcacao__sombra',flat=True)
        avg_sombra = np.mean(sombra)
        mediana_sombra = np.median(sombra)
        max_sombra = max(sombra)
        min_sombra = min(sombra)
    except:
        sombra = [0]
        avg_sombra = np.mean(sombra)
        mediana_sombra = np.median(sombra)
        max_sombra = max(sombra)
        min_sombra = min(sombra)

    ###esto es la salidas de INFORMACIÓN ESTADO PRODUCTIVO CACAOS
    try:
        produccion2 =  filtro.filter(informacioncacao__numero_produccion__gt=0).values_list('informacioncacao__numero_produccion',flat=True)
        avg_produccion2 = np.mean(produccion2)
        mediana_produccion2 = np.median(produccion2)
        max_produccion2 = max(produccion2)
        min_produccion2 = min(produccion2)
    except:
        produccion2 = [0]
        avg_produccion2 = np.mean(produccion2)
        mediana_produccion2 = np.median(produccion2)
        max_produccion2 = max(produccion2)
        min_produccion2 = min(produccion2)
    try:
        desarrollo2 =  filtro.filter(informacioncacao__numero_desarrollo__gt=0).values_list('informacioncacao__numero_desarrollo',flat=True)
        avg_desarrollo2 = np.mean(desarrollo2)
        mediana_desarrollo2 = np.median(desarrollo2)
        max_desarrollo2 = max(desarrollo2)
        min_desarrollo2 = min(desarrollo2)
    except:
        desarrollo2 = [0]
        avg_desarrollo2 = np.mean(desarrollo2)
        mediana_desarrollo2 = np.median(desarrollo2)
        max_desarrollo2 = max(desarrollo2)
        min_desarrollo2 = min(desarrollo2)
    try:
        numero_faltantes =  filtro.filter(informacioncacao__numero_faltantes__gt=0).values_list('informacioncacao__numero_faltantes',flat=True)
        avg_numero_faltantes = np.mean(numero_faltantes)
        mediana_numero_faltantes = np.median(numero_faltantes)
        max_numero_faltantes = max(numero_faltantes)
        min_numero_faltantes = min(numero_faltantes)
    except:
        numero_faltantes = [0]
        avg_numero_faltantes = np.mean(numero_faltantes)
        mediana_numero_faltantes = np.median(numero_faltantes)
        max_numero_faltantes = max(numero_faltantes)
        min_numero_faltantes = min(numero_faltantes)
    try:
        numero_frutos_91 =  filtro.filter(informacioncacao__numero_frutos_91__gt=0).values_list('informacioncacao__numero_frutos_91',flat=True)
        avg_numero_frutos_91 = np.mean(numero_frutos_91)
        mediana_numero_frutos_91 = np.median(numero_frutos_91)
        max_numero_frutos_91 = max(numero_frutos_91)
        min_numero_frutos_91 = min(numero_frutos_91)
    except:
        numero_frutos_91 = [0]
        avg_numero_frutos_91 = np.mean(numero_frutos_91)
        mediana_numero_frutos_91 = np.median(numero_frutos_91)
        max_numero_frutos_91 = max(numero_frutos_91)
        min_numero_frutos_91 = min(numero_frutos_91)
    try:
        numero_frutos_51 =  filtro.filter(informacioncacao__numero_frutos_51__gt=0).values_list('informacioncacao__numero_frutos_51',flat=True)
        avg_numero_frutos_51 = np.mean(numero_frutos_51)
        mediana_numero_frutos_51 = np.median(numero_frutos_51)
        max_numero_frutos_51 = max(numero_frutos_51)
        min_numero_frutos_51 = min(numero_frutos_51)
    except:
        numero_frutos_51 = [0]
        avg_numero_frutos_51 = np.mean(numero_frutos_51)
        mediana_numero_frutos_51 = np.median(numero_frutos_51)
        max_numero_frutos_51 = max(numero_frutos_51)
        min_numero_frutos_51 = min(numero_frutos_51)
    try:
        numero_frutos_31 =  filtro.filter(informacioncacao__numero_frutos_31__gt=0).values_list('informacioncacao__numero_frutos_31',flat=True)
        avg_numero_frutos_31 = np.mean(numero_frutos_31)
        mediana_numero_frutos_31 = np.median(numero_frutos_31)
        max_numero_frutos_31 = max(numero_frutos_31)
        min_numero_frutos_31 = min(numero_frutos_31)
    except:
        numero_frutos_31 = [0]
        avg_numero_frutos_31 = np.mean(numero_frutos_31)
        mediana_numero_frutos_31 = np.median(numero_frutos_31)
        max_numero_frutos_31 = max(numero_frutos_31)
        min_numero_frutos_31 = min(numero_frutos_31)
    try:
        numero_frutos_0 =  filtro.filter(informacioncacao__numero_frutos_0__gt=0).values_list('informacioncacao__numero_frutos_0',flat=True)
        avg_numero_frutos_0 = np.mean(numero_frutos_0)
        mediana_numero_frutos_0 = np.median(numero_frutos_0)
        max_numero_frutos_0 = max(numero_frutos_0)
        min_numero_frutos_0 = min(numero_frutos_0)
    except:
        numero_frutos_0 = [0]
        avg_numero_frutos_0 = np.mean(numero_frutos_0)
        mediana_numero_frutos_0 = np.median(numero_frutos_0)
        max_numero_frutos_0 = max(numero_frutos_0)
        min_numero_frutos_0 = min(numero_frutos_0)


    return render(request, template,  locals())

@login_required
def tabla_beneficiario(request, template='ficha/tabla_beneficiario.html'):
    filtro_principal = _queryset_filtrado(request).select_related('productor',
                                                   'productor__cooperativa',
                                                   'productor__comunidad',
                                                   'tecnico').prefetch_related('mitigacionriesgo_set',
                                                   'historiaproduccion_set')
    try:
        coop = UserExtend.objects.get(user=request.user)
    except:
        pass
    if request.user.is_superuser:
        filtro = filtro_principal
    else:
        try:
            filtro = filtro_principal.filter(productor__cooperativa__id=coop.cooperativa.id)
        except:
            filtro = []
    num_familias = filtro_principal.count()

    tabla = OrderedDict()
    for obj in filtro:
        area_t = 0
        area_d = 0
        area_p = 0
        p_qq = 0
        rendimiento = 0
        certificado = 'No tiene'
        for a in obj.mitigacionriesgo_set.all():
            certificado = a.tipo_certificado
        for x in obj.historiaproduccion_set.all():
            if x.anio == int(request.session['fecha']):
                area_t = x.area_total
                area_d = x.area_desarrollo
                area_p = x.area_produccion
                p_qq = x.produccion_kg
                try:
                    rendimiento = p_qq  / area_p
                except:
                    rendimiento = 0
        tabla[obj.productor.nombre] = [obj.productor.get_sexo_display(), #0
                                        obj.productor.get_escalidad_display(), #1
                                        obj.productor.cooperativa, #2
                                        obj.productor.comunidad, #3
                                        obj.tecnico, #4
                                        area_t, #5
                                        area_d, #6
                                        area_p, #7
                                        p_qq, #8
                                        rendimiento, #9
                                        certificado  #10
                                        ]

    return render(request,template, locals())

@login_required
def mapa_productor(request, template='ficha/mapa.html'):

    return render(request, template, {'productores': Productor.objects.count()})

@login_required
def cooperativa_tabla(request, template='ficha/cooperativas.html'):

    return render(request, template, {'cooperativas': Cooperativa.objects.all()})
#ajax
@login_required
def obtener_lista_mapa(request):
    if request.is_ajax():
        lista = []
        for objeto in Productor.objects.all():
            if objeto.latitud  and objeto.longitud:
                pt = HistoriaProduccion.objects.filter(ficha__productor=objeto,
                        anio=datetime.date.today().year).aggregate(t=Sum('produccion_kg'))['t'] or 0
                if objeto.altitud:
                    dicc = dict(nombre=objeto.nombre, id=objeto.id,
                                lon=float(objeto.longitud) ,
                                lat=float(objeto.latitud),
                                altitud=float(objeto.altitud),
                                produccion=float(pt),
                                )
                else:
                    dicc = dict(nombre=objeto.nombre, id=objeto.id,
                                lon=float(objeto.longitud) ,
                                lat=float(objeto.latitud),
                                altitud=float(0),
                                produccion=float(pt),
                                )
                lista.append(dicc)
            elif objeto.municipio.latitud and objeto.municipio.longitud:
                pt = HistoriaProduccion.objects.filter(ficha__productor=objeto,
                        anio=datetime.date.today().year).aggregate(t=Sum('produccion_kg'))['t'] or 0
                dicc = dict(nombre=objeto.nombre, id=objeto.id,
                            lon=float(objeto.municipio.longitud) ,
                            lat=float(objeto.municipio.latitud),
                            altitud=float(0),
                            produccion=float(pt),
                            )
                lista.append(dicc)
            # else:
            #     messages.warning(request, 'No hay productores que mostrar.')

        serializado = simplejson.dumps(lista)
        return HttpResponse(serializado, content_type='application/json')

@login_required
def get_productor(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        try:
            coop = UserExtend.objects.get(user=request.user)
        except:
            pass
        if request.user.is_superuser:
            personas = Productor.objects.filter(nombre__icontains = q )[:10]
        else:
            try:
                personas = Productor.objects.filter(nombre__icontains = q, cooperativa__id=coop.cooperativa.id)[:10]
            except:
                personas = []
        #print personas
        results = []
        for person in personas:
            personas_json = {}
            personas_json['id'] = person.id
            personas_json['label'] = person.nombre
            personas_json['value'] = person.nombre
            results.append(personas_json)
    else:
        results = 'fail'
    return HttpResponse(simplejson.dumps(results), content_type='application/json')

def load_departamentos(request):
    pais_id = request.GET.get('pais')
    lugares = Departamento.objects.filter(pais_id=pais_id).order_by('nombre')
    return render(request, 'ficha/lugares_dropdown_list_options.html', {'lugares': lugares})

def load_municipios(request):
    departamento_id = request.GET.get('departamento')
    lugares = Municipio.objects.filter(departamento_id=departamento_id).order_by('nombre')
    return render(request, 'ficha/lugares_dropdown_list_options.html', {'lugares': lugares})

def load_comunidades(request):
    municipio_id = request.GET.get('municipio')
    lugares = Comunidad.objects.filter(municipio_id=municipio_id).order_by('nombre')
    return render(request, 'ficha/lugares_dropdown_list_options.html', {'lugares': lugares})

def get_view(request, vista):
    if vista in VALID_VIEWS:
        return VALID_VIEWS[vista](request)
    else:
        raise ViewDoesNotExist("Tried %s in module %s Error: View not defined in VALID_VIEWS." % (vista, 'lineabase.views'))

def saca_porcentajes(dato, total, formato=True):
    '''Si formato es true devuelve float caso contrario es cadena'''
    if dato != None:
        try:
            porcentaje = (dato/float(total)) * 100 if total != None or total != 0 else 0
        except:
            return 0
        if formato:
            return porcentaje
        else:
            return '%.2f' % porcentaje
    else:
        return 0

def save_as_xls(request):
    tabla = request.POST['tabla']
    response = render_to_response('xls.html', {'tabla': tabla, })
    response['Content-Disposition'] = 'attachment; filename=tabla.xls'
    response['Content-Type'] = 'application/vnd.ms-excel'
    response['Charset'] ='UTF-8'
    return response

VALID_VIEWS = {
        'consulta': consulta,
        'composicion': composicion,
        'tierra': tierra,
        'agroecologica': opcion_agroecologica,
        'mitigacion': mitigacion,
        'conocimientos': conocimiento_habilidades,
        'comercializacion': comercializacion,
        'evaluacion': auto_evaluacion,
        'trabajo': fuerza_trabajo,
        'mapa': mapa_productor,
        'ingresos': ingresos_produccion,
        'inventario': inventarios,
        'cooperativas': cooperativa_tabla,
        'beneficiarios': tabla_beneficiario,
        'xls': save_as_xls,
    }
