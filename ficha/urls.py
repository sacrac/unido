from django.conf.urls import url, re_path
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from .views import *
from .panel import panel_lb

urlpatterns =  [
    re_path(r'^$', login_required(TemplateView.as_view(template_name="menu_principal.html")), name='principal'),
    re_path(r'^panel/linea-base/$', panel_lb, name='panel'),
    re_path(r'^consulta/$', consulta, name='consulta'),
    re_path(r'^api/productor/', get_productor, name='productor-search'),
    #re_path(r'^ajax/load-departamentos/', load_departamentos, name='ajax_load_departamentos'),
    re_path(r'^ajax/load-municipios/', load_municipios, name='ajax_load_municipios'),
    re_path(r'^ajax/load-comunidad/', load_comunidades, name='ajax_load_comunidad'),
    re_path(r'^mapaproductores/', obtener_lista_mapa, name='obtener-lista-mapa'),
    re_path(r'^(?P<vista>\w+)/$', get_view, name='get-view'),
]
