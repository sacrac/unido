from django.apps import AppConfig


class CooperativasConfig(AppConfig):
    name = 'cooperativas'
