from django.contrib import admin
from .models import Cooperativa
# Register your models here.

class CooperativasAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(CooperativasAdmin, self).save_model(request, obj, form, change)

    exclude = ('user', )
    list_display = ('id', 'nombre', 'siglas')
    list_display_links = ('id', 'nombre', 'siglas')
    search_fields = ('nombre', 'siglas')

admin.site.register(Cooperativa, CooperativasAdmin)
