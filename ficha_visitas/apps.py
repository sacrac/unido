from django.apps import AppConfig


class FichaVisitasConfig(AppConfig):
    name = 'ficha_visitas'
    verbose_name = "Ficha visitas trimestral"
