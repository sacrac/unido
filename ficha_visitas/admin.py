from django.contrib import admin
from .models import *

# Register your models here.
class InventarioParcelaInlines(admin.TabularInline):
    model = InventarioParcela
    extra = 1
    max_num = 1

class EstadoActualPunto1CacaoInlines(admin.TabularInline):
    model = EstadoActualPunto1Cacao
    extra = 1
    max_num = 10

class PrevalenciaPlagaPunto1Inlines(admin.TabularInline):
    model = PrevalenciaPlagaPunto1
    extra = 1
    max_num = 10

class EstimadoCosechaCacaoPunto1Inlines(admin.TabularInline):
    model = EstimadoCosechaCacaoPunto1
    extra = 1
    max_num = 3

class EstadoActualPunto1MusaceasInlines(admin.TabularInline):
    model = EstadoActualPunto1Musaceas
    extra = 1
    max_num = 10

class EstadoActualPunto1FrutalesInlines(admin.TabularInline):
    model = EstadoActualPunto1Frutales
    extra = 1
    max_num = 10

class EstadoActualPunto1MaderablesInlines(admin.TabularInline):
    model = EstadoActualPunto1Maderables
    extra = 1
    max_num = 10

class EstadoActualPunto1ArbolesInlines(admin.TabularInline):
    model = EstadoActualPunto1Arboles
    extra = 1
    max_num = 10

class EstadoActualPunto2CacaoInlines(admin.TabularInline):
    model = EstadoActualPunto2Cacao
    extra = 1
    max_num = 10

class PrevalenciaPlagaPunto2Inlines(admin.TabularInline):
    model = PrevalenciaPlagaPunto2
    extra = 1
    max_num = 10

class EstimadoCosechaCacaoPunto2Inlines(admin.TabularInline):
    model = EstimadoCosechaCacaoPunto2
    extra = 1
    max_num = 3

class EstadoActualPunto2MusaceasInlines(admin.TabularInline):
    model = EstadoActualPunto2Musaceas
    extra = 1
    max_num = 10

class EstadoActualPunto2FrutalesInlines(admin.TabularInline):
    model = EstadoActualPunto2Frutales
    extra = 1
    max_num = 10

class EstadoActualPunto2MaderablesInlines(admin.TabularInline):
    model = EstadoActualPunto2Maderables
    extra = 1
    max_num = 10

class EstadoActualPunto2ArbolesInlines(admin.TabularInline):
    model = EstadoActualPunto2Arboles
    extra = 1
    max_num = 10

class EstadoActualPunto3CacaoInlines(admin.TabularInline):
    model = EstadoActualPunto3Cacao
    extra = 1
    max_num = 10

class PrevalenciaPlagaPunto3Inlines(admin.TabularInline):
    model = PrevalenciaPlagaPunto3
    extra = 1
    max_num = 10

class EstimadoCosechaCacaoPunto3Inlines(admin.TabularInline):
    model = EstimadoCosechaCacaoPunto3
    extra = 1
    max_num = 3

class EstadoActualPunto3MusaceasInlines(admin.TabularInline):
    model = EstadoActualPunto3Musaceas
    extra = 1
    max_num = 10

class EstadoActualPunto3FrutalesInlines(admin.TabularInline):
    model = EstadoActualPunto3Frutales
    extra = 1
    max_num = 10

class EstadoActualPunto3MaderablesInlines(admin.TabularInline):
    model = EstadoActualPunto3Maderables
    extra = 1
    max_num = 10

class EstadoActualPunto3ArbolesInlines(admin.TabularInline):
    model = EstadoActualPunto3Arboles
    extra = 1
    max_num = 10

class DatosPisoInlines(admin.TabularInline):
    model = DatosPiso
    extra = 1
    max_num = 19

class ActividadesCacaoInlines(admin.TabularInline):
    model = ActividadesCacao
    extra = 1
    max_num = 19

class ActividadesMusaceasInlines(admin.TabularInline):
    model = ActividadesMusaceas
    extra = 1
    max_num = 19

class ActividadesFrutalesInlines(admin.TabularInline):
    model = ActividadesFrutales
    extra = 1
    max_num = 19

class ActividadesMaderablesInlines(admin.TabularInline):
    model = ActividadesMaderables
    extra = 1
    max_num = 19

class ActividadesArbolesInlines(admin.TabularInline):
    model = ActividadesArboles
    extra = 1
    max_num = 19

class CosechaParcelaTrimestreInlines(admin.TabularInline):
    model = CosechaParcelaTrimestre
    extra = 1
    max_num = 19

class CosechaFincaTrimestreInlines(admin.TabularInline):
    model = CosechaFincaTrimestre
    extra = 1
    max_num = 19

class InversionesFincaInlines(admin.TabularInline):
    model = InversionesFinca
    extra = 1
    max_num = 19

class FichaVisitasAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(FichaVisitasAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(FichaVisitasAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    autocomplete_fields = ['productor','tecnico']
    list_display = ('productor','productor_cooperativa','anio','user')
    list_filter = ['productor__cooperativa','anio',]
    search_fields = ('productor__nombre',)
    inlines = [ InventarioParcelaInlines,EstadoActualPunto1CacaoInlines,
                PrevalenciaPlagaPunto1Inlines,EstimadoCosechaCacaoPunto1Inlines,
                EstadoActualPunto1MusaceasInlines,EstadoActualPunto1FrutalesInlines,
                EstadoActualPunto1MaderablesInlines,EstadoActualPunto1ArbolesInlines,
                EstadoActualPunto2CacaoInlines,
                PrevalenciaPlagaPunto2Inlines,EstimadoCosechaCacaoPunto2Inlines,
                EstadoActualPunto2MusaceasInlines,EstadoActualPunto2FrutalesInlines,
                EstadoActualPunto2MaderablesInlines,EstadoActualPunto2ArbolesInlines,
                EstadoActualPunto3CacaoInlines,
                PrevalenciaPlagaPunto3Inlines,EstimadoCosechaCacaoPunto3Inlines,
                EstadoActualPunto3MusaceasInlines,EstadoActualPunto3FrutalesInlines,
                EstadoActualPunto3MaderablesInlines,EstadoActualPunto3ArbolesInlines,
                DatosPisoInlines,ActividadesCacaoInlines,ActividadesMusaceasInlines,
                ActividadesFrutalesInlines,ActividadesMaderablesInlines,
                ActividadesArbolesInlines,CosechaParcelaTrimestreInlines,
                CosechaFincaTrimestreInlines,InversionesFincaInlines
            ]

    def productor_cooperativa(self, obj):
        return obj.productor.cooperativa
    productor_cooperativa.short_description = 'Cooperativa'
    productor_cooperativa.admin_order_field = 'productor__cooperativa'

admin.site.register(FichaVisitas,FichaVisitasAdmin)