# -*- coding: utf-8 -*-
from django import forms
from ficha.models import Tecnico
from lugar.models import (Pais, Departamento,
                        Municipio, Comunidad)
from cooperativas.models import Cooperativa

CHOICE_SEXO1 = (
    ('', '-------'),
    (1, 'Mujer'),
    (2, 'Hombre')
)

CHOICES_ANIO = (
        ('', '-------'),
        (1, '2020-21'),
        (2, '2021-22'),
        (3, '2022-23'),
        (4, '2023-24'),
        (5, '2024-25'),
        (6, '2025-26'),
        (7, '2026-27'),
        (8, '2027-28'),
        (9, '2028-29'),
        (10, '2029-30'),
        (11, '2030-31'),

    )

CHOICES_TIPO_PARCELAS = (
        ('', '-------'),
        (1, 'Parcela hibrida a mejorar'),
        (2, 'Parcela policlonal a mejorar'),
        (3, 'Parcela policlonal nueva'),

    )

class ConsultaFormVisitas(forms.Form):
    def __init__(self, *args, **kwargs):
          super(ConsultaFormVisitas, self).__init__(*args, **kwargs)
          self.fields['fecha'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['productor'].widget.attrs.update({'class': 'form-control'})
          self.fields['departamento'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['municipio'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['comunidad'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['sexo'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['cooperativa'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['tecnico'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
          self.fields['parcela'].widget.attrs.update({'class': 'select2 select2-container select2-container--default select2-container--above'})
    fecha = forms.ChoiceField(choices=CHOICES_ANIO, label="Años", required=False)
    productor = forms.CharField(max_length=250, required=False)
    departamento = forms.ModelChoiceField(queryset=Departamento.objects.all(), required=False)
    municipio = forms.ModelChoiceField(queryset=Municipio.objects.all(), required=False)
    comunidad = forms.ModelChoiceField(queryset=Comunidad.objects.all(), required=False)
    sexo = forms.ChoiceField(choices=CHOICE_SEXO1, required=False)
    cooperativa = forms.ModelChoiceField(queryset=Cooperativa.objects.all(), required=False)
    tecnico = forms.ModelChoiceField(queryset=Tecnico.objects.all(), required=False)
    parcela = forms.ChoiceField(choices=CHOICES_TIPO_PARCELAS, label="Parcela", required=False)
