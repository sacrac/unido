CHOICES_TRIMESTRE = (

        (1, 'Primer Trimestre (Mayo-Julio)'),
        (2, 'Segundo Trimestre (Agosto-Octubre)'),
        (3, 'Tercer Trimestre (Noviembre-Enero)'),

    )

CHOICES_ESTADO_CACAO = (

        (1, 'D'),
        (2, 'P'),
        (3, 'D-P'),

    )

CHOICES_TIPO_CLON = (

        (1, 'Híbrido'),
        (2, 'Clone'),
        

    )

CHOICES_TIPO_MAZORCAS = (

        (1, 'Número de mazorcas sanas'),
        (2, 'Número de mazorcas enfermas'),
        (3, 'Número de mazorcas dañadas'),

    )

CHOICE_PISO = (
        (1, 'Zacate anual'),
        (2, 'Zacate perene'),
        (3, 'Hoja ancha anual'),
        (4, 'Hoja ancha perenne'),
        (5, 'Ciperácea o Coyolillo'),
        (6, 'Bejucos en suelo'),
        (7, 'Cobertura hoja ancha'),
        (8, 'Cobertura hoja angosta'),
        (9, 'Hojarasca'),
        (10, 'Mulch de maleza'),
        (11, 'Suelo desnudo')
    )


CHOICE_LABORES_CACAO = (
        (1, 'Poda de formación'),
        (2, 'Poda de mantenimiento'),
        (3, 'Poda sanitaria'),
        (4, 'Poda de rehabilitación'),
        (5, 'Deschuponado'),
        (6, 'Aplicación de fertilizante orgánico'),
        (7, 'Aplicación de fertilizante sintético'),
        (8, 'Aplicación de insecticidas'),
        (9, 'Aplicación de fungicidas'),
        (10, 'Aplicación de herbicidas'),
        (11, 'Aplicación de Enmienda'),
        (12, 'Selección, preparación del área para el establecimiento del vivero'),
        (13, 'Selección, preparación de sustrato y llenado de bolsas'),
        (14, 'Selección, pre germinación y siembra de semilla'),
        (15, 'Riego'),
        (16, 'Control de malezas'),
        (17, 'Injerto en vivero'),
        (18, 'Injerto de híbridos en desarrollo'),
        (19, 'Injerto de híbridos en producción'),
        (20, 'Obras de drenaje'),
        (21, 'Obras de conservación'),
        (22, 'Remoción de mazorcas'),
        (23, 'Cosecha y Corte'),
        (24, 'Transporte de cosecha'),
    )

CHOICE_LABORES_MUSACEAS = (
        (1, 'Siembra/resiembra'),
        (2, 'Deshije y deshoja'),
        (3, 'Manejo de cabezas'),
        (4, 'Aplicación de fertilizantes'),
        (5, 'Aplicación de abono'),
        (6, 'Aplicación de insecticida'),
        (7, 'Aplicación de fungicida'),
        (8, 'Aplicación de Enmienda'),
        (9, 'Cosecha y Corte'),
        (10, 'Transporte de cosecha'),
    )


CHOICE_LABORES_FRUTALES = (
        (1, 'Siembra/resiembra'),
        (2, 'Poda de frutales'),
        (3, 'Aplicación de fertilizantes'),
        (4, 'Aplicación de abono'),
        (5, 'Aplicación de insecticida'),
        (6, 'Aplicación de fungicida'),
        (7, 'Aplicación de Enmienda'),
        (8, 'Cosecha y Corte'),
        (9, 'Transporte de cosecha'),
    )


CHOICE_LABORES_MADERABLES = (
        (1, 'Siembra/resiembra'),
        (2, 'Poda de frutales'),
        (3, 'Aplicación de fertilizantes'),
        (4, 'Aplicación de abono'),
        (5, 'Aplicación de insecticida'),
        (6, 'Aplicación de fungicida'),
        (7, 'Aplicación de Enmienda'),
        (8, 'Cosecha y Corte'),
        (9, 'Transporte de cosecha'),
    )

CHOICE_LABORES_ARBOLES_SERVICIOS = (
        (1, 'Siembra/resiembra'),
        (2, 'Poda de árboles'),
        (3, 'Cosecha de frutas'),
        (4, 'Corte de leña'),
        (5, 'Transporte de cosecha'),
    )


CHOICE_VISITA_MESES = (
        ('A', 'Enero'),
        ('B', 'Febrero'),
        ('C', 'Marzo'),
        ('D', 'Abril'),
        ('E', 'Mayo'),
        ('F', 'Junio'),
        ('G', 'Julio'),
        ('H', 'Agosto'),
        ('I', 'Septiembre'),
        ('J', 'Octubre'),
        ('K', 'Noviembre'),
        ('L', 'Diciembre'),
    )

CHOICE_VISITA_VENDIO = (
        ('A', 'Intermediario'),
        ('B', 'Cooperativa'),
        ('C', 'Asociación'),
        ('D', 'Empresa'),
        ('E', 'Auto-consumo'),
    )

CHOICE_VISITA_ACTIVIDADES = (
        (1, 'No realizado'),
        (2, 'Realizado en forma mala'),
        (3, 'Realizado en forma regular'),
        (4, 'Realizado en forma buena'),
    )

