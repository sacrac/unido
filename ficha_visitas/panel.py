from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from .models import FichaVisitas

@login_required
def panel_visita(request, template="visitas/panel_visita.html"):
    ''' panel de linea base '''
    fichas = FichaVisitas.objects.all().distinct('productor__id')
    total = fichas.count()
    hombres = fichas.filter(productor__sexo=2).count()
    mujeres = fichas.filter(productor__sexo=1).count()
    
    return render(request,template,locals())