from django.conf.urls import url, re_path
from .views import *
from .panel import panel_visita

urlpatterns =  [
    
    re_path(r'^panel/$', panel_visita, name='panel_visita'),
    re_path(r'^consulta-visitas/$', consulta_visitas, name='consulta_visitas'),
    re_path(r'^(?P<vista>\w+)/$', get_view_visita, name='get-view-visita'),
]