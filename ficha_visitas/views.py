from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.db.models import Avg, Sum, Count, Value as V, Q, F, ExpressionWrapper, FloatField
from django.db.models.functions import Coalesce
from django.core.exceptions import ViewDoesNotExist
from catalogos.models import ProductosInversion
from .models import *
from .forms import ConsultaFormVisitas
from .contants_visita import CHOICES_TRIMESTRE

import numpy as np
from collections import OrderedDict, Counter

# Create your views here.

def _queryset_filtrado_visitas(request):
    params = {}

    if 'fecha' in request.session:
        params['anio'] = request.session['fecha']

    if 'productor' in request.session:
        params['productor__nombre'] = request.session['productor']

    if 'departamento' in request.session:
        params['productor__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['productor__municipio'] = request.session['municipio']

    if 'comunidad' in request.session:
        params['productor__comunidad'] = request.session['comunidad']

    if 'sexo' in request.session:
        params['productor__sexo'] = request.session['sexo']

    if 'cooperativa' in request.session:
        params['productor__cooperativa'] = request.session['cooperativa']

    if 'tecnico' in request.session:
        params['tecnico'] = request.session['tecnico']

    if 'parcela' in request.session:
        params['parcela'] = request.session['parcela']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    return FichaVisitas.objects.filter(**params)

@login_required
def consulta_visitas(request, template='visitas/consulta_visitas.html'):
    conteo_encuesta = 0
    if request.method == 'POST':
        form = ConsultaFormVisitas(request.POST)
        if form.is_valid():
            request.session['fecha'] = form.cleaned_data['fecha']
            request.session['productor'] = form.cleaned_data['productor']
            request.session['departamento'] = form.cleaned_data['departamento']
            request.session['municipio'] = form.cleaned_data['municipio']
            request.session['comunidad'] = form.cleaned_data['comunidad']
            request.session['sexo'] = form.cleaned_data['sexo']
            request.session['cooperativa'] = form.cleaned_data['cooperativa']
            request.session['tecnico'] = form.cleaned_data['tecnico']
            request.session['parcela'] = form.cleaned_data['parcela']
            centinela = 1
            filtro = _queryset_filtrado_visitas(request)
            conteo_encuesta = filtro.count()

        else:
            centinela = 0
    else:
        form = ConsultaFormVisitas()
        centinela = 0

        if 'fecha' in request.session:
            try:
                del request.session['fecha']
                del request.session['productor']
                del request.session['departamento']
                del request.session['municipio']
                del request.session['comunidad']
                del request.session['sexo']
                del request.session['cooperativa']
                del request.session['tecnico']
                del request.session['parcela']
            except:
                pass

    return render(request, template, {'form': form, 'centinela': centinela,
                                    'conteo_encuesta':conteo_encuesta})


def inventario_saf_cacao(request, template="visitas/inventario_saf.html"):
    filtro = _queryset_filtrado_visitas(request)

    CHOICES_RANGOS = (
        ((1,5),'1-5 años'),
        ((6,10),'6-10 años'),
        ((11,15),'11-15 años'),
        ((16,20),'16-20 años'),
        ((21,1000),'Más 21 años')
    )

    CHOICES_PORCENTAJE = (
        ((1,10),'1-10 %'),
        ((11,20),'11-20 %'),
        ((21,30),'21-30 %'),
        ((31,40),'31-40 %'),
        ((41,50),'41-50 %'),
        ((51,1000),'Más 51 %')
    )

    num_familias = filtro.count()
    salida = {}

    for obj in CHOICES_TRIMESTRE:
        objetos = InventarioParcela.objects.filter(ficha__in=filtro, 
                                                   ficha__trimestre=obj[0])
        numero_parcelas = objetos.count()
        if numero_parcelas > 0:
            area_total = objetos.filter(area__gt=0).aggregate(t=Sum('area'))['t']
            area_avg = objetos.filter(area__gt=0).aggregate(t=Avg('area'))['t']
            total_hibrida = objetos.filter(plantas_hibridas__gt=0).aggregate(t=Sum('plantas_hibridas'))['t']
            avg_hibrida = total_hibrida / area_total
            total_injertada = objetos.filter(platas_injertadas__gt=0).aggregate(t=Sum('platas_injertadas'))['t']
            avg_injertada = total_injertada / area_total
            total_musaceas = objetos.filter(musaceas__gt=0).aggregate(t=Sum('musaceas'))['t']
            avg_musaceas = total_musaceas / area_total
            total_frutales = objetos.filter(frutales__gt=0).aggregate(t=Sum('frutales'))['t']
            avg_frutales = total_frutales / area_total
            total_maderables = objetos.filter(maderables__gt=0).aggregate(t=Sum('maderables'))['t']
            avg_maderables = total_maderables / area_total
            total_arboles_servicios = objetos.filter(arboles_servicios__gt=0).aggregate(t=Sum('arboles_servicios'))['t']
            avg_arboles_servicios = total_arboles_servicios / area_total
            
            
            #salidas grafico pie
            desarrollo = objetos.filter(area__gt=0, estado_cacao=1).aggregate(t=Sum('area'))['t']
            producion = objetos.filter(area__gt=0, estado_cacao=2).aggregate(t=Sum('area'))['t']
            dp = objetos.filter(area__gt=0, estado_cacao=3).aggregate(t=Sum('area'))['t']
            
            grafo_salida_barra = {}
            for obx in CHOICES_RANGOS:
                dato = objetos.filter(area__gt=0, edad__range=obx[0]).aggregate(t=Sum('area'))['t'] or 0
                if dato > 0:
                    grafo_salida_barra[obx[1]] = dato

            grafo_salida_porcenjate = {}
            for obq in CHOICES_PORCENTAJE:
                dato = objetos.filter(area__gt=0, porcentaje__range=obq[0]).aggregate(t=Sum('area'))['t'] or 0
                if dato > 0:
                    grafo_salida_porcenjate[obq[1]] = dato

            salida[obj[1]] = (area_total, area_avg,avg_hibrida,avg_injertada,
                          avg_musaceas,avg_frutales,avg_maderables,
                          avg_arboles_servicios,numero_parcelas,desarrollo,producion,
                          dp,grafo_salida_barra,grafo_salida_porcenjate)
            
    return render(request, template, locals())

def estado_planta_cacao(request, template="visitas/estado_plantas.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    objectos1 = EstadoActualPunto1Cacao.objects.filter(ficha__in=filtro)
    objectos2 = EstadoActualPunto2Cacao.objects.filter(ficha__in=filtro)
    objectos3 = EstadoActualPunto3Cacao.objects.filter(ficha__in=filtro)

    salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        query1_hibrido = objectos1.filter(ficha__trimestre=obj[0],
                                  tipo=1).values_list('altura','largo')
        query2_hibrido = objectos2.filter(ficha__trimestre=obj[0],
                                  tipo=1).values_list('altura','largo')
        query3_hibrido = objectos3.filter(ficha__trimestre=obj[0],
                                  tipo=1).values_list('altura','largo')
        query_hibrido_total = query1_hibrido.union(query2_hibrido,query3_hibrido)
        conteo_hibrido = query1_hibrido.count() + query2_hibrido.count() + query3_hibrido.count()
        hibrido_altura = funcion_altura(list(query_hibrido_total))
        hibrido_largo = funcion_largo(list(query_hibrido_total))

        query1_clone = objectos1.filter(ficha__trimestre=obj[0],
                                  tipo=2).values_list('altura','largo')
        query2_clone = objectos2.filter(ficha__trimestre=obj[0],
                                  tipo=2).values_list('altura','largo')
        query3_clone = objectos3.filter(ficha__trimestre=obj[0],
                                  tipo=2).values_list('altura','largo')
        query_clone_total = query1_clone.union(query2_clone,query3_clone)

        conteo_clone = query1_clone.count() + query2_clone.count() + query3_clone.count()
        clone_altura = funcion_altura(list(query_clone_total))
        clone_largo = funcion_largo(list(query_clone_total))
        #hibridos
        deficiencia_h_1 = objectos1.filter(ficha__trimestre=obj[0],
                       tipo=1,deficiencia=1).count()
        deficiencia_h_2 = objectos2.filter(ficha__trimestre=obj[0],
                       tipo=1,deficiencia=1).count()
        deficiencia_h_3 = objectos3.filter(ficha__trimestre=obj[0],
                       tipo=1,deficiencia=1).count()
        try:
            deficiencia_h_total = (((deficiencia_h_1+deficiencia_h_2+deficiencia_h_3)/conteo_hibrido)*100) 
        except:
            deficiencia_h_total = 0

        estructura_h_1 = objectos1.filter(ficha__trimestre=obj[0],
                       tipo=1,estructura=1).count()
        estructura_h_2 = objectos2.filter(ficha__trimestre=obj[0],
                       tipo=1,estructura=1).count()
        estructura_h_3 = objectos3.filter(ficha__trimestre=obj[0],
                       tipo=1,estructura=1).count()
        try:
            estructura_h_total = (((estructura_h_1+estructura_h_2+estructura_h_3)/conteo_hibrido)*100) 
        except:
            estructura_h_total = 0

        floracion_h_1 = objectos1.filter(ficha__trimestre=obj[0],
                       tipo=1,floracion=1).count()
        floracion_h_2 = objectos2.filter(ficha__trimestre=obj[0],
                       tipo=1,floracion=1).count()
        floracion_h_3 = objectos3.filter(ficha__trimestre=obj[0],
                       tipo=1,floracion=1).count()
        try:
            floracion_h_total = (((floracion_h_1+floracion_h_2+floracion_h_3)/conteo_hibrido)*100) 
        except:
            floracion_h_total = 0

        fruti_h_1 = objectos1.filter(ficha__trimestre=obj[0],
                       tipo=1,fructificacion=1).count()
        fruti_h_2 = objectos2.filter(ficha__trimestre=obj[0],
                       tipo=1,fructificacion=1).count()
        fruti_h_3 = objectos3.filter(ficha__trimestre=obj[0],
                       tipo=1,fructificacion=1).count()
        try:
            fruti_h_total = (((fruti_h_1+fruti_h_2+fruti_h_3)/conteo_hibrido)*100) 
        except:
            fruti_h_total = 0

        #clones    
        deficiencia_c_1 = objectos1.filter(ficha__trimestre=obj[0],
                       tipo=2,deficiencia=1).count()
        deficiencia_c_2 = objectos2.filter(ficha__trimestre=obj[0],
                       tipo=2,deficiencia=1).count()
        deficiencia_c_3 = objectos3.filter(ficha__trimestre=obj[0],
                       tipo=2,deficiencia=1).count()
        try:
            deficiencia_c_total = (((deficiencia_c_1+deficiencia_c_2+deficiencia_c_3)/conteo_clone)*100)
        except:
            deficiencia_c_total = 0

        estructura_c_1 = objectos1.filter(ficha__trimestre=obj[0],
                       tipo=2,estructura=1).count()
        estructura_c_2 = objectos2.filter(ficha__trimestre=obj[0],
                       tipo=2,estructura=1).count()
        estructura_c_3 = objectos3.filter(ficha__trimestre=obj[0],
                       tipo=2,estructura=1).count()
        try:
            estructura_c_total = (((estructura_c_1+estructura_c_2+estructura_c_3)/conteo_clone)*100)
        except:
            estructura_c_total = 0

        floracion_c_1 = objectos1.filter(ficha__trimestre=obj[0],
                       tipo=2,floracion=1).count()
        floracion_c_2 = objectos2.filter(ficha__trimestre=obj[0],
                       tipo=2,floracion=1).count()
        floracion_c_3 = objectos3.filter(ficha__trimestre=obj[0],
                       tipo=2,floracion=1).count()
        try:
            floracion_c_total = (((floracion_c_1+floracion_c_2+floracion_c_3)/conteo_clone)*100)
        except:
            floracion_c_total = 0

        fruti_c_1 = objectos1.filter(ficha__trimestre=obj[0],
                       tipo=2,fructificacion=1).count()
        fruti_c_2 = objectos2.filter(ficha__trimestre=obj[0],
                       tipo=2,fructificacion=1).count()
        fruti_c_3 = objectos3.filter(ficha__trimestre=obj[0],
                       tipo=2,fructificacion=1).count()
        try:
            fruti_c_total = (((fruti_c_1+fruti_c_2+fruti_c_3)/conteo_clone)*100)
        except:
            fruti_c_total = 0

        if conteo_hibrido or conteo_clone > 0:
            salida[obj[1]] = (conteo_hibrido,
                              np.mean(hibrido_altura),
                              np.median(hibrido_altura),
                              np.mean(hibrido_largo),
                              np.median(hibrido_largo),
                              conteo_clone,
                              np.mean(clone_altura),
                              np.median(clone_altura),
                              np.mean(clone_largo),
                              np.median(clone_largo),
                              num_familias1,
                              deficiencia_h_total,
                              estructura_h_total,
                              floracion_h_total,
                              fruti_h_total,
                              deficiencia_c_total,
                              estructura_c_total,
                              floracion_c_total,
                              fruti_c_total
                            )
            
    return render(request, template, locals())


def plagas_enfermedades_cacao(request, template="visitas/plagas_enfermedades.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    #objectos1 = PrevalenciaPlagaPunto1.objects.filter(ficha__in=filtro)
    #objectos2 = PrevalenciaPlagaPunto2.objects.filter(ficha__in=filtro)
    #objectos3 = PrevalenciaPlagaPunto3.objects.filter(ficha__in=filtro)

    salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()

        hibrido1 = PrevalenciaPlagaPunto1.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],
                    tipo=1).aggregate(
         total=Count('pk'),
         monilia=Count('pk', filter=Q(monilia=1)),
         mazorca_negra=Count('pk', filter=Q(mazorca_negra=1)),
         alternaria=Count('pk', filter=Q(alternaria=1)),
         xyleborus=Count('pk', filter=Q(xyleborus=1)),
         trips=Count('pk', filter=Q(trips=1)),
         gallina_ciega=Count('pk', filter=Q(gallina_ciega=1)),
         chinche=Count('pk', filter=Q(chinche=1)),
         zompopos=Count('pk', filter=Q(zompopos=1)),
        )
        hibrido2 = PrevalenciaPlagaPunto2.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],
                    tipo=1).aggregate(
         total=Count('pk'),
         monilia=Count('pk', filter=Q(monilia=1)),
         mazorca_negra=Count('pk', filter=Q(mazorca_negra=1)),
         alternaria=Count('pk', filter=Q(alternaria=1)),
         xyleborus=Count('pk', filter=Q(xyleborus=1)),
         trips=Count('pk', filter=Q(trips=1)),
         gallina_ciega=Count('pk', filter=Q(gallina_ciega=1)),
         chinche=Count('pk', filter=Q(chinche=1)),
         zompopos=Count('pk', filter=Q(zompopos=1)),
        )
        hibrido3 = PrevalenciaPlagaPunto3.objects.filter(ficha__in=filtro,
                ficha__trimestre=obj[0],
                tipo=1).aggregate(
         total=Count('pk'),
         monilia=Count('pk', filter=Q(monilia=1)),
         mazorca_negra=Count('pk', filter=Q(mazorca_negra=1)),
         alternaria=Count('pk', filter=Q(alternaria=1)),
         xyleborus=Count('pk', filter=Q(xyleborus=1)),
         trips=Count('pk', filter=Q(trips=1)),
         gallina_ciega=Count('pk', filter=Q(gallina_ciega=1)),
         chinche=Count('pk', filter=Q(chinche=1)),
         zompopos=Count('pk', filter=Q(zompopos=1)),
        )
        try:  
            conteo_total_h = hibrido1["total"] + hibrido2["total"] + hibrido3["total"]
        except:
            conteo_total_h = 0
        try:
            total_monilia_h = (((hibrido1["monilia"] + hibrido2["monilia"] + hibrido3["monilia"])/conteo_total_h)*100)
        except:
            total_monilia_h = 0
        try:
            total_mazorca_h = (((hibrido1["mazorca_negra"] + hibrido2["mazorca_negra"] + hibrido3["mazorca_negra"])/conteo_total_h)*100)
        except:
            total_mazorca_h = 0
        try:
            total_alternaria_h = (((hibrido1["alternaria"] + hibrido2["alternaria"] + hibrido3["alternaria"])/conteo_total_h)*100)
        except:
            total_alternaria_h = 0
        try:
            total_xyleborus_h = (((hibrido1["xyleborus"] + hibrido2["xyleborus"] + hibrido3["xyleborus"])/conteo_total_h)*100)
        except:
            total_xyleborus_h = 0
        try:
            total_trips_h = (((hibrido1["trips"] + hibrido2["trips"] + hibrido3["trips"])/conteo_total_h)*100)
        except:
            total_trips_h = 0
        try:
            total_gallina_h = (((hibrido1["gallina_ciega"] + hibrido2["gallina_ciega"] + hibrido3["gallina_ciega"])/conteo_total_h)*100)
        except:
            total_gallina_h = 0
        try:
            total_chinche_h = (((hibrido1["chinche"] + hibrido2["chinche"] + hibrido3["chinche"])/conteo_total_h)*100)
        except:
            total_chinche_h = 0
        try:
            total_zompopos_h = (((hibrido1["zompopos"] + hibrido2["zompopos"] + hibrido3["zompopos"])/conteo_total_h)*100)
        except:
            total_zompopos_h = 0

        #clones

        clone1 = PrevalenciaPlagaPunto1.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],
                    tipo=2).aggregate(
         total=Count('pk'),
         monilia=Count('pk', filter=Q(monilia=1)),
         mazorca_negra=Count('pk', filter=Q(mazorca_negra=1)),
         alternaria=Count('pk', filter=Q(alternaria=1)),
         xyleborus=Count('pk', filter=Q(xyleborus=1)),
         trips=Count('pk', filter=Q(trips=1)),
         gallina_ciega=Count('pk', filter=Q(gallina_ciega=1)),
         chinche=Count('pk', filter=Q(chinche=1)),
         zompopos=Count('pk', filter=Q(zompopos=1)),
        )
        clone2 = PrevalenciaPlagaPunto2.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],
                    tipo=2).aggregate(
         total=Count('pk'),
         monilia=Count('pk', filter=Q(monilia=1)),
         mazorca_negra=Count('pk', filter=Q(mazorca_negra=1)),
         alternaria=Count('pk', filter=Q(alternaria=1)),
         xyleborus=Count('pk', filter=Q(xyleborus=1)),
         trips=Count('pk', filter=Q(trips=1)),
         gallina_ciega=Count('pk', filter=Q(gallina_ciega=1)),
         chinche=Count('pk', filter=Q(chinche=1)),
         zompopos=Count('pk', filter=Q(zompopos=1)),
        )
        clone3 = PrevalenciaPlagaPunto3.objects.filter(ficha__in=filtro,
                ficha__trimestre=obj[0],
                tipo=2).aggregate(
         total=Count('pk'),
         monilia=Count('pk', filter=Q(monilia=1)),
         mazorca_negra=Count('pk', filter=Q(mazorca_negra=1)),
         alternaria=Count('pk', filter=Q(alternaria=1)),
         xyleborus=Count('pk', filter=Q(xyleborus=1)),
         trips=Count('pk', filter=Q(trips=1)),
         gallina_ciega=Count('pk', filter=Q(gallina_ciega=1)),
         chinche=Count('pk', filter=Q(chinche=1)),
         zompopos=Count('pk', filter=Q(zompopos=1)),
        )
        try:  
            conteo_total_c = clone1["total"] + clone2["total"] + clone3["total"]
        except:
            conteo_total_c = 0
        try:
            total_monilia_c = (((clone1["monilia"] + clone2["monilia"] + clone3["monilia"])/conteo_total_c)*100)
        except:
            total_monilia_c = 0
        try:
            total_mazorca_c = (((clone1["mazorca_negra"] + clone2["mazorca_negra"] + clone3["mazorca_negra"])/conteo_total_c)*100)
        except:
            total_mazorca_c = 0
        try:
            total_alternaria_c = (((clone1["alternaria"] + clone2["alternaria"] + clone3["alternaria"])/conteo_total_c)*100)
        except:
            total_alternaria_c = 0
        try:
            total_xyleborus_c = (((clone1["xyleborus"] + clone2["xyleborus"] + clone3["xyleborus"])/conteo_total_c)*100)
        except:
            total_xyleborus_c = 0
        try:
            total_trips_c = (((clone1["trips"] + clone2["trips"] + clone3["trips"])/conteo_total_c)*100)
        except:
            total_trips_c = 0
        try:
            total_gallina_c = (((clone1["gallina_ciega"] + clone2["gallina_ciega"] + clone3["gallina_ciega"])/conteo_total_c)*100)
        except:
            total_gallina_c = 0
        try:
            total_chinche_c = (((clone1["chinche"] + clone2["chinche"] + clone3["chinche"])/conteo_total_c)*100)
        except:
            total_chinche_c = 0
        try:
            total_zompopos_c = (((clone1["zompopos"] + clone2["zompopos"] + clone3["zompopos"])/conteo_total_c)*100)
        except:
            total_zompopos_c = 0


        if conteo_total_h or conteo_total_c > 0:
            salida[obj[1]] = (conteo_total_h,total_monilia_h,total_mazorca_h,
                          total_alternaria_h,total_xyleborus_h,total_trips_h,
                          total_chinche_h,total_gallina_h,total_zompopos_h,
                          conteo_total_c,total_monilia_c,total_mazorca_c,
                          total_alternaria_c,total_xyleborus_c,total_trips_c,
                          total_chinche_c,total_gallina_c,total_zompopos_c,
                          num_familias1)

            
    return render(request, template, locals())


def estado_musaceas(request, template="visitas/estado_musaceas.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    salida = {}
    otra_salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        otra_salida[obj[1]] = (num_familias1,)
        if num_familias1 > 0:
            salida[obj[1]] = {}
            for musa in PlantasMusaceas.objects.all():
                tabla1 = EstadoActualPunto1Musaceas.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],tipo=musa).aggregate(
                                             conteo=Count('pk'),
                                             plagas=Count('pk', filter=Q(plagas=1)),
                                             enfermedades=Count('pk', filter=Q(enfermedades=1)),
                                             deficiencia=Count('pk', filter=Q(deficiencia=1)),
                                             foliar=Count('pk', filter=Q(foliar=1)),
                                             floracion=Count('pk', filter=Q(floracion=1)),
                                             fructificacion=Count('pk', filter=Q(fructificacion=1)),
                                             )
                tabla2 = EstadoActualPunto2Musaceas.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],tipo=musa).aggregate(
                                             conteo=Count('pk'),
                                             plagas=Count('pk', filter=Q(plagas=1)),
                                             enfermedades=Count('pk', filter=Q(enfermedades=1)),
                                             deficiencia=Count('pk', filter=Q(deficiencia=1)),
                                             foliar=Count('pk', filter=Q(foliar=1)),
                                             floracion=Count('pk', filter=Q(floracion=1)),
                                             fructificacion=Count('pk', filter=Q(fructificacion=1)),
                                             )
                tabla3 = EstadoActualPunto3Musaceas.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],tipo=musa).aggregate(
                                             conteo=Count('pk'),
                                             plagas=Count('pk', filter=Q(plagas=1)),
                                             enfermedades=Count('pk', filter=Q(enfermedades=1)),
                                             deficiencia=Count('pk', filter=Q(deficiencia=1)),
                                             foliar=Count('pk', filter=Q(foliar=1)),
                                             floracion=Count('pk', filter=Q(floracion=1)),
                                             fructificacion=Count('pk', filter=Q(fructificacion=1)),
                                             )

                query1 = EstadoActualPunto1Musaceas.objects.filter(ficha__trimestre=obj[0],
                                      ficha__in=filtro,tipo=musa).values_list('altura',flat=True)
                query2 = EstadoActualPunto2Musaceas.objects.filter(ficha__trimestre=obj[0],
                                      ficha__in=filtro,tipo=musa).values_list('altura',flat=True)
                query3 = EstadoActualPunto3Musaceas.objects.filter(ficha__trimestre=obj[0],
                                      ficha__in=filtro,tipo=musa).values_list('altura',flat=True)
                query_total = query1.union(query2,query3)

                total_conteo = tabla1['conteo']+tabla2['conteo']+tabla3['conteo']
                
                try:
                    altura_toda = list(query_total)
                    total_promedio = np.mean(altura_toda)
                except:
                    total_promedio = 0
                try:
                    media_toda = list(query_total)
                    total_mediana = np.median(media_toda)
                except:
                    total_mediana = 0
                try:
                    por_plagas = (((tabla1['plagas']+tabla2['plagas']+tabla3['plagas'])/total_conteo)*100)
                except:
                    por_plagas = 0
                try:
                    por_enfermedades = (((tabla1['enfermedades']+tabla2['enfermedades']+tabla3['enfermedades'])/total_conteo)*100)
                except:
                    por_enfermedades = 0
                try:
                    por_deficiencia = (((tabla1['deficiencia']+tabla2['deficiencia']+tabla3['deficiencia'])/total_conteo)*100)
                except:
                    por_deficiencia = 0
                try:
                    por_foliar = (((tabla1['foliar']+tabla2['foliar']+tabla3['foliar'])/total_conteo)*100)
                except:
                    por_foliar = 0
                try:
                    por_floracion = (((tabla1['floracion']+tabla2['floracion']+tabla3['floracion'])/total_conteo)*100)
                except:
                    por_floracion = 0
                try:
                    por_fructificacion = (((tabla1['fructificacion']+tabla2['fructificacion']+tabla3['fructificacion'])/total_conteo)*100)
                except:
                    por_fructificacion = 0

                if total_conteo > 0:
                    salida[obj[1]][musa] = (total_conteo,total_promedio,total_mediana,
                                        por_plagas,por_enfermedades,por_deficiencia,
                                        por_foliar,por_floracion,por_fructificacion)
    
    return render(request, template, locals())


def estado_frutales(request, template="visitas/estado_frutales.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    salida = {}
    otra_salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        otra_salida[obj[1]] = (num_familias1,)
        if num_familias1 > 0:
            salida[obj[1]] = {}
            for musa in PlantasFrutales.objects.all():
                tabla1 = EstadoActualPunto1Frutales.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],especies=musa).aggregate(
                                             conteo=Count('pk'),
                                             plagas=Count('pk', filter=Q(plagas=1)),
                                             enfermedades=Count('pk', filter=Q(enfermedades=1)),
                                             deficiencia=Count('pk', filter=Q(deficiencia=1)),
                                             crecimiento=Count('pk', filter=Q(crecimiento=1)),
                                             floracion=Count('pk', filter=Q(floracion=1)),
                                             fructificacion=Count('pk', filter=Q(fructificacion=1)),
                                             )
                tabla2 = EstadoActualPunto2Frutales.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],especies=musa).aggregate(
                                             conteo=Count('pk'),
                                             plagas=Count('pk', filter=Q(plagas=1)),
                                             enfermedades=Count('pk', filter=Q(enfermedades=1)),
                                             deficiencia=Count('pk', filter=Q(deficiencia=1)),
                                             crecimiento=Count('pk', filter=Q(crecimiento=1)),
                                             floracion=Count('pk', filter=Q(floracion=1)),
                                             fructificacion=Count('pk', filter=Q(fructificacion=1)),
                                             )
                tabla3 = EstadoActualPunto3Frutales.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],especies=musa).aggregate(
                                             conteo=Count('pk'),
                                             plagas=Count('pk', filter=Q(plagas=1)),
                                             enfermedades=Count('pk', filter=Q(enfermedades=1)),
                                             deficiencia=Count('pk', filter=Q(deficiencia=1)),
                                             crecimiento=Count('pk', filter=Q(crecimiento=1)),
                                             floracion=Count('pk', filter=Q(floracion=1)),
                                             fructificacion=Count('pk', filter=Q(fructificacion=1)),
                                             )
                query1 = EstadoActualPunto1Frutales.objects.filter(ficha__trimestre=obj[0],
                                      ficha__in=filtro,especies=musa).values_list('altura',flat=True)
                query2 = EstadoActualPunto2Frutales.objects.filter(ficha__trimestre=obj[0],
                                      ficha__in=filtro,especies=musa).values_list('altura',flat=True)
                query3 = EstadoActualPunto3Frutales.objects.filter(ficha__trimestre=obj[0],
                                      ficha__in=filtro,especies=musa).values_list('altura',flat=True)
                query_total = query1.union(query2,query3)

                total_conteo = tabla1['conteo']+tabla2['conteo']+tabla3['conteo']
                
                try:
                    altura_toda = list(query_total)
                    total_promedio = np.mean(altura_toda)
                except:
                    total_promedio = 0
                try:
                    media_toda = list(query_total)
                    total_mediana = np.median(media_toda)
                except:
                    total_mediana = 0
                try:
                    por_plagas = (((tabla1['plagas']+tabla2['plagas']+tabla3['plagas'])/total_conteo)*100)
                except:
                    por_plagas = 0
                try:
                    por_enfermedades = (((tabla1['enfermedades']+tabla2['enfermedades']+tabla3['enfermedades'])/total_conteo)*100)
                except:
                    por_enfermedades = 0
                try:
                    por_deficiencia = (((tabla1['deficiencia']+tabla2['deficiencia']+tabla3['deficiencia'])/total_conteo)*100)
                except:
                    por_deficiencia = 0
                try:
                    por_crecimiento = (((tabla1['crecimiento']+tabla2['crecimiento']+tabla3['crecimiento'])/total_conteo)*100)
                except:
                    por_crecimiento = 0
                try:
                    por_floracion = (((tabla1['floracion']+tabla2['floracion']+tabla3['floracion'])/total_conteo)*100)
                except:
                    por_floracion = 0
                try:
                    por_fructificacion = (((tabla1['fructificacion']+tabla2['fructificacion']+tabla3['fructificacion'])/total_conteo)*100)
                except:
                    por_fructificacion = 0

                if total_conteo > 0:
                    salida[obj[1]][musa] = (total_conteo,total_promedio,total_mediana,
                                        por_plagas,por_enfermedades,por_deficiencia,
                                        por_crecimiento,por_floracion,por_fructificacion)
    
    return render(request, template, locals())

def estado_maderables(request, template="visitas/estado_maderables.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    salida = {}
    otra_salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        otra_salida[obj[1]] = (num_familias1,)
        if num_familias1 > 0:
            salida[obj[1]] = {}
            for musa in PlantasMaderables.objects.all():
                tabla1 = EstadoActualPunto1Maderables.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],especies=musa).aggregate(
                                             conteo=Count('pk'),
                                             plagas=Count('pk', filter=Q(plagas=1)),
                                             enfermedades=Count('pk', filter=Q(enfermedades=1)),
                                             deficiencia=Count('pk', filter=Q(deficiencia=1)),
                                             crecimiento=Count('pk', filter=Q(crecimiento=1)),
                                             
                                             )
                tabla2 = EstadoActualPunto2Maderables.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],especies=musa).aggregate(
                                             conteo=Count('pk'),
                                             plagas=Count('pk', filter=Q(plagas=1)),
                                             enfermedades=Count('pk', filter=Q(enfermedades=1)),
                                             deficiencia=Count('pk', filter=Q(deficiencia=1)),
                                             crecimiento=Count('pk', filter=Q(crecimiento=1)),
                                             
                                             )
                tabla3 = EstadoActualPunto3Maderables.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],especies=musa).aggregate(
                                             conteo=Count('pk'),
                                             plagas=Count('pk', filter=Q(plagas=1)),
                                             enfermedades=Count('pk', filter=Q(enfermedades=1)),
                                             deficiencia=Count('pk', filter=Q(deficiencia=1)),
                                             crecimiento=Count('pk', filter=Q(crecimiento=1)),
                                             
                                             )
                query1 = EstadoActualPunto1Maderables.objects.filter(ficha__trimestre=obj[0],
                                      ficha__in=filtro,especies=musa).values_list('altura',flat=True)
                query2 = EstadoActualPunto2Maderables.objects.filter(ficha__trimestre=obj[0],
                                      ficha__in=filtro,especies=musa).values_list('altura',flat=True)
                query3 = EstadoActualPunto3Maderables.objects.filter(ficha__trimestre=obj[0],
                                      ficha__in=filtro,especies=musa).values_list('altura',flat=True)
                query_total = query1.union(query2,query3)

                total_conteo = tabla1['conteo']+tabla2['conteo']+tabla3['conteo']
                
                try:
                    altura_toda = list(query_total)
                    total_promedio = np.mean(altura_toda)
                except:
                    total_promedio = 0
                try:
                    media_toda = list(query_total)
                    total_mediana = np.median(media_toda)
                except:
                    total_mediana = 0
                try:
                    por_plagas = (((tabla1['plagas']+tabla2['plagas']+tabla3['plagas'])/total_conteo)*100)
                except:
                    por_plagas = 0
                try:
                    por_enfermedades = (((tabla1['enfermedades']+tabla2['enfermedades']+tabla3['enfermedades'])/total_conteo)*100)
                except:
                    por_enfermedades = 0
                try:
                    por_deficiencia = (((tabla1['deficiencia']+tabla2['deficiencia']+tabla3['deficiencia'])/total_conteo)*100)
                except:
                    por_deficiencia = 0
                try:
                    por_crecimiento = (((tabla1['crecimiento']+tabla2['crecimiento']+tabla3['crecimiento'])/total_conteo)*100)
                except:
                    por_crecimiento = 0

                if total_conteo > 0:
                    salida[obj[1]][musa] = (total_conteo,total_promedio,total_mediana,
                                        por_plagas,por_enfermedades,por_deficiencia,
                                        por_crecimiento)
    
    return render(request, template, locals())

def estado_arboles(request, template="visitas/estado_arboles.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    salida = {}
    otra_salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        otra_salida[obj[1]] = (num_familias1,)
        if num_familias1 > 0:
            salida[obj[1]] = {}
            for musa in ArbolesServicios.objects.all():
                tabla1 = EstadoActualPunto1Arboles.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],especies=musa).aggregate(
                                             conteo=Count('pk'),
                                             plagas=Count('pk', filter=Q(plagas=1)),
                                             enfermedades=Count('pk', filter=Q(enfermedades=1)),
                                             deficiencia=Count('pk', filter=Q(deficiencia=1)),
                                             crecimiento=Count('pk', filter=Q(crecimiento=1)),
                                             
                                             )
                tabla2 = EstadoActualPunto2Arboles.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],especies=musa).aggregate(
                                             conteo=Count('pk'),
                                             plagas=Count('pk', filter=Q(plagas=1)),
                                             enfermedades=Count('pk', filter=Q(enfermedades=1)),
                                             deficiencia=Count('pk', filter=Q(deficiencia=1)),
                                             crecimiento=Count('pk', filter=Q(crecimiento=1)),
                                             
                                             )
                tabla3 = EstadoActualPunto3Arboles.objects.filter(ficha__in=filtro,
                    ficha__trimestre=obj[0],especies=musa).aggregate(
                                             conteo=Count('pk'),
                                             plagas=Count('pk', filter=Q(plagas=1)),
                                             enfermedades=Count('pk', filter=Q(enfermedades=1)),
                                             deficiencia=Count('pk', filter=Q(deficiencia=1)),
                                             crecimiento=Count('pk', filter=Q(crecimiento=1)),
                                             
                                             )
                query1 = EstadoActualPunto1Arboles.objects.filter(ficha__trimestre=obj[0],
                                      ficha__in=filtro,especies=musa).values_list('altura',flat=True)
                query2 = EstadoActualPunto2Arboles.objects.filter(ficha__trimestre=obj[0],
                                      ficha__in=filtro,especies=musa).values_list('altura',flat=True)
                query3 = EstadoActualPunto3Arboles.objects.filter(ficha__trimestre=obj[0],
                                      ficha__in=filtro,especies=musa).values_list('altura',flat=True)
                query_total = query1.union(query2,query3)

                total_conteo = tabla1['conteo']+tabla2['conteo']+tabla3['conteo']
                
                try:
                    altura_toda = list(query_total)
                    total_promedio = np.mean(altura_toda)
                except:
                    total_promedio = 0
                try:
                    media_toda = list(query_total)
                    total_mediana = np.median(media_toda)
                except:
                    total_mediana = 0
                try:
                    por_plagas = (((tabla1['plagas']+tabla2['plagas']+tabla3['plagas'])/total_conteo)*100)
                except:
                    por_plagas = 0
                try:
                    por_enfermedades = (((tabla1['enfermedades']+tabla2['enfermedades']+tabla3['enfermedades'])/total_conteo)*100)
                except:
                    por_enfermedades = 0
                try:
                    por_deficiencia = (((tabla1['deficiencia']+tabla2['deficiencia']+tabla3['deficiencia'])/total_conteo)*100)
                except:
                    por_deficiencia = 0
                try:
                    por_crecimiento = (((tabla1['crecimiento']+tabla2['crecimiento']+tabla3['crecimiento'])/total_conteo)*100)
                except:
                    por_crecimiento = 0

                if total_conteo > 0:
                    salida[obj[1]][musa] = (total_conteo,total_promedio,total_mediana,
                                        por_plagas,por_enfermedades,por_deficiencia,
                                        por_crecimiento)

    return render(request, template, locals())


def piso_cacaotal(request, template="visitas/piso_cacaotal.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        if num_familias1 > 0:
            tabla_composicion = OrderedDict()
            for objecto in CHOICE_PISO:
                conteo = DatosPiso.objects.select_related('ficha').filter(estado=objecto[0],
                                ficha__trimestre=obj[0], ficha__in=filtro).count()
                suma = DatosPiso.objects.select_related('ficha').filter(estado=objecto[0],
                                ficha__trimestre=obj[0],ficha__in=filtro).aggregate(total=Sum('conteo'))['total'] or 0

                tabla_composicion[objecto[1]] = suma

            VAR_TOTAL = 0
            for k,v in tabla_composicion.items():
                try:
                    VAR_TOTAL += v
                except:
                    VAR_TOTAL = 0

            salida[obj[1]] = (num_familias1,tabla_composicion,VAR_TOTAL)
    
    return render(request, template, locals())


def actividades_cacao(request, template="visitas/actvidades_cacao.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        if num_familias1 > 0:
            tabla_trabajo = OrderedDict()
            for objecto in CHOICE_LABORES_CACAO:
                grafo = ActividadesCacao.objects.select_related('ficha').filter(labores=objecto[0],
                                         ficha__trimestre=obj[0],
                                         ficha__in=filtro).aggregate(
                                         conteo=Count('pk'),
                                         no_realiza=Count('pk', filter=Q(opcion=1)),
                                         mala=Count('pk', filter=Q(opcion=2)),
                                         regular=Count('pk', filter=Q(opcion=3)),
                                         buena=Count('pk', filter=Q(opcion=4)),
                                         planificado=Count('pk', filter=Q(planificado=1)),
                                        )
                
                if grafo['conteo'] > 0:
                    tabla_trabajo[objecto[1]] = [grafo['no_realiza'],grafo['mala'],
                                                grafo['regular'],grafo['buena'],
                                                grafo['planificado']]

            salida[obj[1]] = (num_familias1,tabla_trabajo)
    
    return render(request, template, locals())

def actividades_musaceas(request, template="visitas/actvidades_musaceas.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        if num_familias1 > 0:
            tabla_trabajo = OrderedDict()
            for objecto in CHOICE_LABORES_MUSACEAS:
                grafo = ActividadesMusaceas.objects.select_related('ficha').filter(labores=objecto[0],
                                         ficha__trimestre=obj[0],
                                         ficha__in=filtro).aggregate(
                                         conteo=Count('pk'),
                                         no_realiza=Count('pk', filter=Q(opcion=1)),
                                         mala=Count('pk', filter=Q(opcion=2)),
                                         regular=Count('pk', filter=Q(opcion=3)),
                                         buena=Count('pk', filter=Q(opcion=4)),
                                         planificado=Count('pk', filter=Q(planificado=1)),
                                        )
                
                if grafo['conteo'] > 0:
                    tabla_trabajo[objecto[1]] = [grafo['no_realiza'],grafo['mala'],
                                                grafo['regular'],grafo['buena'],
                                                grafo['planificado']]

            salida[obj[1]] = (num_familias1,tabla_trabajo)
    
    return render(request, template, locals())

def actividades_frutales(request, template="visitas/actvidades_frutales.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        if num_familias1 > 0:
            tabla_trabajo = OrderedDict()
            for objecto in CHOICE_LABORES_FRUTALES:
                grafo = ActividadesFrutales.objects.select_related('ficha').filter(labores=objecto[0],
                                         ficha__trimestre=obj[0],
                                         ficha__in=filtro).aggregate(
                                         conteo=Count('pk'),
                                         no_realiza=Count('pk', filter=Q(opcion=1)),
                                         mala=Count('pk', filter=Q(opcion=2)),
                                         regular=Count('pk', filter=Q(opcion=3)),
                                         buena=Count('pk', filter=Q(opcion=4)),
                                         planificado=Count('pk', filter=Q(planificado=1)),
                                        )
                
                if grafo['conteo'] > 0:
                    tabla_trabajo[objecto[1]] = [grafo['no_realiza'],grafo['mala'],
                                                grafo['regular'],grafo['buena'],
                                                grafo['planificado']]

            salida[obj[1]] = (num_familias1,tabla_trabajo)
    
    return render(request, template, locals())

def actividades_maderables(request, template="visitas/actividades_maderables.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        if num_familias1 > 0:
            tabla_trabajo = OrderedDict()
            for objecto in CHOICE_LABORES_MADERABLES:
                grafo = ActividadesMaderables.objects.select_related('ficha').filter(labores=objecto[0],
                                         ficha__trimestre=obj[0],
                                         ficha__in=filtro).aggregate(
                                         conteo=Count('pk'),
                                         no_realiza=Count('pk', filter=Q(opcion=1)),
                                         mala=Count('pk', filter=Q(opcion=2)),
                                         regular=Count('pk', filter=Q(opcion=3)),
                                         buena=Count('pk', filter=Q(opcion=4)),
                                         planificado=Count('pk', filter=Q(planificado=1)),
                                        )
                
                if grafo['conteo'] > 0:
                    tabla_trabajo[objecto[1]] = [grafo['no_realiza'],grafo['mala'],
                                                grafo['regular'],grafo['buena'],
                                                grafo['planificado']]

            salida[obj[1]] = (num_familias1,tabla_trabajo)
    
    return render(request, template, locals())

def actividades_arboles(request, template="visitas/actividades_arboles.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        if num_familias1 > 0:
            tabla_trabajo = OrderedDict()
            for objecto in CHOICE_LABORES_ARBOLES_SERVICIOS:
                grafo = ActividadesArboles.objects.select_related('ficha').filter(labores=objecto[0],
                                         ficha__trimestre=obj[0],
                                         ficha__in=filtro).aggregate(
                                         conteo=Count('pk'),
                                         no_realiza=Count('pk', filter=Q(opcion=1)),
                                         mala=Count('pk', filter=Q(opcion=2)),
                                         regular=Count('pk', filter=Q(opcion=3)),
                                         buena=Count('pk', filter=Q(opcion=4)),
                                         planificado=Count('pk', filter=Q(planificado=1)),
                                        )
                
                if grafo['conteo'] > 0:
                    tabla_trabajo[objecto[1]] = [grafo['no_realiza'],grafo['mala'],
                                                grafo['regular'],grafo['buena'],
                                                grafo['planificado']]

            salida[obj[1]] = (num_familias1,tabla_trabajo)
    
    return render(request, template, locals())

def visita_cosecha(request, template="visitas/visita_cosecha.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        area_total = InventarioParcela.objects.select_related('ficha').filter(
                    ficha__in=filtro,
                    ficha__trimestre=obj[0],
                    area__gt=0).aggregate(t=Sum('area'))['t']
        if num_familias1 > 0:
            tabla_trabajo = OrderedDict()
            for objecto in ProductosParcela.objects.all():
                tabla = CosechaParcelaTrimestre.objects.select_related('ficha','productos').filter(
                                         productos=objecto,
                                         ficha__trimestre=obj[0],
                                         ficha__in=filtro).aggregate(
                                         conteo=Count('pk'),
                                         produccion=Sum('cantidad'),
                                         intermediario=Count('pk', filter=Q(vendio__contains='A')),
                                         cooperativa=Count('pk', filter=Q(vendio__contains='B')),
                                         asociacion=Count('pk', filter=Q(vendio__contains='C')),
                                         empresa=Count('pk', filter=Q(vendio__contains='D')),
                                         consumo=Count('pk', filter=Q(vendio__contains='E')),
                                        )
                
                if tabla['conteo'] > 0:
                    tabla_trabajo[objecto.nombre] = [tabla['conteo'],tabla['produccion'],
                                              objecto.get_unidad_display,tabla['intermediario'],
                                              tabla['cooperativa'],tabla['asociacion'],
                                              tabla['empresa'],tabla['consumo']
                                            ]

            salida[obj[1]] = (num_familias1,tabla_trabajo,area_total)
    
    return render(request, template, locals())

def visita_cosecha_finca(request, template="visitas/visita_cosecha_finca.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        area_total = InventarioParcela.objects.select_related('ficha').filter(
                    ficha__in=filtro,
                    ficha__trimestre=obj[0],
                    area__gt=0).aggregate(t=Sum('area'))['t']
        if num_familias1 > 0:
            tabla_trabajo = OrderedDict()
            for objecto in ProductosFinca.objects.all():
                tabla = CosechaFincaTrimestre.objects.select_related('ficha','productos').filter(
                                         productos=objecto,
                                         ficha__trimestre=obj[0],
                                         ficha__in=filtro).aggregate(
                                         conteo=Count('pk'),
                                         produccion=Sum('cantidad'),
                                         intermediario=Count('pk', filter=Q(vendio__contains='A')),
                                         cooperativa=Count('pk', filter=Q(vendio__contains='B')),
                                         asociacion=Count('pk', filter=Q(vendio__contains='C')),
                                         empresa=Count('pk', filter=Q(vendio__contains='D')),
                                         consumo=Count('pk', filter=Q(vendio__contains='E')),
                                        )
                
                if tabla['conteo'] > 0:
                    tabla_trabajo[objecto.nombre] = [tabla['conteo'],tabla['produccion'],
                                              objecto.get_unidad_display,tabla['intermediario'],
                                              tabla['cooperativa'],tabla['asociacion'],
                                              tabla['empresa'],tabla['consumo']
                                            ]

            salida[obj[1]] = (num_familias1,tabla_trabajo,area_total)
    
    return render(request, template, locals())

def inversiones_finca(request, template="visitas/inversiones_finca.html"):
    filtro = _queryset_filtrado_visitas(request)
    num_familias = filtro.count()

    salida = {}
    otra_salida = {}
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        area_total = InventarioParcela.objects.select_related('ficha').filter(
                    ficha__in=filtro,
                    ficha__trimestre=obj[0],
                    area__gt=0).aggregate(t=Sum('area'))['t']
        otra_salida[obj[1]] = (num_familias1,)
        if num_familias1 > 0:
            tabla_trabajo = OrderedDict()
            for producto in ProductosInversion.objects.all():
                tabla = InversionesFinca.objects.select_related('productos','ficha').filter(ficha__in=filtro,
                    productos=producto, ficha__trimestre=obj[0]).aggregate(
                    conteo=Count('pk'),
                    cantidad=Sum('cantidad'),
                    manoObra=Sum('mano_obra'),
                    contra=Sum('contrapartida'),
                    familiar=Sum('contrapartida_familiar')
                )
                if tabla['conteo'] > 0:
                    tabla_trabajo[producto.nombre] = (tabla['cantidad'],
                                               tabla['manoObra'],
                                               tabla['contra'],
                                               tabla['familiar'],
                                            )
            salida[obj[1]] = (num_familias1,tabla_trabajo,area_total)

    return render(request, template, locals())


def estimacion_cosecha_cacao(request, template="visitas/estimacion_cosecha.html"):
    filtro = _queryset_filtrado_visitas(request)
    
    num_familias = filtro.count()
    salida = {}
    
    for obj in CHOICES_TRIMESTRE:
        num_familias1 = filtro.filter(trimestre=obj[0]).count()
        if num_familias1 > 0:
            hibrido = InventarioParcela.objects.filter(ficha__in=filtro, 
                                                   ficha__trimestre=obj[0],
                                                   area__gt=0,
                                                   plantas_hibridas__gt=0).aggregate(
                                                   avg_t=Coalesce(ExpressionWrapper(Sum(F('plantas_hibridas')) / Sum(F('area')),output_field=FloatField()),0 ))
            injertos = InventarioParcela.objects.filter(ficha__in=filtro, 
                                                   ficha__trimestre=obj[0],
                                                   area__gt=0,
                                                   plantas_hibridas__gt=0).aggregate(
                                                   avg_t=Coalesce(ExpressionWrapper(Sum(F('platas_injertadas')) / Sum(F('area')),output_field=FloatField()),0 ))
            
            mazorcasana1 = EstimadoCosechaCacaoPunto1.objects.filter(ficha__in=filtro, 
                                                   ficha__trimestre=obj[0],opcion=1).aggregate(
                                                   total=Coalesce(ExpressionWrapper(Sum(F('planta1')) + 
                                                                           Sum(F('planta2')) +
                                                                           Sum(F('planta3')) +
                                                                           Sum(F('planta4')) +
                                                                           Sum(F('planta5')) +
                                                                           Sum(F('planta6')) +
                                                                           Sum(F('planta7')) +
                                                                           Sum(F('planta8')) +
                                                                           Sum(F('planta9')) +
                                                                           Sum(F('planta10'))
                                                                           ,output_field=FloatField() 
                                                                        ), 0)
                                                   )
            mazorcasana2 = EstimadoCosechaCacaoPunto2.objects.filter(ficha__in=filtro, 
                                                   ficha__trimestre=obj[0],opcion=1).aggregate(
                                                   total=Coalesce(ExpressionWrapper(Sum(F('planta1')) + 
                                                                           Sum(F('planta2')) +
                                                                           Sum(F('planta3')) +
                                                                           Sum(F('planta4')) +
                                                                           Sum(F('planta5')) +
                                                                           Sum(F('planta6')) +
                                                                           Sum(F('planta7')) +
                                                                           Sum(F('planta8')) +
                                                                           Sum(F('planta9')) +
                                                                           Sum(F('planta10'))
                                                                           ,output_field=FloatField() 
                                                                        ), 0)
                                                   )
            mazorcasana3 = EstimadoCosechaCacaoPunto3.objects.filter(ficha__in=filtro, 
                                                   ficha__trimestre=obj[0],opcion=1).aggregate(
                                                   total=Coalesce(ExpressionWrapper(Sum(F('planta1')) + 
                                                                           Sum(F('planta2')) +
                                                                           Sum(F('planta3')) +
                                                                           Sum(F('planta4')) +
                                                                           Sum(F('planta5')) +
                                                                           Sum(F('planta6')) +
                                                                           Sum(F('planta7')) +
                                                                           Sum(F('planta8')) +
                                                                           Sum(F('planta9')) +
                                                                           Sum(F('planta10'))
                                                                           ,output_field=FloatField()
                                                                        ), 0)
                                                   )
            #mazorca enferma
            mazorcaenferma1 = EstimadoCosechaCacaoPunto1.objects.filter(ficha__in=filtro, 
                                                   ficha__trimestre=obj[0],opcion=2).aggregate(
                                                   total=Coalesce(ExpressionWrapper(Sum(F('planta1')) + 
                                                                           Sum(F('planta2')) +
                                                                           Sum(F('planta3')) +
                                                                           Sum(F('planta4')) +
                                                                           Sum(F('planta5')) +
                                                                           Sum(F('planta6')) +
                                                                           Sum(F('planta7')) +
                                                                           Sum(F('planta8')) +
                                                                           Sum(F('planta9')) +
                                                                           Sum(F('planta10'))
                                                                           ,output_field=FloatField() 
                                                                        ), 0)
                                                   )
            mazorcaenferma2 = EstimadoCosechaCacaoPunto2.objects.filter(ficha__in=filtro, 
                                                   ficha__trimestre=obj[0],opcion=2).aggregate(
                                                   total=Coalesce(ExpressionWrapper(Sum(F('planta1')) + 
                                                                           Sum(F('planta2')) +
                                                                           Sum(F('planta3')) +
                                                                           Sum(F('planta4')) +
                                                                           Sum(F('planta5')) +
                                                                           Sum(F('planta6')) +
                                                                           Sum(F('planta7')) +
                                                                           Sum(F('planta8')) +
                                                                           Sum(F('planta9')) +
                                                                           Sum(F('planta10'))
                                                                           ,output_field=FloatField() 
                                                                        ), 0)
                                                   )
            mazorcaenferma3 = EstimadoCosechaCacaoPunto3.objects.filter(ficha__in=filtro, 
                                                   ficha__trimestre=obj[0],opcion=2).aggregate(
                                                   total=Coalesce(ExpressionWrapper(Sum(F('planta1')) + 
                                                                           Sum(F('planta2')) +
                                                                           Sum(F('planta3')) +
                                                                           Sum(F('planta4')) +
                                                                           Sum(F('planta5')) +
                                                                           Sum(F('planta6')) +
                                                                           Sum(F('planta7')) +
                                                                           Sum(F('planta8')) +
                                                                           Sum(F('planta9')) +
                                                                           Sum(F('planta10'))
                                                                           ,output_field=FloatField()
                                                                        ), 0)
                                                   )
            #mazorca danadas
            mazorcadanada1 = EstimadoCosechaCacaoPunto1.objects.filter(ficha__in=filtro, 
                                                   ficha__trimestre=obj[0],opcion=3).aggregate(
                                                   total=Coalesce(ExpressionWrapper(Sum(F('planta1')) + 
                                                                           Sum(F('planta2')) +
                                                                           Sum(F('planta3')) +
                                                                           Sum(F('planta4')) +
                                                                           Sum(F('planta5')) +
                                                                           Sum(F('planta6')) +
                                                                           Sum(F('planta7')) +
                                                                           Sum(F('planta8')) +
                                                                           Sum(F('planta9')) +
                                                                           Sum(F('planta10'))
                                                                           ,output_field=FloatField() 
                                                                        ), 0)
                                                   )
            mazorcadanada2 = EstimadoCosechaCacaoPunto2.objects.filter(ficha__in=filtro, 
                                                   ficha__trimestre=obj[0],opcion=3).aggregate(
                                                   total=Coalesce(ExpressionWrapper(Sum(F('planta1')) + 
                                                                           Sum(F('planta2')) +
                                                                           Sum(F('planta3')) +
                                                                           Sum(F('planta4')) +
                                                                           Sum(F('planta5')) +
                                                                           Sum(F('planta6')) +
                                                                           Sum(F('planta7')) +
                                                                           Sum(F('planta8')) +
                                                                           Sum(F('planta9')) +
                                                                           Sum(F('planta10'))
                                                                           ,output_field=FloatField() 
                                                                        ), 0)
                                                   )
            mazorcadanada3 = EstimadoCosechaCacaoPunto3.objects.filter(ficha__in=filtro, 
                                                   ficha__trimestre=obj[0],opcion=3).aggregate(
                                                   total=Coalesce(ExpressionWrapper(Sum(F('planta1')) + 
                                                                           Sum(F('planta2')) +
                                                                           Sum(F('planta3')) +
                                                                           Sum(F('planta4')) +
                                                                           Sum(F('planta5')) +
                                                                           Sum(F('planta6')) +
                                                                           Sum(F('planta7')) +
                                                                           Sum(F('planta8')) +
                                                                           Sum(F('planta9')) +
                                                                           Sum(F('planta10'))
                                                                           ,output_field=FloatField()
                                                                        ), 0)
                                                   )

            total_mazorca_sana =  mazorcasana1['total'] + mazorcasana2['total'] + mazorcasana3['total']
            total_mazorca_enferma = mazorcaenferma1['total'] + mazorcaenferma2['total'] + mazorcaenferma3['total']
            total_mazorca_danada = mazorcadanada1['total'] + mazorcadanada2['total'] + mazorcadanada3['total']
            
            #formulas falguni
            try:
                PO = 30 * num_familias1
            except:
                PO = 0
            try:
                MSO = total_mazorca_sana
            except:
                MSO = 0
            try:
                MEO = total_mazorca_enferma
            except:
                MEO = 0
            try:
                MDO = total_mazorca_danada
            except:
                MDO = 0

            try:
                NPM = hibrido['avg_t'] + injertos['avg_t']
            except:
                NPM = 0
            try:
                NMSM =  round((MSO/PO) * NPM,2)
            except:
                NMSM = 0
            try:
                COS = round((NMSM / (5*100)),2)
            except:
                COS = 0
            try:
                NMEM = round((MEO/PO) * NPM,2)
            except:
                NMEM = 0
            try:
                PEN = round((NMEM / (5*100)),2)
            except:
                PEN = 0
            try:
                NMDM = round((MDO/PO) * NPM,2)
            except:
                NMDM = 0
            try:
                PDA = round((NMDM/ (5*100)),2)
            except:
                PDA = 0
            try:
                PPE = round((PEN/(COS+PEN*PDA)) * 100,2)
            except:
                PPE = 0
            try:
                PPDARP = round((PDA/(COS+PEN*PDA)) *100,2)
            except:
                PPDARP = 0

            salida[obj[1]] = (num_familias1,PO,MSO,MEO,
                              MDO,NPM,NMSM,COS,NMEM,PEN,
                              NMDM,PDA,PPE,PPDARP)
    
    return render(request, template, locals())

def funcion_altura(lista):
    lista1 = []
    for x in lista:
       lista1.append(x[0])
    return lista1

def funcion_largo(lista):
    lista1 = []
    for x in lista:
       lista1.append(x[1])
    return lista1

def get_view_visita(request, vista):
    if vista in VALID_VIEWS_VISITA:
        return VALID_VIEWS_VISITA[vista](request)
    else:
        raise ViewDoesNotExist("Tried %s in module %s Error: View not defined in VALID_VIEWS." % (vista, 'ficha_cierre.views'))


VALID_VIEWS_VISITA = {
        'inventario_saf':      inventario_saf_cacao,
        'estado_plantas':      estado_planta_cacao,
        'plagas_enfermedades': plagas_enfermedades_cacao,
        'inversiones':         inversiones_finca,
        'estado_musaceas':     estado_musaceas,
        'estado_frutales':     estado_frutales,
        'estado_maderables':   estado_maderables,
        'estado_arboles':      estado_arboles,
        'piso_cacaotal':       piso_cacaotal,
        'actividades_cacao':   actividades_cacao,
        'actividades_musaceas':actividades_musaceas,
        'actividades_frutales':actividades_frutales,
        'actividades_madera':  actividades_maderables,
        'actividades_arboles': actividades_arboles,
        'visita_cosecha':      visita_cosecha,
        'visita_cosecha_finca':visita_cosecha_finca,
        'estimacion_cosecha':  estimacion_cosecha_cacao,
    }


