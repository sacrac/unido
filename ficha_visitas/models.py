from django.db import models
from django.contrib.auth.models import User

from ficha.models import Tecnico
from productores.models import Productor
from .contants_visita import ( CHOICES_TRIMESTRE, CHOICES_ESTADO_CACAO,
                                CHOICES_TIPO_MAZORCAS, CHOICE_LABORES_CACAO,
                                CHOICE_LABORES_MUSACEAS,CHOICE_LABORES_FRUTALES,
                                CHOICE_LABORES_MADERABLES, 
                                CHOICE_LABORES_ARBOLES_SERVICIOS, 
                                CHOICE_VISITA_MESES, CHOICE_PISO,
                                CHOICE_VISITA_MESES, CHOICE_VISITA_VENDIO,
                                CHOICES_TIPO_CLON,CHOICE_VISITA_ACTIVIDADES)
from ficha_cierre.constant import ( CHOICES_ANIO, CHOICES_TIPO_PARCELAS )
from catalogos.models import (Clones, CHOICES_SI_NO,
                              PlantasMusaceas, PlantasFrutales,
                              PlantasMaderables, ArbolesServicios,
                              ProductosParcela, ProductosInversion,
                              ProductosFinca )

from multiselectfield import MultiSelectField
# Create your models here.

class FichaVisitas(models.Model):
    
    productor = models.ForeignKey(Productor, verbose_name='Productor/a', on_delete=models.CASCADE)
    tecnico = models.ForeignKey(Tecnico, verbose_name='Técnico', on_delete=models.CASCADE)
    fecha = models.DateField('Fecha de recolección')
    trimestre = models.IntegerField(choices=CHOICES_TRIMESTRE)
    anio = models.IntegerField('Año', choices=CHOICES_ANIO)
    parcela = models.IntegerField('Tipo de parcela', choices=CHOICES_TIPO_PARCELAS)

    year = models.IntegerField(editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.year = self.fecha.year
        super(FichaVisitas, self).save(*args, **kwargs)

    def __str__(self):
        return self.productor.nombre

    class Meta:
        verbose_name = 'Ficha visita'
        verbose_name_plural = 'Ficha visita'




class InventarioParcela(models.Model):
    area = models.FloatField('Área en mz')
    plantas_hibridas = models.IntegerField('Número de plantas híbridas de cacao')
    platas_injertadas = models.IntegerField('Número de plantas injertadas de cacao')
    musaceas = models.IntegerField('Número Musáceas')
    frutales = models.IntegerField('Número Frutales')
    maderables = models.IntegerField('Número Maderables')
    arboles_servicios = models.IntegerField('Número de árboles de servicio')
    estado_cacao = models.IntegerField('Estado de cacao', choices=CHOICES_ESTADO_CACAO)
    edad = models.FloatField('Edad en años')
    porcentaje = models.FloatField('% sombra')

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u""

    class Meta:
        verbose_name='1. Inventario parcela'
        verbose_name_plural='1. Inventario parcela'

#Estacion punto 1

class EstadoActualPunto1Cacao(models.Model):
    #clon = models.ForeignKey(Clones, on_delete=models.CASCADE)
    tipo = models.IntegerField(choices=CHOICES_TIPO_CLON)
    altura = models.FloatField('Altura de planta en metros')
    largo = models.FloatField('Largo de madera productiva metros')
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    estructura = models.IntegerField('Buena estructura', choices=CHOICES_SI_NO)
    floracion = models.IntegerField('Floración', choices=CHOICES_SI_NO)
    fructificacion = models.IntegerField('Fructificación', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"cacao"

    class Meta:
        verbose_name='Punto 1. Estado actual de plantas Cacao'
        verbose_name_plural='Punto 1. Estado actual de plantas Cacao'


class PrevalenciaPlagaPunto1(models.Model):
    tipo = models.IntegerField(choices=CHOICES_TIPO_CLON)
    monilia = models.IntegerField(choices=CHOICES_SI_NO)
    mazorca_negra = models.IntegerField(choices=CHOICES_SI_NO)
    alternaria = models.IntegerField(choices=CHOICES_SI_NO)
    xyleborus = models.IntegerField(choices=CHOICES_SI_NO)
    trips = models.IntegerField(choices=CHOICES_SI_NO)
    chinche = models.IntegerField(choices=CHOICES_SI_NO)
    gallina_ciega = models.IntegerField(choices=CHOICES_SI_NO)
    zompopos = models.IntegerField(choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"plaga"

    class Meta:
        verbose_name='Punto 1. Prevalencia de plagas y enfermedades'
        verbose_name_plural='Punto 1. Prevalencia de plagas y enfermedades'


class EstimadoCosechaCacaoPunto1(models.Model):
    opcion = models.IntegerField(choices=CHOICES_TIPO_MAZORCAS)
    planta1 = models.IntegerField('Planta 1')
    planta2 = models.IntegerField('Planta 2')
    planta3 = models.IntegerField('Planta 3')
    planta4 = models.IntegerField('Planta 4')
    planta5 = models.IntegerField('Planta 5')
    planta6 = models.IntegerField('Planta 6')
    planta7 = models.IntegerField('Planta 7')
    planta8 = models.IntegerField('Planta 8')
    planta9 = models.IntegerField('Planta 9')
    planta10 = models.IntegerField('Planta 10')

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"estimado"

    class Meta:
        verbose_name='Punto 1. Estimado de cosecha cacao 3 meses'
        verbose_name_plural='Punto 1. Estimado de cosecha cacao 3 meses'


class EstadoActualPunto1Musaceas(models.Model):
    tipo = models.ForeignKey(PlantasMusaceas, on_delete=models.CASCADE)
    altura = models.FloatField('Altura en metros')
    plagas = models.IntegerField('Daño de plagas', choices=CHOICES_SI_NO)
    enfermedades = models.IntegerField('Daño de enfermedades', choices=CHOICES_SI_NO)
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    foliar = models.IntegerField('Buena área foliar y hijos', choices=CHOICES_SI_NO)
    floracion = models.IntegerField('Floración', choices=CHOICES_SI_NO)
    fructificacion = models.IntegerField('Fructificación', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"musaceas"

    class Meta:
        verbose_name='Punto 1. Estado actual de plantas de Musáceas'
        verbose_name_plural='Punto 1. Estado actual de plantas de Musáceas'


class EstadoActualPunto1Frutales(models.Model):
    especies = models.ForeignKey(PlantasFrutales, on_delete=models.CASCADE)
    altura = models.FloatField('Altura en metros')
    plagas = models.IntegerField('Daño de plagas', choices=CHOICES_SI_NO)
    enfermedades = models.IntegerField('Daño de enfermedades', choices=CHOICES_SI_NO)
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    crecimiento = models.IntegerField('Buen crecimiento', choices=CHOICES_SI_NO)
    floracion = models.IntegerField('Floración', choices=CHOICES_SI_NO)
    fructificacion = models.IntegerField('Fructificación', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"frutales"

    class Meta:
        verbose_name='Punto 1. Estado actual de plantas frutales'
        verbose_name_plural='Punto 1. Estado actual de plantas frutales'


class EstadoActualPunto1Maderables(models.Model):
    especies = models.ForeignKey(PlantasMaderables, on_delete=models.CASCADE)
    altura = models.FloatField('Altura en metros')
    plagas = models.IntegerField('Daño de plagas', choices=CHOICES_SI_NO)
    enfermedades = models.IntegerField('Daño de enfermedades', choices=CHOICES_SI_NO)
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    crecimiento = models.IntegerField('Buen fuste y crecimiento', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"maderable"

    class Meta:
        verbose_name='Punto 1. Estado actual de plantas maderables'
        verbose_name_plural='Punto 1. Estado actual de plantas maderables'


class EstadoActualPunto1Arboles(models.Model):
    especies = models.ForeignKey(ArbolesServicios, on_delete=models.CASCADE)
    altura = models.FloatField('Altura en metros')
    plagas = models.IntegerField('Daño de plagas', choices=CHOICES_SI_NO)
    enfermedades = models.IntegerField('Daño de enfermedades', choices=CHOICES_SI_NO)
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    crecimiento = models.IntegerField('Buen crecimiento', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"arboles"

    class Meta:
        verbose_name='Punto 1. Estado actual árboles servicios'
        verbose_name_plural='Punto 1. Estado áctual arboles servicios'


#Estacion o Punto 2 

class EstadoActualPunto2Cacao(models.Model):
    #clon = models.ForeignKey(Clones, on_delete=models.CASCADE)
    tipo = models.IntegerField(choices=CHOICES_TIPO_CLON)
    altura = models.FloatField('Altura de planta en metros')
    largo = models.FloatField('Largo de madera productiva metros')
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    estructura = models.IntegerField('Buena estructura', choices=CHOICES_SI_NO)
    floracion = models.IntegerField('Floración', choices=CHOICES_SI_NO)
    fructificacion = models.IntegerField('Fructificación', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"cacao"

    class Meta:
        verbose_name='Punto 2. Estado actual de plantas Cacao'
        verbose_name_plural='Punto 2. Estado actual de plantas Cacao'


class PrevalenciaPlagaPunto2(models.Model):
    tipo = models.IntegerField(choices=CHOICES_TIPO_CLON)
    monilia = models.IntegerField(choices=CHOICES_SI_NO)
    mazorca_negra = models.IntegerField(choices=CHOICES_SI_NO)
    alternaria = models.IntegerField(choices=CHOICES_SI_NO)
    xyleborus = models.IntegerField(choices=CHOICES_SI_NO)
    trips = models.IntegerField(choices=CHOICES_SI_NO)
    chinche = models.IntegerField(choices=CHOICES_SI_NO)
    gallina_ciega = models.IntegerField(choices=CHOICES_SI_NO)
    zompopos = models.IntegerField(choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"plaga"

    class Meta:
        verbose_name='Punto 2. Prevalencia de plagas y enfermedades'
        verbose_name_plural='Punto 2. Prevalencia de plagas y enfermedades'


class EstimadoCosechaCacaoPunto2(models.Model):
    opcion = models.IntegerField(choices=CHOICES_TIPO_MAZORCAS)
    planta1 = models.IntegerField('Planta 1')
    planta2 = models.IntegerField('Planta 2')
    planta3 = models.IntegerField('Planta 3')
    planta4 = models.IntegerField('Planta 4')
    planta5 = models.IntegerField('Planta 5')
    planta6 = models.IntegerField('Planta 6')
    planta7 = models.IntegerField('Planta 7')
    planta8 = models.IntegerField('Planta 8')
    planta9 = models.IntegerField('Planta 9')
    planta10 = models.IntegerField('Planta 10')

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"estimado"

    class Meta:
        verbose_name='Punto 2. Estimado de cosecha cacao 3 meses'
        verbose_name_plural='Punto 2. Estimado de cosecha cacao 3 meses'


class EstadoActualPunto2Musaceas(models.Model):
    tipo = models.ForeignKey(PlantasMusaceas, on_delete=models.CASCADE)
    altura = models.FloatField('Altura en metros')
    plagas = models.IntegerField('Daño de plagas', choices=CHOICES_SI_NO)
    enfermedades = models.IntegerField('Daño de enfermedades', choices=CHOICES_SI_NO)
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    foliar = models.IntegerField('Buena área foliar y hijos', choices=CHOICES_SI_NO)
    floracion = models.IntegerField('Floración', choices=CHOICES_SI_NO)
    fructificacion = models.IntegerField('Fructificación', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"musaceas"

    class Meta:
        verbose_name='Punto 2. Estado actual de plantas de Musáceas'
        verbose_name_plural='Punto 2. Estado actual de plantas de Musáceas'


class EstadoActualPunto2Frutales(models.Model):
    especies = models.ForeignKey(PlantasFrutales, on_delete=models.CASCADE)
    altura = models.FloatField('Altura en metros')
    plagas = models.IntegerField('Daño de plagas', choices=CHOICES_SI_NO)
    enfermedades = models.IntegerField('Daño de enfermedades', choices=CHOICES_SI_NO)
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    crecimiento = models.IntegerField('Buen crecimiento', choices=CHOICES_SI_NO)
    floracion = models.IntegerField('Floración', choices=CHOICES_SI_NO)
    fructificacion = models.IntegerField('Fructificación', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"frutales"

    class Meta:
        verbose_name='Punto 2. Estado actual de plantas frutales'
        verbose_name_plural='Punto 2. Estado actual de plantas frutales'


class EstadoActualPunto2Maderables(models.Model):
    especies = models.ForeignKey(PlantasMaderables, on_delete=models.CASCADE)
    altura = models.FloatField('Altura en metros')
    plagas = models.IntegerField('Daño de plagas', choices=CHOICES_SI_NO)
    enfermedades = models.IntegerField('Daño de enfermedades', choices=CHOICES_SI_NO)
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    crecimiento = models.IntegerField('Buen fuste y crecimiento', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"maderable"

    class Meta:
        verbose_name='Punto 2. Estado actual de plantas maderables'
        verbose_name_plural='Punto 2. Estado actual de plantas maderables'


class EstadoActualPunto2Arboles(models.Model):
    especies = models.ForeignKey(ArbolesServicios, on_delete=models.CASCADE)
    altura = models.FloatField('Altura en metros')
    plagas = models.IntegerField('Daño de plagas', choices=CHOICES_SI_NO)
    enfermedades = models.IntegerField('Daño de enfermedades', choices=CHOICES_SI_NO)
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    crecimiento = models.IntegerField('Buen crecimiento', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"arboles"

    class Meta:
        verbose_name='Punto 2. Estado actual árboles servicios'
        verbose_name_plural='Punto 2. Estado áctual arboles servicios'


#Estacion punto 3

class EstadoActualPunto3Cacao(models.Model):
    #clon = models.ForeignKey(Clones, on_delete=models.CASCADE)
    tipo = models.IntegerField(choices=CHOICES_TIPO_CLON)
    altura = models.FloatField('Altura de planta en metros')
    largo = models.FloatField('Largo de madera productiva metros')
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    estructura = models.IntegerField('Buena estructura', choices=CHOICES_SI_NO)
    floracion = models.IntegerField('Floración', choices=CHOICES_SI_NO)
    fructificacion = models.IntegerField('Fructificación', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"cacao"

    class Meta:
        verbose_name='Punto 3. Estado actual de plantas Cacao'
        verbose_name_plural='Punto 3. Estado actual de plantas Cacao'


class PrevalenciaPlagaPunto3(models.Model):
    tipo = models.IntegerField(choices=CHOICES_TIPO_CLON)
    monilia = models.IntegerField(choices=CHOICES_SI_NO)
    mazorca_negra = models.IntegerField(choices=CHOICES_SI_NO)
    alternaria = models.IntegerField(choices=CHOICES_SI_NO)
    xyleborus = models.IntegerField(choices=CHOICES_SI_NO)
    trips = models.IntegerField(choices=CHOICES_SI_NO)
    chinche = models.IntegerField(choices=CHOICES_SI_NO)
    gallina_ciega = models.IntegerField(choices=CHOICES_SI_NO)
    zompopos = models.IntegerField(choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"plaga"

    class Meta:
        verbose_name='Punto 3. Prevalencia de plagas y enfermedades'
        verbose_name_plural='Punto 3. Prevalencia de plagas y enfermedades'


class EstimadoCosechaCacaoPunto3(models.Model):
    opcion = models.IntegerField(choices=CHOICES_TIPO_MAZORCAS)
    planta1 = models.IntegerField('Planta 1')
    planta2 = models.IntegerField('Planta 2')
    planta3 = models.IntegerField('Planta 3')
    planta4 = models.IntegerField('Planta 4')
    planta5 = models.IntegerField('Planta 5')
    planta6 = models.IntegerField('Planta 6')
    planta7 = models.IntegerField('Planta 7')
    planta8 = models.IntegerField('Planta 8')
    planta9 = models.IntegerField('Planta 9')
    planta10 = models.IntegerField('Planta 10')

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"estimado"

    class Meta:
        verbose_name='Punto 3. Estimado de cosecha cacao 3 meses'
        verbose_name_plural='Punto 3. Estimado de cosecha cacao 3 meses'


class EstadoActualPunto3Musaceas(models.Model):
    tipo = models.ForeignKey(PlantasMusaceas, on_delete=models.CASCADE)
    altura = models.FloatField('Altura en metros')
    plagas = models.IntegerField('Daño de plagas', choices=CHOICES_SI_NO)
    enfermedades = models.IntegerField('Daño de enfermedades', choices=CHOICES_SI_NO)
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    foliar = models.IntegerField('Buena área foliar y hijos', choices=CHOICES_SI_NO)
    floracion = models.IntegerField('Floración', choices=CHOICES_SI_NO)
    fructificacion = models.IntegerField('Fructificación', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"musaceas"

    class Meta:
        verbose_name='Punto 3. Estado actual de plantas de Musáceas'
        verbose_name_plural='Punto 3. Estado actual de plantas de Musáceas'


class EstadoActualPunto3Frutales(models.Model):
    especies = models.ForeignKey(PlantasFrutales, on_delete=models.CASCADE)
    altura = models.FloatField('Altura en metros')
    plagas = models.IntegerField('Daño de plagas', choices=CHOICES_SI_NO)
    enfermedades = models.IntegerField('Daño de enfermedades', choices=CHOICES_SI_NO)
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    crecimiento = models.IntegerField('Buen crecimiento', choices=CHOICES_SI_NO)
    floracion = models.IntegerField('Floración', choices=CHOICES_SI_NO)
    fructificacion = models.IntegerField('Fructificación', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"frutales"

    class Meta:
        verbose_name='Punto 3. Estado actual de plantas frutales'
        verbose_name_plural='Punto 3. Estado actual de plantas frutales'


class EstadoActualPunto3Maderables(models.Model):
    especies = models.ForeignKey(PlantasMaderables, on_delete=models.CASCADE)
    altura = models.FloatField('Altura en metros')
    plagas = models.IntegerField('Daño de plagas', choices=CHOICES_SI_NO)
    enfermedades = models.IntegerField('Daño de enfermedades', choices=CHOICES_SI_NO)
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    crecimiento = models.IntegerField('Buen fuste y crecimiento', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"maderables"

    class Meta:
        verbose_name='Punto 3. Estado actual de plantas maderables'
        verbose_name_plural='Punto 3. Estado actual de plantas maderables'


class EstadoActualPunto3Arboles(models.Model):
    especies = models.ForeignKey(ArbolesServicios, on_delete=models.CASCADE)
    altura = models.FloatField('Altura en metros')
    plagas = models.IntegerField('Daño de plagas', choices=CHOICES_SI_NO)
    enfermedades = models.IntegerField('Daño de enfermedades', choices=CHOICES_SI_NO)
    deficiencia = models.IntegerField('Deficiencia nutricional', choices=CHOICES_SI_NO)
    crecimiento = models.IntegerField('Buen crecimiento', choices=CHOICES_SI_NO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"arboles"

    class Meta:
        verbose_name='Punto 3. Estado actual árboles servicios'
        verbose_name_plural='Punto 3. Estado áctual arboles servicios'


# 3 datos de piso
class DatosPiso(models.Model):
    estado = models.IntegerField(choices=CHOICE_PISO,
            verbose_name="Estado de Piso")
    #raya = models.IntegerField('Rayas o Palotes')
    conteo = models.FloatField('Conteo (números)')
    porcentaje = models.FloatField('% de cobertura')

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"piso"

    class Meta:
        verbose_name_plural = '3. Cuadro para apuntar los datos de piso'

# 4 activides durante ultimo trimestre y calidad de labores

class ActividadesCacao(models.Model):
    labores = models.IntegerField(choices=CHOICE_LABORES_CACAO)
    opcion = models.IntegerField(choices=CHOICE_VISITA_ACTIVIDADES)
    planificado = models.IntegerField(choices=CHOICES_SI_NO,
            verbose_name='Planificada para próximo trimestre',
            null=True)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"cacao"

    class Meta:
        verbose_name_plural = '4. Actividad cacao'


class ActividadesMusaceas(models.Model):
    labores = models.IntegerField(choices=CHOICE_LABORES_MUSACEAS)
    opcion = models.IntegerField(choices=CHOICE_VISITA_ACTIVIDADES)
    planificado = models.IntegerField(choices=CHOICES_SI_NO,
            verbose_name='Planificada para próximo trimestre',
            null=True)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"musaceas"

    class Meta:
        verbose_name_plural = '4. Actividad musáceas'



class ActividadesFrutales(models.Model):
    labores = models.IntegerField(choices=CHOICE_LABORES_FRUTALES)
    opcion = models.IntegerField(choices=CHOICE_VISITA_ACTIVIDADES)
    planificado = models.IntegerField(choices=CHOICES_SI_NO,
            verbose_name='Planificada para próximo trimestre',
            null=True)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"frutales"

    class Meta:
        verbose_name_plural = '4. Actividad frutales'

class ActividadesMaderables(models.Model):
    labores = models.IntegerField(choices=CHOICE_LABORES_MADERABLES)
    opcion = models.IntegerField(choices=CHOICE_VISITA_ACTIVIDADES)
    planificado = models.IntegerField(choices=CHOICES_SI_NO,
            verbose_name='Planificada para próximo trimestre',
            null=True)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"maderables"

    class Meta:
        verbose_name_plural = '4. Actividad maderables'


class ActividadesArboles(models.Model):
    labores = models.IntegerField(choices=CHOICE_LABORES_ARBOLES_SERVICIOS)
    opcion = models.IntegerField(choices=CHOICE_VISITA_ACTIVIDADES)
    planificado = models.IntegerField(choices=CHOICES_SI_NO,
            verbose_name='Planificada para próximo trimestre',
            null=True)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    def __str__(self):
        return u"arboles"

    class Meta:
        verbose_name_plural = '4. Árboles de servicios'


# 5 Cosecha realizadas parcela SAF

class CosechaParcelaTrimestre(models.Model):
    mes = MultiSelectField(choices=CHOICE_VISITA_MESES)
    productos = models.ForeignKey(ProductosParcela, on_delete=models.CASCADE)
    cantidad = models.FloatField('Cantidad')
    vendio = MultiSelectField('A quien vendió',
                            choices=CHOICE_VISITA_VENDIO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = '5. Cosecha de la parcela SAF'

# 6 Cosecha realiazadas finca completa

class CosechaFincaTrimestre(models.Model):
    mes = MultiSelectField(choices=CHOICE_VISITA_MESES)
    productos = models.ForeignKey(ProductosFinca, on_delete=models.CASCADE)
    cantidad = models.FloatField('Cantidad')
    vendio = MultiSelectField('A quien vendió',
                            choices=CHOICE_VISITA_VENDIO)

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)
    


    class Meta:
        verbose_name_plural = '6. Cosecha de la Finca'


# 7 Inversiones realizadas en la finca durante el último trimestre por parte de proyecto

class InversionesFinca(models.Model):
    mes = MultiSelectField(choices=CHOICE_VISITA_MESES)
    productos = models.ForeignKey(ProductosInversion, on_delete=models.CASCADE)
    cantidad = models.FloatField('Cantidad')
    mano_obra = models.FloatField('Mano de obra proyecto')
    contrapartida = models.FloatField('Contrapartida de familia mano de obra Días')
    contrapartida_familiar = models.FloatField('Contrapartida de familia efectivo C$')

    ficha = models.ForeignKey(FichaVisitas, on_delete=models.CASCADE)


    class Meta:
        verbose_name_plural = '7. Inversiones realizada en la Finca'




