from django.apps import AppConfig


class FichaCierreConfig(AppConfig):
    name = 'ficha_cierre'
    verbose_name = "Ficha de cierre de año"
