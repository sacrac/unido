
CHOICES_ANIO = (

		(1, '2020-21'),
		(2, '2021-22'),
		(3, '2022-23'),
		(4, '2023-24'),
		(5, '2024-25'),
        (6, '2025-26'),
        (7, '2026-27'),
        (8, '2027-28'),
        (9, '2028-29'),
        (10, '2029-30'),
        (11, '2030-31'),

	)

CHOICES_TIPO_PARCELAS = (

		(1, 'Parcela hibrida a mejorar'),
		(2, 'Parcela policlonal a mejorar'),
		(3, 'Parcela policlonal nueva'),
		
	)

CHOICES_ESTADO_PARCELAS = (

		(1, '100% desarrollo'),
		(2, '75% desarrollo - 25% producción'),
		(3, '50% desarrollo - 50% producción'),
		(4, '25% desarrollo - 75% producción'),
		(5, '100% producción'),
		
	)

CHOICE_CIERRE_1_1_IMPACTO = (
		('A', 'Tipo de árboles y cantidad'),
		('B', 'Mucha sombra de los árboles'),
		('C', 'Poca sombra de los árboles'),
		('D', 'Efecto de sombra sobre las plagas y enfermedades'),
		('E', 'Efecto de sombra sobre la producción'),
		('F', 'Ninguna'),
	)

CHOICE_CIERRE_1_1_PLANIFICADA = (
        ('A', 'Regulación de sombra'),
        ('B', 'Eliminación de árboles'),
        ('C', 'Sembrar árboles'),
        ('D', 'Eliminar musaceas'),
        ('E', 'Sembrar musaceas y sombra temporal'),
        ('F', 'Ninguna'),
        ('G', 'Ninguna por falta de recursos'),
    )

CHOICE_CIERRE_1_1_REALIZADA = (
        ('A', 'Regulación de sombra'),
        ('B', 'Eliminación de árboles'),
        ('C', 'Sembrar árboles'),
        ('D', 'Eliminar musaceas'),
        ('E', 'Sembrar musaceas y sombra temporal'),
        ('F', 'Ninguna'),
        ('G', 'Ninguna por falta de recursos'),
    )


CHOICE_CIERRE_1_1_RESULTADOS = (
	    ('A', 'Aumento de producción'),
	    ('B', 'Mejor control de malas hierbas'),
	    ('C', 'Reducción de enfermedades'),
	    ('D', 'Eliminar musaceas'),
	    ('E', 'Ninguna'),
	)

CHOICE_CIERRE_1_2_IMPACTO = (
        ('A', 'Altura y ancho de plantas de cacao'),
        ('B', 'Falta de horquetas'),
        ('C', 'Muchas ramas bajeras y entrecruzadas'),
        ('D', 'Poca penetración de luz'),
        ('E', 'Relación entre poda y productividad'),
        ('F', 'Ninguna'),
    )

CHOICE_CIERRE_1_2_PLANIFICADA = (
        ('A', 'Descope de las plantas'),
        ('B', 'Poda de las ramas  entrecruzadas'),
        ('C', 'Eliminar los chupones'),
        ('D', 'Formar horquetas'),
        ('E', 'Eliminar ramas bajeras'),
        ('F', 'Ninguna'),
        ('G', 'Ninguna por falta de recursos'),
    )

CHOICE_CIERRE_1_2_REALIZADA = (
        ('A', 'Descope de las plantas'),
        ('B', 'Poda de las ramas  entrecruzadas'),
        ('C', 'Eliminar los chupones'),
        ('D', 'Formar horquetas'),
        ('E', 'Eliminar ramas bajeras'),
        ('F', 'Ninguna'),
        ('G', 'Ninguna por falta de recursos'),
    )

CHOICE_CIERRE_1_2_RESULTADOS = (
        ('A', 'Aumento de producción'),
        ('B', 'Mejor entrada de luz'),
        ('C', 'Reducción de enfermedades'),
        ('D', 'Ninguna'),
    )


CHOICE_CIERRE_1_3_IMPACTO = (
	    ('A', 'Falta de obra de conservación'),
	    ('B', 'Falta de obra de drenaje'),
	    ('C', 'Deficiencia o desbalance de nutrientes'),
	    ('D', 'Estado de fertilidad de suelo'),
	    ('E', 'Relación entre suelo, fertilidad y la productividad'),
	    ('F', 'Ninguna'),
	)

CHOICE_CIERRE_1_3_PLANIFICADA = (
        ('A', 'Aplicar abono orgánicos'),
        ('B', 'Aplicar abono mineral'),
        ('C', 'Aplicar Cal o Ceniza'),
        ('D', 'Abonar según datos de análisis'),
        ('E', 'Sembrar abono verde y cobertura'),
        ('F', 'Ninguna'),
        ('G', 'Ninguna por falta de recursos'),
    )

CHOICE_CIERRE_1_3_REALIZADA = (
        ('A', 'Aplicar abono orgánicos'),
        ('B', 'Aplicar abono mineral'),
        ('C', 'Aplicar Cal o Ceniza'),
        ('D', 'Abonar según datos de análisis'),
        ('E', 'Sembrar abono verde y cobertura'),
        ('F', 'Ninguna'),
        ('G', 'Ninguna por falta de recursos'),
    )

CHOICE_CIERRE_1_3_RESULTADOS = (
        ('A', 'Aumento de producción'),
        ('B', 'Aumento de la floración'),
        ('C', 'Reducción de enfermedades'),
        ('D', 'Abonar según datos de análisis'),
        ('E', 'Ninguna'),
    )

CHOICE_CIERRE_1_4_IMPACTO = (
        ('A', 'Variedad de plagas y enfermedades'),
        ('B', 'Nivel de daño de plagas y enfermedades'),
        ('C', 'Relación entre poda , plagas y enfermedades'),
        ('D', 'Relación entre sombra y plagas y enfermedades'),
        ('E', 'Impacto de plagas y enfermedades sobre producción'),
        ('F', 'Ninguna'),
    )

CHOICE_CIERRE_1_4_PLANIFICADA = (
        ('A', 'Realizar recuentos'),
        ('B', 'Mejorar la sombra'),
        ('C', 'Mejorar la poda'),
        ('D', 'Eliminar mazorcas enfermas'),
        ('E', 'Aplicar caldo sulfo-calcico'),
        ('F', 'Aplicar bio-fermentados'),
        ('G', 'Ninguna'),
        ('H', 'Ninguna por falta de recursos'),
    )

CHOICE_CIERRE_1_4_REALIZADA = (
        ('A', 'Realizar recuentos'),
        ('B', 'Mejorar la sombra'),
        ('C', 'Mejorar la poda'),
        ('D', 'Eliminar mazorcas enfermas'),
        ('E', 'Aplicar caldo sulfo-calcico'),
        ('F', 'Aplicar bio-fermentados'),
        ('G', 'Ninguna'),
        ('H', 'Ninguna por falta de recursos'),
    )

CHOICE_CIERRE_1_4_RESULTADOS = (
        ('A', 'Aumento de producción'),
        ('B', 'Reducción de daño de plagas'),
        ('C', 'Reducción de enfermedades'),
        ('D', 'Ninguna'),
    )

CHOICE_CIERRE_1_5_IMPACTO = (
        ('A', 'Variedad de mala hierbas'),
        ('B', 'Nivel de daño de mala hierbas'),
        ('C', 'Relación entre chapoda y composición del piso'),
        ('D', 'Relación entre herbicidas y composición del piso'),
        ('E', 'Cantidad de bejucos en el piso y plantas'),
        ('F', 'Ninguna'),
        ('G', 'Falta de materia organica'),
    )

CHOICE_CIERRE_1_5_PLANIFICADA = (
        ('A', 'Realizar conteo'),
        ('B', 'Mejorar la sombra'),
        ('C', 'Eliminar bejucos'),
        ('D', 'Eliminar tanda'),
        ('E', 'Realizar manejo selectivo'),
        ('F', 'Ninguna'),
        ('G', 'Ninguna por falta de recursos'),
        ('H', 'Repartir hojarasca'),
    )

CHOICE_CIERRE_1_5_REALIZADA = (
	    ('A', 'Realizar conteo'),
	    ('B', 'Mejorar la sombra'),
	    ('C', 'Eliminar bejucos'),
	    ('D', 'Eliminar tanda'),
	    ('E', 'Realizar manejo selectivo'),
	    ('F', 'Ninguna'),
	    ('G', 'Ninguna por falta de recursos'),
	    ('H', 'Repartir hojarasca'),
	)

CHOICE_CIERRE_1_5_RESULTADOS = (
        ('A', 'Aumento de producción'),
        ('B', 'Reducción de malas hierbas dañinas'),
        ('C', 'Aumento de cobertura'),
        ('D', 'Eliminar tanda'),
        ('E', 'Ninguna'),
    )

CHOICE_CIERRE_1_6_IMPACTO = (
        ('A', 'Tipo de cacao que estamos sembrando'),
        ('B', 'Auto-incompatibilidad de las semillas'),
        ('C', 'La calidad de semillas'),
        ('D', 'Incidencia de plagas y enfermedades en vivero'),
        ('E', 'Calidad de plantas'),
        ('F', 'Ninguna'),
    )

CHOICE_CIERRE_1_6_PLANIFICADA = (
        ('A', 'Seleccionar mazorcas y mezclar para conseguir semilla'),
        ('B', 'Utilizar mejor calidad de semillas'),
        ('C', 'Mejorar el sustrato'),
        ('D', 'Mejorar el tamaño de bolsa'),
        ('E', 'Mejorar manejo de enfermedades y plagas'),
        ('F', 'Ninguna'),
        ('G', 'Ninguna por falta de recursos'),
    )

CHOICE_CIERRE_1_6_REALIZADA = (
        ('A', 'Seleccionar mazorcas y mezclar para conseguir semilla'),
        ('B', 'Utilizar mejor calidad de semillas'),
        ('C', 'Mejorar el sustrato'),
        ('D', 'Mejorar el tamaño de bolsa'),
        ('E', 'Mejorar manejo de enfermedades y plagas'),
        ('F', 'Ninguna'),
        ('G', 'Ninguna por falta de recursos'),
    )

CHOICE_CIERRE_1_6_RESULTADOS = (
        ('A', 'Mejor vigor de las plantas'),
        ('B', 'Menos daño de plagas'),
        ('C', 'Menos daño de enfermedades'),
        ('D', 'Ninguna'),
    )

CHOICE_CIERRE_1_7_IMPACTO = (
	    ('A', 'Cantidad de planta productiva'),
	    ('B', 'Numero de mazorcas sanas'),
	    ('C', 'Numero de mazorcas dañadas'),
	    ('D', 'Nivel de cosecha de la parcela'),
	    ('E', 'Ninguna'),
	    ('F', 'Efecto de sombra sobre la producción'),
	    ('G', 'Efecto de poda sobre la producción'),
	)

CHOICE_CIERRE_1_7_PLANIFICADA = (
        ('A', 'Mejorar la poda y sombra'),
        ('B', 'Mejorar la fertilización'),
        ('C', 'Mejorar manejo de plagas'),
        ('D', 'Eliminar planta poca productivas'),
        ('E', 'Sembrar plantas más productivas'),
        ('F', 'Ninguna'),
        ('G', 'Ninguna por falta de recursos'),
    )

CHOICE_CIERRE_1_7_REALIZADA = (
        ('A', 'Mejorar la poda y sombra'),
        ('B', 'Mejorar la fertilización'),
        ('C', 'Mejorar manejo de plagas'),
        ('D', 'Eliminar planta poca productivas'),
        ('E', 'Sembrar plantas más productivas'),
        ('F', 'Ninguna'),
        ('G', 'Ninguna por falta de recursos'),
    )

CHOICE_CIERRE_1_7_RESULTADO = (
        ('A', 'Aumento de la cosecha'),
        ('B', 'Aumento de plantas productivas'),
        ('C', 'Mejor calidad de mazorcas'),
        ('D', 'Mejor calidad de granos'),
        ('E', 'Ninguna'),
    )

CHOICE_COSTO_CACAO_ACTIVIDAD = (
	    (1, 'Siembra/resiembra'),
	    (2, 'Injertación'),
	    (3, 'Poda de cacao'),
	    (4, 'Deschuponado'),
	    (5, 'Aplicación de fertilizante'),
	    (6, 'Aplicación de abono orgánico'),
	    (7, 'Aplicación de insecticida'),
	    (8, 'Aplicación de fungicida'),
	    (9, 'Aplicación de herbicida'),
	    (10, 'Manejo de piso'),
	    (11, 'Aplicación de riego'),
	    (12, 'Obras de drenaje'),
	    (13, 'Obras de conservación'),
	    (14, 'Cosecha y Corte'),
	    (15, 'Transporte de cosecha'),
    )

CHOICE_MESES = (
	    ('A', 'Enero'),
	    ('B', 'Febrero'),
	    ('C', 'Marzo'),
	    ('D', 'Abril'),
	    ('E', 'Mayo'),
	    ('F', 'Junio'),
	    ('G', 'Julio'),
	    ('H', 'Agosto'),
	    ('I', 'Septiembre'),
	    ('J', 'Octubre'),
	    ('K', 'Noviembre'),
	    ('L', 'Diciembre'),
	)

CHOICE_COSTO_MUSACEAS_ACTIVIDAD = (
	    (1, 'Siembra/resiembra'),
	    (2, 'Deshije y deshoja'),
	    (3, 'Manejo de cabezas'),
	    (4, 'Aplicación de fertilizante'),
	    (5, 'Aplicación de abono orgánico'),
	    (6, 'Aplicación de insecticida'),
	    (7, 'Aplicación de fungicida'),
	    (8, 'Cosecha y Corte'),
	    (9, 'Transporte de cosecha'),
    )

CHOICE_COSTO_FRUTALES_ACTIVIDAD = (
	    (1, 'Siembra/resiembra'),
	    (2, 'Poda de frutales'),
	    (3, 'Aplicación de fertilizante'),
	    (4, 'Aplicación de abono orgánico'),
	    (5, 'Aplicación de insecticida'),
	    (6, 'Aplicación de fungicida'),
	    (7, 'Cosecha y Corte'),
	    (8, 'Transporte de cosecha'),
    )

CHOICE_COSTO_MADERABLES_ACTIVIDAD = (
	    (1, 'Siembra/resiembra'),
	    (2, 'Poda de maderables'),
	    (3, 'Aplicación de fertilizante'),
	    (4, 'Aplicación de abono orgánico'),
	    (5, 'Aplicación de insecticida'),
	    (6, 'Aplicación de fungicida'),
	    (7, 'Cosecha y Corte'),
	    (8, 'Transporte de cosecha'),
    )

CHOICE_COSTO_ARBOLES_SERVICIO_ACTIVIDAD = (
    (1, 'Siembra/resiembra'),
    (2, 'Poda de árboles'),
    (3, 'Cosecha y Corte'),
    (4, 'Transporte de cosecha'),
    )

CHOICE_PRODUCTOS = (
	    (1, 'Cacao en baba'),
	    (2, 'Cacao rojo'),
	    (3, 'Musaceas-Banano'),
	    (4, 'Musaceas-Plátano'),
	    (5, 'Frutales-Zapote'),
	    (6, 'Frutales-Cítricos'),
	    (7, 'Frutales-Aguacate'),
	    (8, 'Frutales-Coco'),
	    (9, 'Madera-Caoba'),
	    (10, 'Madera-Cortez'),
	    (11, 'Madera-Roble'),
	    (12, 'Madera-Melina'),
	    (13, 'Madera-Granadillo'),
	    (14, 'Madera-Leña'),
	    (15, 'Granos básicos-Maíz'),
	    (16, 'Granos básicos-Frijol'),
	    (17, 'Gandul'),
	    (18, 'Guaba'),
	    (19, 'Café Uva'),
	    (20, 'Café Pergamino'),
	    (21, 'Café Tostado'),
    )

CHOICE_PRODUCTOS_FINCA = (
	    (1, 'Cacao en baba'),
	    (2, 'Cacao rojo'),
	    (3, 'Musaceas-Banano'),
	    (4, 'Musaceas-Plátano'),
	    (5, 'Musaceas-Guineo'),
	    (6, 'Frutales-Zapote'),
	    (7, 'Frutales-Cítricos'),
	    (8, 'Frutales-Aguacate'),
	    (9, 'Frutales-Coco'),
	    (10, 'Frutales-Pejibaye'),
	    (11, 'Madera-Caoba'),
	    (12, 'Madera-Cortez'),
	    (13, 'Madera-Roble'),
	    (14, 'Madera-Melina'),
	    (15, 'Madera-Granadillo'),
	    (16, 'Madera-Leña'),
	    (17, 'Granos básicos-Maíz'),
	    (18, 'Granos básicos-Frijol'),
	    (19, 'Gandul'),
	    (20, 'Guaba'),
	    (21, 'Yuca'),
	    (22, 'Tomate'),
	    (23, 'Cebolla'),
	    (24, 'Chiltoma'),
	    (25, 'Leche'),
	    (26, 'Ganado'),
	    (27, 'Huevo'),
	    (28, 'Gallina'),

    )



CHOICE_COMPRADOR_DATOS = (
	    ('A', 'Intermediario'),
	    ('B', 'Cooperativa'),
	    ('C', 'Asociación'),
	    ('D', 'Empresa'),
	    ('E', 'Autoconsumo'),
    )

CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA = (
	    (1, 'Mucho'),
	    (2, 'Algo'),
	    (3, 'Poco'),
	    (4, 'Nada '),
    )

CHOICE_CIERRE_CICLO_TRABAJO2_RESPUESTA = (
	    (1, 'Todas'),
	    (2, 'Algunas'),
	    (3, 'Pocas'),
	    (4, 'Ninguna'),
    )

CHOICE_CIERRE_CICLO_TRABAJO3_RESPUESTA = (
	    (1, 'Demasiada visitas'),
	    (2, 'Adecuadas visitas'),
	    (3, 'Pocas visitas'),
    )

CHOICE_CIERRE_CICLO_TRABAJO4_RESPUESTA = (
	    (1, 'Demasiada larga'),
	    (2, 'Adecuado tiempo '),
	    (3, 'Muy corta'),
    )

CHOICE_CIERRE_CICLO_TRABAJO5_RESPUESTA = (
	    (1, 'Si y con mucho ánimo'),
	    (2, 'Si, pero con poco ánimo'),
	    (3, 'Si, porque siento obligado'),
	    (4, 'No quiero seguir'),
    )

