# -*- coding: utf-8 -*-
from django.shortcuts import render, render_to_response
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.db.models import Avg, Sum, Count,  Value as V
from django.db.models.functions import Coalesce
from django.contrib import messages
from django.core.exceptions import ViewDoesNotExist

import json as simplejson
from collections import OrderedDict
import numpy as np

from .forms import ConsultaFormCierre
from .models import *
from productores.models import (Productor, CHOICES_ESCOLARIDAD,
                                CHOICES_LEGALIDAD, UserExtend)
from catalogos.models import *
from lugar.models import Departamento, Municipio, Comunidad
from cooperativas.models import Cooperativa

# Create your views here.

def _queryset_filtrado_cierre(request):
    params = {}

    if 'fecha' in request.session:
        params['anio'] = request.session['fecha']

    if 'productor' in request.session:
        params['productor__nombre'] = request.session['productor']

    if 'departamento' in request.session:
        params['productor__departamento'] = request.session['departamento']

    if 'municipio' in request.session:
        params['productor__municipio'] = request.session['municipio']

    if 'comunidad' in request.session:
        params['productor__comunidad'] = request.session['comunidad']

    if 'sexo' in request.session:
        params['productor__sexo'] = request.session['sexo']

    if 'cooperativa' in request.session:
        params['productor__cooperativa'] = request.session['cooperativa']

    if 'tecnico' in request.session:
        params['tecnico'] = request.session['tecnico']

    if 'parcela' in request.session:
        params['parcela'] = request.session['parcela']

    unvalid_keys = []
    for key in params:
        if not params[key]:
            unvalid_keys.append(key)

    for key in unvalid_keys:
        del params[key]

    return FichaCierre.objects.filter(**params)


@login_required
def consulta_cierre(request, template='cierre/consulta_cierre.html'):
    conteo_encuesta = 0
    if request.method == 'POST':
        form = ConsultaFormCierre(request.POST)
        if form.is_valid():
            request.session['fecha'] = form.cleaned_data['fecha']
            request.session['productor'] = form.cleaned_data['productor']
            request.session['departamento'] = form.cleaned_data['departamento']
            request.session['municipio'] = form.cleaned_data['municipio']
            request.session['comunidad'] = form.cleaned_data['comunidad']
            request.session['sexo'] = form.cleaned_data['sexo']
            request.session['cooperativa'] = form.cleaned_data['cooperativa']
            request.session['tecnico'] = form.cleaned_data['tecnico']
            request.session['parcela'] = form.cleaned_data['parcela']
            centinela = 1
            filtro = _queryset_filtrado_cierre(request)
            conteo_encuesta = filtro.count()

        else:
            centinela = 0
    else:
        form = ConsultaFormCierre()
        centinela = 0

        if 'fecha' in request.session:
            try:
                del request.session['fecha']
                del request.session['productor']
                del request.session['departamento']
                del request.session['municipio']
                del request.session['comunidad']
                del request.session['sexo']
                del request.session['cooperativa']
                del request.session['tecnico']
                del request.session['parcela']
            except:
                pass

    return render(request, template, {'form': form, 'centinela': centinela,
                                    'conteo_encuesta':conteo_encuesta})



def analisis_sombra(request, template="cierre/analisis_sombra.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro =filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    numero_parcelas = filtro.count()

    impacto_sombra = OrderedDict()
    for obj in CHOICE_CIERRE_1_1_IMPACTO:
        conteo = filtro.filter(cierremanejo1__campo1__contains=obj[0]).count()
        impacto_sombra[obj[1]] = conteo

    planificada_sombra = OrderedDict()
    for obj in CHOICE_CIERRE_1_1_PLANIFICADA:
        conteo = filtro.filter(cierremanejo1__campo2__contains=obj[0]).count()
        planificada_sombra[obj[1]] = conteo

    realizada_sombra = OrderedDict()
    for obj in CHOICE_CIERRE_1_1_REALIZADA:
        conteo = filtro.filter(cierremanejo1__campo3__contains=obj[0]).count()
        realizada_sombra[obj[1]] = conteo

    resultado_sombra = OrderedDict()
    for obj in CHOICE_CIERRE_1_1_RESULTADOS:
        conteo = filtro.filter(cierremanejo1__campo4__contains=obj[0]).count()
        resultado_sombra[obj[1]] = conteo

    return render(request, template, locals())


def poda_cierre(request, template="cierre/poda_cierre.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro =filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    numero_parcelas = filtro.count()

    impacto_poda = OrderedDict()
    for obj in CHOICE_CIERRE_1_2_IMPACTO:
        conteo = filtro.filter(cierremanejo2__campo1__contains=obj[0]).count()
        impacto_poda[obj[1]] = conteo

    planificada_poda = OrderedDict()
    for obj in CHOICE_CIERRE_1_2_PLANIFICADA:
        conteo = filtro.filter(cierremanejo2__campo2__contains=obj[0]).count()
        planificada_poda[obj[1]] = conteo

    realizada_poda = OrderedDict()
    for obj in CHOICE_CIERRE_1_2_REALIZADA:
        conteo = filtro.filter(cierremanejo2__campo3__contains=obj[0]).count()
        realizada_poda[obj[1]] = conteo

    resultado_poda = OrderedDict()
    for obj in CHOICE_CIERRE_1_2_RESULTADOS:
        conteo = filtro.filter(cierremanejo2__campo4__contains=obj[0]).count()
        resultado_poda[obj[1]] = conteo 

    return render(request, template, locals())

def suelo_cierre(request, template="cierre/suelo_cierre.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro =filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    numero_parcelas = filtro.count()

    impacto_suelo = OrderedDict()
    for obj in CHOICE_CIERRE_1_3_IMPACTO:
        conteo = filtro.filter(cierremanejo3__campo1__contains=obj[0]).count()
        impacto_suelo[obj[1]] = conteo

    planificada_suelo = OrderedDict()
    for obj in CHOICE_CIERRE_1_3_PLANIFICADA:
        conteo = filtro.filter(cierremanejo3__campo2__contains=obj[0]).count()
        planificada_suelo[obj[1]] = conteo

    realizada_suelo = OrderedDict()
    for obj in CHOICE_CIERRE_1_3_REALIZADA:
        conteo = filtro.filter(cierremanejo3__campo3__contains=obj[0]).count()
        realizada_suelo[obj[1]] = conteo

    resultado_suelo = OrderedDict()
    for obj in CHOICE_CIERRE_1_3_RESULTADOS:
        conteo = filtro.filter(cierremanejo3__campo4__contains=obj[0]).count()
        resultado_suelo[obj[1]] = conteo

    return render(request, template, locals())

def plaga_cierre(request, template="cierre/plaga_cierre.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro = filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    numero_parcelas = filtro.count()

    impacto_plaga = OrderedDict()
    for obj in CHOICE_CIERRE_1_4_IMPACTO:
        conteo = filtro.filter(cierremanejo4__campo1__contains=obj[0]).count()
        impacto_plaga[obj[1]] = conteo

    planificada_plaga = OrderedDict()
    for obj in CHOICE_CIERRE_1_4_PLANIFICADA:
        conteo = filtro.filter(cierremanejo4__campo2__contains=obj[0]).count()
        planificada_plaga[obj[1]] = conteo

    realizada_plaga = OrderedDict()
    for obj in CHOICE_CIERRE_1_4_REALIZADA:
        conteo = filtro.filter(cierremanejo4__campo3__contains=obj[0]).count()
        realizada_plaga[obj[1]] = conteo

    resultado_plaga = OrderedDict()
    for obj in CHOICE_CIERRE_1_4_RESULTADOS:
        conteo = filtro.filter(cierremanejo4__campo4__contains=obj[0]).count()
        resultado_plaga[obj[1]] = conteo

    return render(request, template, locals())

def piso_cierre(request, template="cierre/piso_cierre.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro = filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    numero_parcelas = filtro.count()

    impacto_piso = OrderedDict()
    for obj in CHOICE_CIERRE_1_5_IMPACTO:
        conteo = filtro.filter(cierremanejo5__campo1__contains=obj[0]).count()
        impacto_piso[obj[1]] = conteo

    planificada_piso = OrderedDict()
    for obj in CHOICE_CIERRE_1_5_PLANIFICADA:
        conteo = filtro.filter(cierremanejo5__campo2__contains=obj[0]).count()
        planificada_piso[obj[1]] = conteo

    realizada_piso = OrderedDict()
    for obj in CHOICE_CIERRE_1_5_REALIZADA:
        conteo = filtro.filter(cierremanejo5__campo3__contains=obj[0]).count()
        realizada_piso[obj[1]] = conteo

    resultado_piso = OrderedDict()
    for obj in CHOICE_CIERRE_1_5_RESULTADOS:
        conteo = filtro.filter(cierremanejo5__campo4__contains=obj[0]).count()
        resultado_piso[obj[1]] = conteo

    return render(request, template, locals())

def vivero_cierre(request, template="cierre/vivero_cierre.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro = filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    numero_parcelas = filtro.count()

    impacto_vivero = OrderedDict()
    for obj in CHOICE_CIERRE_1_6_IMPACTO:
        conteo = filtro.filter(cierremanejo6__campo1__contains=obj[0]).count()
        impacto_vivero[obj[1]] = conteo

    planificada_vivero = OrderedDict()
    for obj in CHOICE_CIERRE_1_6_PLANIFICADA:
        conteo = filtro.filter(cierremanejo6__campo2__contains=obj[0]).count()
        planificada_vivero[obj[1]] = conteo

    realizada_vivero = OrderedDict()
    for obj in CHOICE_CIERRE_1_6_REALIZADA:
        conteo = filtro.filter(cierremanejo6__campo3__contains=obj[0]).count()
        realizada_vivero[obj[1]] = conteo

    resultado_vivero = OrderedDict()
    for obj in CHOICE_CIERRE_1_6_RESULTADOS:
        conteo = filtro.filter(cierremanejo6__campo4__contains=obj[0]).count()
        resultado_vivero[obj[1]] = conteo

    return render(request, template, locals())


def cosecha_cierre(request, template="cierre/cosecha_cierre.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro = filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    numero_parcelas = filtro.count()

    impacto_cosecha = OrderedDict()
    for obj in CHOICE_CIERRE_1_7_IMPACTO:
        conteo = filtro.filter(cierremanejo7__campo1__contains=obj[0]).count()
        impacto_cosecha[obj[1]] = conteo

    planificada_cosecha = OrderedDict()
    for obj in CHOICE_CIERRE_1_7_PLANIFICADA:
        conteo = filtro.filter(cierremanejo7__campo2__contains=obj[0]).count()
        planificada_cosecha[obj[1]] = conteo

    realizada_cosecha = OrderedDict()
    for obj in CHOICE_CIERRE_1_7_REALIZADA:
        conteo = filtro.filter(cierremanejo7__campo3__contains=obj[0]).count()
        realizada_cosecha[obj[1]] = conteo

    resultado_cosecha = OrderedDict()
    for obj in CHOICE_CIERRE_1_7_RESULTADO:
        conteo = filtro.filter(cierremanejo7__campo4__contains=obj[0]).count()
        resultado_cosecha[obj[1]] = conteo

    return render(request, template, locals())

def trabajo_cierre(request, template="cierre/trabajo_cierre.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro = filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    numero_parcelas = filtro.count()

    pie_1 = OrderedDict()
    for obj in CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA:
        conteo = filtro.filter(ciclotrabajocierre__pregunta1=obj[0]).count()
        pie_1[obj[1]] = conteo

    pie_2 = OrderedDict()
    for obj in CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA:
        conteo = filtro.filter(ciclotrabajocierre__pregunta2=obj[0]).count()
        pie_2[obj[1]] = conteo

    pie_3 = OrderedDict()
    for obj in CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA:
        conteo = filtro.filter(ciclotrabajocierre__pregunta3=obj[0]).count()
        pie_3[obj[1]] = conteo

    pie_4 = OrderedDict()
    for obj in CHOICE_CIERRE_CICLO_TRABAJO2_RESPUESTA:
        conteo = filtro.filter(ciclotrabajocierre__pregunta4=obj[0]).count()
        pie_4[obj[1]] = conteo

    pie_5 = OrderedDict()
    for obj in CHOICE_CIERRE_CICLO_TRABAJO3_RESPUESTA:
        conteo = filtro.filter(ciclotrabajocierre__pregunta5=obj[0]).count()
        pie_5[obj[1]] = conteo

    pie_6 = OrderedDict()
    for obj in CHOICE_CIERRE_CICLO_TRABAJO4_RESPUESTA:
        conteo = filtro.filter(ciclotrabajocierre__pregunta6=obj[0]).count()
        pie_6[obj[1]] = conteo

    pie_7 = OrderedDict()
    for obj in CHOICE_CIERRE_CICLO_TRABAJO5_RESPUESTA:
        conteo = filtro.filter(ciclotrabajocierre__pregunta7=obj[0]).count()
        pie_7[obj[1]] = conteo

    pie_8 = OrderedDict()
    for obj in CHOICES_SI_NO:
        conteo = filtro.filter(ciclotrabajocierre__pregunta8=obj[0]).count()
        pie_8[obj[1]] = conteo

    return render(request, template, locals())

def cierre_costo_cacao(request, template="cierre/costo_cacao.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro = filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    numero_parcelas = filtro.count()

    area_total = filtro.aggregate(t=Sum('cierrecosto1__area'))['t']
    area_total_desarollo = filtro.aggregate(t=Sum('cierrecosto1__area_desarrollo'))['t']
    area_total_produccion = filtro.aggregate(t=Sum('cierrecosto1__area_produccion'))['t']

    lista_promedio1=filtro.filter(cierrecosto1__costo__gt=0).values_list('cierrecosto1__costo', flat=True)

    lista_total_promedio = list(lista_promedio1)

    promedio_mo_dia = np.mean(lista_total_promedio)

    tabla_costo_cacao = OrderedDict()
    for obj in CHOICE_COSTO_CACAO_ACTIVIDAD:
        tmo_familia1 = filtro.filter(costocacaocierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costocacaocierre__mo_familiar'), V(0)))['t']
        tmo_contratada1 = filtro.filter(costocacaocierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costocacaocierre__mo_contratada'), V(0)))['t']
        tmo_temporal = filtro.filter(costocacaocierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costocacaocierre__mo_temporal'), V(0)))['t']
        insumos1 = filtro.filter(costocacaocierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costocacaocierre__costo_insumo'), V(0)))['t']
        tabla_costo_cacao[obj[1]] = (tmo_familia1, tmo_contratada1, insumos1, tmo_temporal)


    #totales de costo
    total_costo_mo_familiar = 0
    total_costo_mo_contratada = 0
    total_costo_insumos = 0
    total_costo_mo_temporal = 0
    for k,v in tabla_costo_cacao.items():
        total_costo_mo_familiar += v[0]
        total_costo_mo_contratada += v[1]
        total_costo_insumos +=v[2]
        total_costo_mo_temporal +=v[3]

    #Cosecha consolidada Cacao
    CHOICE_PRODUCTOS_CACAO = (
                        (1, 'Cacao en baba'),
                        (2, 'Cacao en grano rojo'),
    )
    tabla_cosecha_cacao = OrderedDict()
    for obj in CHOICE_PRODUCTOS_CACAO:
        cantidad1 = filtro.filter(datoscosechaparcelacierre__productos=obj[0]).aggregate(t=Coalesce(Sum('datoscosechaparcelacierre__produccion'), V(0)))['t']
        precio1 = filtro.filter(datoscosechaparcelacierre__productos=obj[0]).aggregate(t=Coalesce(Avg('datoscosechaparcelacierre__precio'), V(0)))['t']

        total_cosecha = cantidad1 * precio1
        tabla_cosecha_cacao[obj[1]] = (cantidad1, precio1, total_cosecha)

    suma_baba = 0
    suma_seco = 0
    suma_coseha = 0
    for k,v in tabla_cosecha_cacao.items():
        if k == 'Cacao en baba':
            suma_baba += v[0] / 3.0
        if k == 'Cacao en grano rojo':
            suma_seco += v[0]
        suma_coseha += v[2]

    #Terce cuadro
    try:
        valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
    except:
        valor_mo_familiar = 0
    try:
        valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
    except:
        valor_mo_contratada = 0
    try:
        valor_mo_temporal = total_costo_mo_temporal * promedio_mo_dia
    except:
        valor_mo_temporal = 0
    try:
        valor_insumos = total_costo_insumos
    except:
        valor_insumos = 0
    try:
        costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_mo_temporal + valor_insumos
    except:
        costo_total_produccion = 0
    try:
        prod_cacao_seco = suma_baba + suma_seco
    except:
        prod_cacao_seco = 0
    try:
        productividad_cacao = prod_cacao_seco / float(area_total_produccion)
    except:
        productividad_cacao = 0
    try:
        costo_prod_unitario = costo_total_produccion / float(prod_cacao_seco)
    except:
        costo_prod_unitario = 0
    try:
        ingreso_neto = suma_coseha - costo_total_produccion
    except:
        ingreso_neto = 0
    try:
        ingreso_neto_ha = ingreso_neto / area_total
    except:
        ingreso_neto_ha = 0
    try:
        tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
    except:
        tasa_retorno = 0
    try:
        ingreso_neto_parcial = suma_coseha - total_costo_insumos
    except:
        ingreso_neto_parcial = 0
    try:
        valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
    except:
        valor_agregado_mo_familiar = 0

    return render(request, template, locals())

def cierre_costo_musaceas(request, template="cierre/costo_musaceas.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro = filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    numero_parcelas = filtro.count()

    area_total = filtro.aggregate(t=Sum('cierrecosto1__area'))['t']
    lista_promedio1=filtro.filter(cierrecosto1__costo__gt=0).values_list('cierrecosto1__costo', flat=True)
    lista_total_promedio = list(lista_promedio1)
    promedio_mo_dia = np.mean(lista_total_promedio)

    tabla_costo_musaceas = OrderedDict()
    for obj in CHOICE_COSTO_MUSACEAS_ACTIVIDAD:
        tmo_familia1 = filtro.filter(costomusaceascierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costomusaceascierre__mo_familiar'), V(0)))['t']
        tmo_contratada1 = filtro.filter(costomusaceascierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costomusaceascierre__mo_contratada'), V(0)))['t']
        tmo_temporal = filtro.filter(costomusaceascierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costomusaceascierre__mo_temporal'), V(0)))['t']
        insumos1 = filtro.filter(costomusaceascierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costomusaceascierre__costo_insumo'), V(0)))['t']

        tabla_costo_musaceas[obj[1]] = (tmo_familia1, tmo_contratada1, insumos1, tmo_temporal)


    #totales de costo
    total_costo_mo_familiar = 0
    total_costo_mo_contratada = 0
    total_costo_insumos = 0
    total_costo_mo_temporal = 0
    for k,v in tabla_costo_musaceas.items():
        total_costo_mo_familiar += v[0]
        total_costo_mo_contratada += v[1]
        total_costo_insumos +=v[2]
        total_costo_mo_temporal +=v[3]

    #Cosecha consolidada Cacao
    CHOICE_PRODUCTOS_MUSACEAS = (
                        (3, 'Musaceas-Banano'),
                        (4, 'Musaceas-Plátano'),
    )
    tabla_cosecha_musaceas = OrderedDict()
    for obj in CHOICE_PRODUCTOS_MUSACEAS:
        cantidad1_kg = filtro.filter(datoscosechaparcelacierre__productos=obj[0]).aggregate(t=Coalesce(Sum('datoscosechaparcelacierre__produccion'), V(0)))['t']
        precio1_kg = filtro.filter(datoscosechaparcelacierre__productos=obj[0]).aggregate(t=Coalesce(Avg('datoscosechaparcelacierre__precio'), V(0)))['t']

        total_cosecha = cantidad1_kg * precio1_kg

        tabla_cosecha_musaceas[obj[1]] = (cantidad1_kg, precio1_kg, total_cosecha)

    suma_banano = 0
    suma_platano = 0
    suma_coseha = 0
    for k,v in tabla_cosecha_musaceas.items():
        if k == 'Musaceas-Banano':
            suma_banano += v[0]
        if k == 'Musaceas-Plátano':
            suma_platano += v[0]
        suma_coseha += v[2]

    #tercer tabla
    try:
        valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
    except:
        valor_mo_familiar = 0
    try:
        valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
    except:
        valor_mo_contratada = 0
    try:
        valor_mo_temporal = total_costo_mo_temporal * promedio_mo_dia
    except:
        valor_mo_temporal = 0
    try:
        valor_insumos = total_costo_insumos
    except:
        valor_insumos = 0
    try:
        costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
    except:
        costo_total_produccion = 0
    try:
        prod_musaceas = suma_banano + suma_platano
    except:
        prod_musaceas = 0
    try:
        productividad_musaceas = prod_musaceas / float(area_total)
    except:
        productividad_musaceas = 0
    try:
        costo_prod_unitario = costo_total_produccion / float(prod_musaceas)
    except:
        costo_prod_unitario = 0
    try:
        ingreso_neto = suma_coseha - costo_total_produccion
    except:
        ingreso_neto = 0
    try:
        ingreso_neto_ha = ingreso_neto / area_total
    except:
        ingreso_neto_ha = 0
    try:
        tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
    except:
        tasa_retorno = 0
    try:
        ingreso_neto_parcial = suma_coseha - total_costo_insumos
    except:
        ingreso_neto_parcial = 0
    try:
        valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
    except:
        valor_agregado_mo_familiar = 0

    return render(request, template, locals())

def cierre_costo_frutales(request, template="cierre/costo_frutales.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro = filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    numero_parcelas = filtro.count()

    area_total = filtro.aggregate(t=Sum('cierrecosto1__area'))['t']
    lista_promedio1=filtro.filter(cierrecosto1__costo__gt=0).values_list('cierrecosto1__costo', flat=True)
    lista_total_promedio = list(lista_promedio1)
    promedio_mo_dia = np.mean(lista_total_promedio)

    tabla_costo_frutales = OrderedDict()
    for obj in CHOICE_COSTO_FRUTALES_ACTIVIDAD:
        tmo_familia1 = filtro.filter(costofrutalescierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costofrutalescierre__mo_familiar'), V(0)))['t']
        tmo_contratada1 = filtro.filter(costofrutalescierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costofrutalescierre__mo_contratada'), V(0)))['t']
        tmo_temporal = filtro.filter(costofrutalescierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costofrutalescierre__mo_temporal'), V(0)))['t']
        insumos1 = filtro.filter(costofrutalescierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costofrutalescierre__costo_insumo'), V(0)))['t']

        tabla_costo_frutales[obj[1]] = (tmo_familia1, tmo_contratada1, insumos1, tmo_temporal)


    #totales de costo
    total_costo_mo_familiar = 0
    total_costo_mo_contratada = 0
    total_costo_insumos = 0
    total_costo_mo_temporal = 0
    for k,v in tabla_costo_frutales.items():
        total_costo_mo_familiar += v[0]
        total_costo_mo_contratada += v[1]
        total_costo_insumos +=v[2]
        total_costo_mo_temporal +=v[3]

    #Cosecha consolidada Cacao
    CHOICE_PRODUCTOS_FRUTALES = (
                        (5, 'Frutales-Zapote'),
                        (6, 'Frutales-Cítricos'),
                        (7, 'Frutales-Aguacate'),
                        (8, 'Frutales-Coco'),
    )
    tabla_cosecha_frutales = OrderedDict()
    for obj in CHOICE_PRODUCTOS_FRUTALES:
        cantidad1 = filtro.filter(datoscosechaparcelacierre__productos=obj[0]).aggregate(t=Coalesce(Sum('datoscosechaparcelacierre__produccion'), V(0)))['t']
        precio1 = filtro.filter(datoscosechaparcelacierre__productos=obj[0]).aggregate(t=Coalesce(Avg('datoscosechaparcelacierre__precio'), V(0)))['t']

        total_cosecha = cantidad1 * precio1
        tabla_cosecha_frutales[obj[1]] = (cantidad1, precio1, total_cosecha)

    suma_zapote = 0
    suma_citricos = 0
    suma_aguacate = 0
    suma_coco = 0
    suma_coseha = 0
    for k,v in tabla_cosecha_frutales.items():
        if k == 'Frutales-Zapote':
            suma_zapote += v[0]
        if k == 'Frutales-Cítricos':
            suma_citricos += v[0]
        if k == 'Frutales-Aguacate':
            suma_aguacate += v[0]
        if k == 'Frutales-Coco':
            suma_coco += v[0]
        suma_coseha += v[2]

    total_sumas = suma_zapote + suma_citricos +suma_aguacate + suma_coco

    #tercer tabla
    try:
        valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
    except:
        valor_mo_familiar = 0
    try:
        valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
    except:
        valor_mo_contratada = 0
    try:
        valor_mo_temporal = total_costo_mo_temporal * promedio_mo_dia
    except:
        valor_mo_temporal = 0
    try:
        valor_insumos = total_costo_insumos
    except:
        valor_insumos = 0
    try:
        costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
    except:
        costo_total_produccion = 0
    try:
        prod_frutales = total_sumas
    except:
        prod_frutales = 0
    try:
        productividad_frutales = prod_frutales / float(area_total)
    except:
        productividad_frutales = 0
    try:
        costo_prod_unitario = costo_total_produccion / float(prod_frutales)
    except:
        costo_prod_unitario = 0
    try:
        ingreso_neto = suma_coseha - costo_total_produccion
    except:
        ingreso_neto = 0
    try:
        ingreso_neto_ha = ingreso_neto / area_total
    except:
        ingreso_neto_ha = 0
    try:
        tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
    except:
        tasa_retorno = 0
    try:
        ingreso_neto_parcial = suma_coseha - total_costo_insumos
    except:
        ingreso_neto_parcial = 0
    try:
        valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
    except:
        valor_agregado_mo_familiar = 0

    return render(request, template, locals())


def cierre_costo_maderables(request, template="cierre/costo_maderables.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro = filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    numero_parcelas = filtro.count()

    area_total = filtro.aggregate(t=Sum('cierrecosto1__area'))['t']
    lista_promedio1=filtro.filter(cierrecosto1__costo__gt=0).values_list('cierrecosto1__costo', flat=True)
    lista_total_promedio = list(lista_promedio1)
    promedio_mo_dia = np.mean(lista_total_promedio)

    tabla_costo_maderables = OrderedDict()
    for obj in CHOICE_COSTO_MADERABLES_ACTIVIDAD:
        tmo_familia1 = filtro.filter(costomaderablescierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costomaderablescierre__mo_familiar'), V(0)))['t']
        tmo_contratada1 = filtro.filter(costomaderablescierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costomaderablescierre__mo_contratada'), V(0)))['t']
        tmo_temporal = filtro.filter(costomaderablescierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costomaderablescierre__mo_temporal'), V(0)))['t']
        insumos1 = filtro.filter(costomaderablescierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costomaderablescierre__costo_insumo'), V(0)))['t']

        tabla_costo_maderables[obj[1]] = (tmo_familia1, tmo_contratada1, insumos1, tmo_temporal)

    #totales de costo
    total_costo_mo_familiar = 0
    total_costo_mo_contratada = 0
    total_costo_insumos = 0
    total_costo_mo_temporal = 0
    for k,v in tabla_costo_maderables.items():
        total_costo_mo_familiar += v[0]
        total_costo_mo_contratada += v[1]
        total_costo_insumos +=v[2]
        total_costo_mo_temporal +=v[3]

    #Cosecha consolidada Cacao
    CHOICE_PRODUCTOS_MADERABLES = (
                        (9, 'Madera-Caoba'),
                        (10, 'Madera-Cortez'),
                        (11, 'Madera-Roble'),
                        (12, 'Madera-Melina'),
                        (13, 'Madera-Granadillo'),
                        (14, 'Madera-Leña'),
    )
    tabla_cosecha_maderables = OrderedDict()
    for obj in CHOICE_PRODUCTOS_MADERABLES:
        cantidad1 = filtro.filter(datoscosechaparcelacierre__productos=obj[0]).aggregate(t=Coalesce(Sum('datoscosechaparcelacierre__produccion'), V(0)))['t']
        precio1 = filtro.filter(datoscosechaparcelacierre__productos=obj[0]).aggregate(t=Coalesce(Avg('datoscosechaparcelacierre__precio'), V(0)))['t']

        total_cosecha = cantidad1 * precio1
        tabla_cosecha_maderables[obj[1]] = (cantidad1, precio1, total_cosecha)

    suma_caoba = 0
    suma_cortez = 0
    suma_roble = 0
    suma_melina = 0
    suma_granadillo = 0
    suma_lena = 0
    suma_coseha = 0
    for k,v in tabla_cosecha_maderables.items():
        if k == 'Madera-Caoba':
            suma_caoba += v[0]
        if k == 'Madera-Cortez':
            suma_cortez += v[0]
        if k == 'Madera-Roble':
            suma_roble += v[0]
        if k == 'Madera-Melina':
            suma_melina += v[0]
        if k == 'Madera-Granadillo':
            suma_granadillo += v[0]
        if k == 'Madera-Leña':
            suma_lena += v[0]
        suma_coseha += v[2]

    total_sumas = suma_caoba + suma_cortez +suma_roble + suma_melina + suma_granadillo
    total_lena = suma_lena
    #tercer tabla
    try:
        valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
    except:
        valor_mo_familiar = 0
    try:
        valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
    except:
        valor_mo_contratada = 0
    try:
        valor_mo_temporal = total_costo_mo_temporal * promedio_mo_dia
    except:
        valor_mo_temporal = 0
    try:
        valor_insumos = total_costo_insumos
    except:
        valor_insumos = 0
    try:
        costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
    except:
        costo_total_produccion = 0
    try:
        prod_maderables = total_sumas
    except:
        prod_maderables = 0
    try:
        prod_lena = total_lena
    except:
        prod_lena = 0
    try:
        productividad_maderables = prod_maderables / float(area_total)
    except:
        productividad_maderables = 0
    try:
        productividad_lena = prod_lena / float(area_total)
    except:
        productividad_lena = 0
    try:
        costo_prod_unitario = costo_total_produccion / float(prod_maderables)
    except:
        costo_prod_unitario = 0
    try:
        costo_prod_unitario_lena = costo_total_produccion / float(prod_lena)
    except:
        costo_prod_unitario_lena = 0
    try:
        ingreso_neto = suma_coseha - costo_total_produccion
    except:
        ingreso_neto = 0
    try:
        ingreso_neto_ha = ingreso_neto / area_total
    except:
        ingreso_neto_ha = 0
    try:
        tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
    except:
        tasa_retorno = 0
    try:
        ingreso_neto_parcial = suma_coseha - total_costo_insumos
    except:
        ingreso_neto_parcial = 0
    try:
        valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
    except:
        valor_agregado_mo_familiar = 0

    return render(request, template, locals())

def cierre_costo_arboles(request, template="cierre/costo_arboles.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro = filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()
    numero_parcelas = filtro.count()

    area_total = filtro.aggregate(t=Sum('cierrecosto1__area'))['t']
    lista_promedio1=filtro.filter(cierrecosto1__costo__gt=0).values_list('cierrecosto1__costo', flat=True)
    lista_total_promedio = list(lista_promedio1)
    promedio_mo_dia = np.mean(lista_total_promedio)

    tabla_costo_arboles = OrderedDict()
    for obj in CHOICE_COSTO_ARBOLES_SERVICIO_ACTIVIDAD:
        tmo_familia1 = filtro.filter(costoarbolescierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costoarbolescierre__mo_familiar'), V(0)))['t']
        tmo_contratada1 = filtro.filter(costoarbolescierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costoarbolescierre__mo_contratada'), V(0)))['t']
        tmo_temporal = filtro.filter(costoarbolescierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costoarbolescierre__mo_temporal'), V(0)))['t']
        insumos1 = filtro.filter(costoarbolescierre__actividades=obj[0]).aggregate(t=Coalesce(Sum('costoarbolescierre__costo_insumo'), V(0)))['t']

        tabla_costo_arboles[obj[1]] = (tmo_familia1, tmo_contratada1, insumos1, tmo_temporal)


    #totales de costo
    total_costo_mo_familiar = 0
    total_costo_mo_contratada = 0
    total_costo_insumos = 0
    total_costo_mo_temporal = 0
    for k,v in tabla_costo_arboles.items():
        total_costo_mo_familiar += v[0]
        total_costo_mo_contratada += v[1]
        total_costo_insumos +=v[2]
        total_costo_mo_temporal +=v[3]

    #Cosecha consolidada Cacao
    CHOICE_PRODUCTOS_ARBOLES = (
                        (17, 'Gandul'),
                        (18, 'Guaba'),
    )
    tabla_cosecha_arboles = OrderedDict()
    for obj in CHOICE_PRODUCTOS_ARBOLES:
        cantidad1 = filtro.filter(datoscosechaparcelacierre__productos=obj[0]).aggregate(t=Coalesce(Sum('datoscosechaparcelacierre__produccion'), V(0)))['t']
        precio1 = filtro.filter(datoscosechaparcelacierre__productos=obj[0]).aggregate(t=Coalesce(Avg('datoscosechaparcelacierre__precio'), V(0)))['t']

        total_cosecha = cantidad1 * precio1
        tabla_cosecha_arboles[obj[1]] = (cantidad1, precio1, total_cosecha)

    suma_gandul = 0
    suma_guaba = 0
    suma_coseha = 0
    for k,v in tabla_cosecha_arboles.items():
        if k == 'Gandul':
            suma_gandul += v[0]
        if k == 'Guaba':
            suma_guaba += v[0]
        suma_coseha += v[2]

    total_sumas = suma_gandul + suma_guaba

    #tercer tabla
    try:
        valor_mo_familiar = total_costo_mo_familiar * promedio_mo_dia
    except:
        valor_mo_familiar = 0
    try:
        valor_mo_contratada = total_costo_mo_contratada * promedio_mo_dia
    except:
        valor_mo_contratada = 0
    try:
        valor_mo_temporal = total_costo_mo_temporal * promedio_mo_dia
    except:
        valor_mo_temporal = 0
    try:
        valor_insumos = total_costo_insumos
    except:
        valor_insumos = 0
    try:
        costo_total_produccion = valor_mo_familiar + valor_mo_contratada + valor_insumos
    except:
        costo_total_produccion = 0
    try:
        prod_arboles = total_sumas
    except:
        prod_arboles = 0
    try:
        productividad_arboles = prod_arboles / float(area_total)
    except:
        productividad_arboles = 0
    try:
        costo_prod_unitario = costo_total_produccion / float(prod_arboles)
    except:
        costo_prod_unitario = 0
    try:
        ingreso_neto = suma_coseha - costo_total_produccion
    except:
        ingreso_neto = 0
    try:
        ingreso_neto_ha = ingreso_neto / area_total
    except:
        ingreso_neto_ha = 0
    try:
        tasa_retorno = (ingreso_neto/float(costo_total_produccion)) * 100
    except:
        tasa_retorno = 0
    try:
        ingreso_neto_parcial = suma_coseha - total_costo_insumos
    except:
        ingreso_neto_parcial = 0
    try:
        valor_agregado_mo_familiar = ingreso_neto_parcial / total_costo_mo_familiar
    except:
        valor_agregado_mo_familiar = 0

    return render(request, template, locals())

def cierre_costo_parcela(request, template="cierre/costo_parcela.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro = filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()

    numero_parcelas = filtro.count()
    area_total = filtro.aggregate(t=Sum('cierrecosto1__area'))['t']

    tabla_parcelas = OrderedDict()
    tabla_destino = OrderedDict()
    for obj in ProductosParcela.objects.all():
        conteo_productor = filtro.filter(datoscosechaparcelacierre__productos=obj).count()
        produccion = filtro.filter(datoscosechaparcelacierre__productos=obj).aggregate(t=Coalesce(Sum('datoscosechaparcelacierre__produccion'), V(0)))['t']
        venta = filtro.filter(datoscosechaparcelacierre__productos=obj).aggregate(t=Coalesce(Sum('datoscosechaparcelacierre__venta'), V(0)))['t']
        precio = filtro.filter(datoscosechaparcelacierre__productos=obj).aggregate(t=Coalesce(Avg('datoscosechaparcelacierre__precio'), V(0)))['t']
        ingreso_total = filtro.filter(datoscosechaparcelacierre__productos=obj).aggregate(t=Coalesce(Sum('datoscosechaparcelacierre__total_produccion'), V(0)))['t']
        ingreso_venta = filtro.filter(datoscosechaparcelacierre__productos=obj).aggregate(t=Coalesce(Sum('datoscosechaparcelacierre__total'), V(0)))['t']
        if produccion > 0:
            tabla_parcelas[obj.nombre] = (produccion,venta,obj.get_unidad_display(),precio,ingreso_total,ingreso_venta,conteo_productor) 
        conteo_intermediario = filtro.filter(datoscosechaparcelacierre__productos=obj, datoscosechaparcelacierre__destino__contains='A').count()
        conteo_coop = filtro.filter(datoscosechaparcelacierre__productos=obj, datoscosechaparcelacierre__destino__contains='B').count()
        conteo_asociacion = filtro.filter(datoscosechaparcelacierre__productos=obj, datoscosechaparcelacierre__destino__contains='C').count()
        conteo_empresa = filtro.filter(datoscosechaparcelacierre__productos=obj, datoscosechaparcelacierre__destino__contains='D').count()
        conteo_auto = filtro.filter(datoscosechaparcelacierre__productos=obj, datoscosechaparcelacierre__destino__contains='E').count()
        if conteo_productor > 0:
            tabla_destino[obj.nombre] = (conteo_productor,conteo_intermediario,conteo_coop,conteo_asociacion,conteo_empresa,conteo_auto)

    total_ingreso = 0
    total_venta = 0
    for k,v in tabla_parcelas.items():
        total_ingreso += v[4]
        total_venta += v[5]

    try:
        parcela_ingreso = total_ingreso / numero_parcelas
        parcela_venta = total_venta / numero_parcelas
    except:
        parcela_ingreso = 0
        parcela_venta = 0

    try:
        manzana_ingreso = total_ingreso / area_total
        manzana_venta = total_venta / area_total
    except:
        manzana_ingreso = 0
        manzana_venta = 0

    return render(request, template, locals())

def cierre_costo_finca(request, template="cierre/costo_finca.html"):
    filtro1 = _queryset_filtrado_cierre(request)

    try:
        coop = UserExtend.objects.get(user=request.user)
        filtro = filtro1.filter(productor__cooperativa__id=coop.cooperativa.id)
    except:
        filtro = filtro1

    num_familias = filtro.count()

    numero_parcelas = filtro.count()
    area_total = filtro.aggregate(t=Sum('cierrecosto1__area'))['t']

    tabla_fincas = OrderedDict()
    tabla_destino = OrderedDict()
    for obj in ProductosFinca.objects.all():
        conteo_productor = filtro.filter(datoscosechafincacierre__productos=obj).count()
        produccion = filtro.filter(datoscosechafincacierre__productos=obj).aggregate(t=Coalesce(Sum('datoscosechafincacierre__produccion'), V(0)))['t']
        venta = filtro.filter(datoscosechafincacierre__productos=obj).aggregate(t=Coalesce(Sum('datoscosechafincacierre__venta'), V(0)))['t']
        precio = filtro.filter(datoscosechafincacierre__productos=obj).aggregate(t=Coalesce(Avg('datoscosechafincacierre__precio'), V(0)))['t']
        ingreso_total = filtro.filter(datoscosechafincacierre__productos=obj).aggregate(t=Coalesce(Sum('datoscosechafincacierre__total_produccion'), V(0)))['t']
        ingreso_venta = filtro.filter(datoscosechafincacierre__productos=obj).aggregate(t=Coalesce(Sum('datoscosechafincacierre__total'), V(0)))['t']
        if produccion > 0:
            tabla_fincas[obj.nombre] = (produccion,venta,obj.get_unidad_display(),precio,ingreso_total,ingreso_venta,conteo_productor)

        conteo_intermediario = filtro.filter(datoscosechafincacierre__productos=obj, datoscosechafincacierre__destino__contains='A').count()
        conteo_coop = filtro.filter(datoscosechafincacierre__productos=obj, datoscosechafincacierre__destino__contains='B').count()
        conteo_asociacion = filtro.filter(datoscosechafincacierre__productos=obj, datoscosechafincacierre__destino__contains='C').count()
        conteo_empresa = filtro.filter(datoscosechafincacierre__productos=obj, datoscosechafincacierre__destino__contains='D').count()
        conteo_auto = filtro.filter(datoscosechafincacierre__productos=obj, datoscosechafincacierre__destino__contains='E').count()
        if conteo_productor > 0:
            tabla_destino[obj.nombre] = (conteo_productor,conteo_intermediario,conteo_coop,conteo_asociacion,conteo_empresa,conteo_auto)

    total_ingreso = 0
    total_venta = 0
    for k,v in tabla_fincas.items():
        total_ingreso += v[4]
        total_venta += v[5]

    try:
        parcela_ingreso = total_ingreso / numero_parcelas
        parcela_venta = total_venta / numero_parcelas
    except:
        parcela_ingreso = 0
        parcela_venta = 0

    try:
        manzana_ingreso = total_ingreso / area_total
        manzana_venta = total_venta / area_total
    except:
        manzana_ingreso = 0
        manzana_venta = 0

    return render(request, template, locals())


@login_required
def tabla_beneficiario_cierre(request, template='cierre/tabla_beneficiario.html'):
    filtro_principal = _queryset_filtrado_cierre(request).select_related('productor',
                                                   'productor__cooperativa',
                                                   'productor__comunidad',
                                                   'productor__municipio',
                                                   'tecnico').prefetch_related('cierrecosto1_set',
                                                   'datoscosechaparcelacierre_set',
                                                   'datoscosechaparcelacierre_set__productos')
    try:
        coop = UserExtend.objects.get(user=request.user)
    except:
        pass
    if request.user.is_superuser:
        filtro = filtro_principal
    else:
        try:
            filtro = filtro_principal.filter(productor__cooperativa__id=coop.cooperativa.id)
        except:
            filtro = []
    num_familias = filtro_principal.count()

    tabla = OrderedDict()
    for obj in filtro:
        area_t = 0
        area_d = 0
        area_p = 0
        suma_seco = 0
        suma_rojo = 0
        total_seco = 0
        p_qq = 0
        rendimiento = 0
        # suma_baba = obj.datoscosechaparcelacierre_set.filter(productos=1).aggregate(t=Coalesce(Sum('produccion'), V(0)))['t']
        # suma_seco = suma_baba / 3.0
        # suma_rojo = obj.datoscosechaparcelacierre_set.filter(productos=2).aggregate(t=Coalesce(Sum('produccion'), V(0)))['t']
        # total_seco = suma_seco + suma_rojo
        #print(total_seco)
        for datos in obj.datoscosechaparcelacierre_set.all():
            #print(datos.productos_id)
            if datos.productos_id == 1:
                suma_seco = datos.produccion / 3
            if datos.productos_id == 2:
                suma_rojo = datos.produccion
            total_seco = suma_seco + suma_rojo

        for x in obj.cierrecosto1_set.all():
            area_t = x.area
            area_d = x.area_desarrollo
            area_p = x.area_produccion
            try:
                rendimiento = total_seco  / area_t
            except:
                rendimiento = 0
        tabla[obj.productor.nombre] = [
                    obj.productor.cooperativa,
                    obj.productor.comunidad,
                    obj.productor.municipio,
                    obj.tecnico,
                    obj.get_estado_parcela_display(),
                    obj.edad,
                    area_t,
                    area_d,
                    area_p,
                    total_seco,
                    rendimiento,
            ]

    return render(request,template, locals())

def get_view_cierre(request, vista):
    if vista in VALID_VIEWS_CIERRE:
        return VALID_VIEWS_CIERRE[vista](request)
    else:
        raise ViewDoesNotExist("Tried %s in module %s Error: View not defined in VALID_VIEWS." % (vista, 'ficha_cierre.views'))


VALID_VIEWS_CIERRE = {
        'sombra':  analisis_sombra,
        'poda':    poda_cierre,
        'suelo':   suelo_cierre,
        'plaga':   plaga_cierre,
        'piso':    piso_cierre,
        'vivero':  vivero_cierre,
        'cosecha': cosecha_cierre,
        'trabajo': trabajo_cierre,
        'costo_cacao': cierre_costo_cacao,
        'costo_musaceas': cierre_costo_musaceas,
        'costo_frutales': cierre_costo_frutales,
        'costo_maderables': cierre_costo_maderables,
        'costo_arboles': cierre_costo_arboles,
        'costo_parcela': cierre_costo_parcela,
        'costo_finca': cierre_costo_finca,
        'beneficiario_cierre': tabla_beneficiario_cierre,

    }







