from django.contrib import admin
from .models import *

# Register your models here.
class CierreManejo1Inlines(admin.TabularInline):
    model = CierreManejo1
    extra = 1
    max_num = 1

class CierreManejo2Inlines(admin.TabularInline):
    model = CierreManejo2
    extra = 1
    max_num = 1

class CierreManejo3Inlines(admin.TabularInline):
    model = CierreManejo3
    extra = 1
    max_num = 1

class CierreManejo4Inlines(admin.TabularInline):
    model = CierreManejo4
    extra = 1
    max_num = 1

class CierreManejo5Inlines(admin.TabularInline):
    model = CierreManejo5
    extra = 1
    max_num = 1

class CierreManejo6Inlines(admin.TabularInline):
    model = CierreManejo6
    extra = 1
    max_num = 1

class CierreManejo7Inlines(admin.TabularInline):
    model = CierreManejo7
    extra = 1
    max_num = 1

class CierreCosto1Inlines(admin.TabularInline):
    model = CierreCosto1
    extra = 1
    max_num = 1

class CostoCacaoCierreInlines(admin.TabularInline):
    model = CostoCacaoCierre
    extra = 1

class CostoMusaceasCierreInlines(admin.TabularInline):
    model = CostoMusaceasCierre
    extra = 1

class CostoFrutalesCierreInlines(admin.TabularInline):
    model = CostoFrutalesCierre
    extra = 1

class CostoMaderablesCierreInlines(admin.TabularInline):
    model = CostoMaderablesCierre
    extra = 1

class CostoArbolesCierreInlines(admin.TabularInline):
    model = CostoArbolesCierre
    extra = 1

class DatosCosechaParcelaCierreInline(admin.TabularInline):
    model = DatosCosechaParcelaCierre
    autocomplete_fields = ('productos',)
    extra = 1

class DatosCosechaFincaCierreInlines(admin.TabularInline):
    model = DatosCosechaFincaCierre
    autocomplete_fields = ('productos',)
    extra = 1

class CicloTrabajoCierreInlines(admin.TabularInline):
    model = CicloTrabajoCierre
    extra = 1
    max_num = 1

class FichaCierreAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(FichaCierreAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(FichaCierreAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    exclude = ('user', )
    autocomplete_fields = ['productor','tecnico']
    list_display = ('productor','productor_cooperativa','anio','user')
    list_filter = ['productor__cooperativa','anio',]
    search_fields = ('productor__nombre',)
    inlines = [CierreManejo1Inlines,CierreManejo2Inlines,
             CierreManejo3Inlines,CierreManejo4Inlines,
             CierreManejo5Inlines,CierreManejo6Inlines,
             CierreManejo7Inlines,CierreCosto1Inlines,
             CostoCacaoCierreInlines,CostoMusaceasCierreInlines,
             CostoFrutalesCierreInlines,CostoMaderablesCierreInlines,
             CostoArbolesCierreInlines,DatosCosechaParcelaCierreInline,
             DatosCosechaFincaCierreInlines,CicloTrabajoCierreInlines
            ]

    def productor_cooperativa(self, obj):
        return obj.productor.cooperativa
    productor_cooperativa.short_description = 'Cooperativa'
    productor_cooperativa.admin_order_field = 'productor__cooperativa'

admin.site.register(FichaCierre,FichaCierreAdmin)
