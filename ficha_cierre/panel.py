# -*- coding: utf-8 -*-
from django.shortcuts import render, render_to_response
from django.contrib.auth.decorators import login_required
from django.db.models import Avg, Sum, Count,  Value as V, Case, When, IntegerField, Q
from .models import *
from productores.models import (Productor, CHOICES_ESCOLARIDAD,
                                                            CHOICES_LEGALIDAD)
from catalogos.models import *
from lugar.models import Departamento, Municipio, Comunidad
from cooperativas.models import Cooperativa


@login_required
def panel_cierre(request, template="cierre/panel_cierre.html"):
    ''' panel de linea base '''
    fichas = FichaCierre.objects.all().distinct('productor__id')
    total = fichas.count()
    hombres = fichas.filter(productor__sexo=2).count()
    mujeres = fichas.filter(productor__sexo=1).count()
    
    return render(request,template,locals())
