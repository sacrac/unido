from django.db import models
from django.contrib.auth.models import User
from multiselectfield import MultiSelectField
from ficha.models import Tecnico
from productores.models import Productor
from catalogos.models import ProductosFinca, ProductosParcela
from .constant import *

# Create your models here.

class FichaCierre(models.Model):
    
    productor = models.ForeignKey(Productor, verbose_name='Productor/a', on_delete=models.CASCADE)
    tecnico = models.ForeignKey(Tecnico, verbose_name='Técnico', on_delete=models.CASCADE)
    fecha = models.DateField('Fecha de recolección')
    anio = models.IntegerField('Año', choices=CHOICES_ANIO)
    parcela = models.IntegerField('Tipo de parcela', choices=CHOICES_TIPO_PARCELAS)
    estado_parcela = models.IntegerField('Estado de parcela', choices=CHOICES_ESTADO_PARCELAS, null=True)
    edad = models.FloatField('Edad de la parcela en año', null=True)

    year = models.IntegerField(editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.year = self.fecha.year
        super(FichaCierre, self).save(*args, **kwargs)

    def __str__(self):
        return self.productor.nombre

    class Meta:
        verbose_name = 'Ficha cierre'
        verbose_name_plural = 'Ficha cierre'


class CierreManejo1(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_1_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_1_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_1_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_1_RESULTADOS,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre, on_delete=models.CASCADE)

    def __str__(self):
        return u"1.1"

    class Meta:
        verbose_name='1.1 Manejo de sombra'
        verbose_name_plural='1.1 Manejo de Sombra'


class CierreManejo2(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_2_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_2_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_2_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_2_RESULTADOS,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre, on_delete=models.CASCADE)

    def __str__(self):
        return u"1.2"

    class Meta:
        verbose_name='1.2 Manejo de poda'
        verbose_name_plural='1.2 Manejo de poda'


class CierreManejo3(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_3_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_3_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_3_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_3_RESULTADOS,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre, on_delete=models.CASCADE)

    def __str__(self):
        return u"1.3"

    class Meta:
        verbose_name='1.3 Manejo de suelo y fertilidad'
        verbose_name_plural='1.3 Manejo de suelo y fertilidad'


class CierreManejo4(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_4_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_4_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_4_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_4_RESULTADOS,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre, on_delete=models.CASCADE)

    def __str__(self):
        return u"1.4"

    class Meta:
        verbose_name='1.4 Manejo de plagas y enfermedades'
        verbose_name_plural='1.4 Manejo de plagas y enfermedades'


class CierreManejo5(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_5_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_5_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_5_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_5_RESULTADOS,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre, on_delete=models.CASCADE)

    def __str__(self):
        return u"1.5"

    class Meta:
        verbose_name='1.5 Manejo de piso'
        verbose_name_plural='1.5 Manejo de piso'


class CierreManejo6(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_6_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_6_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_6_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_6_RESULTADOS,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre, on_delete=models.CASCADE)

    def __str__(self):
        return u"1.6"

    class Meta:
        verbose_name='1.6 Manejo de vivero'
        verbose_name_plural='1.6 Manejo de vivero'


class CierreManejo7(models.Model):
    campo1 = MultiSelectField(choices=CHOICE_CIERRE_1_7_IMPACTO,
                                verbose_name='Observación que impacto')
    campo2 = MultiSelectField(choices=CHOICE_CIERRE_1_7_PLANIFICADA,
                                verbose_name='Acciones planificadas')
    campo3 = MultiSelectField(choices=CHOICE_CIERRE_1_7_REALIZADA,
                                verbose_name='Acciones realizadas')
    campo4 = MultiSelectField(choices=CHOICE_CIERRE_1_7_RESULTADO,
                                verbose_name='Resultados obtenidos', null=True, blank=True)

    ficha = models.ForeignKey(FichaCierre, on_delete=models.CASCADE)

    def __str__(self):
        return u"1.7"

    class Meta:
        verbose_name='1.7 Cosecha'
        verbose_name_plural='1.7 Cosecha'


class CierreCosto1(models.Model):
    area = models.FloatField('Área de parcela SAF cacao en Mz')
    area_desarrollo = models.FloatField('Área en desarrollo en Mz', default=0)
    area_produccion = models.FloatField('Área en producción en Mz', default=0)
    costo = models.FloatField('Costo de mano de obra por día')
    
    ficha = models.ForeignKey(FichaCierre, on_delete=models.CASCADE)

    def __str__(self):
        return u""

    class Meta:
        verbose_name_plural = 'Costos e ingreso de la parcela SAF '


class CostoCacaoCierre(models.Model):
    meses = MultiSelectField(choices=CHOICE_MESES)
    actividades = models.IntegerField(choices=CHOICE_COSTO_CACAO_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_contratada = models.FloatField('Uso de MO contratada permanente DP')
    mo_temporal = models.FloatField('Uso de MO contratada temporal DP')
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos C$')

    ficha = models.ForeignKey(FichaCierre,
                            on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Costos de cacao'


class CostoMusaceasCierre(models.Model):
    meses = MultiSelectField(choices=CHOICE_MESES)
    actividades = models.IntegerField(choices=CHOICE_COSTO_MUSACEAS_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_contratada = models.FloatField('Uso de MO contratada permanente DP')
    mo_temporal = models.FloatField('Uso de MO contratada temporal DP')
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos C$')

    ficha = models.ForeignKey(FichaCierre,
                            on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Costos de musaceas'


class CostoFrutalesCierre(models.Model):
    meses = MultiSelectField(choices=CHOICE_MESES)
    actividades = models.IntegerField(choices=CHOICE_COSTO_FRUTALES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_contratada = models.FloatField('Uso de MO contratada permanente DP')
    mo_temporal = models.FloatField('Uso de MO contratada temporal DP')
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos C$')

    ficha = models.ForeignKey(FichaCierre,
                            on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Costos de frutales'


class CostoMaderablesCierre(models.Model):
    meses = MultiSelectField(choices=CHOICE_MESES)
    actividades = models.IntegerField(choices=CHOICE_COSTO_MADERABLES_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_contratada = models.FloatField('Uso de MO contratada permanente DP')
    mo_temporal = models.FloatField('Uso de MO contratada temporal DP')
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos C$')

    ficha = models.ForeignKey(FichaCierre,
                            on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Costos de maderables'


class CostoArbolesCierre(models.Model):
    meses = MultiSelectField(choices=CHOICE_MESES)
    actividades = models.IntegerField(choices=CHOICE_COSTO_ARBOLES_SERVICIO_ACTIVIDAD)
    mo_familiar = models.FloatField('Uso de MO familiar DP')
    mo_contratada = models.FloatField('Uso de MO contratada permanente DP')
    mo_temporal = models.FloatField('Uso de MO contratada temporal DP')
    insumos = models.CharField(max_length=250, null=True, blank=True)
    costo_insumo = models.FloatField('Costo insumos C$')

    ficha = models.ForeignKey(FichaCierre,
                            on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Costos de arboles servicio'


class DatosCosechaParcelaCierre(models.Model):
    mes = MultiSelectField(choices=CHOICE_MESES)
    productos = models.ForeignKey(ProductosParcela, on_delete=models.CASCADE)
    produccion = models.FloatField('Producción')
    venta = models.FloatField()
    precio = models.FloatField('Precio/Unidad')
    destino = MultiSelectField('Destino',
                            choices=CHOICE_COMPRADOR_DATOS)

    ficha = models.ForeignKey(FichaCierre, on_delete=models.CASCADE)
    total = models.FloatField(editable=False, null=True, blank=True)
    total_produccion = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.total = self.venta * self.precio
        self.total_produccion = self.produccion * self.precio
        super(DatosCosechaParcelaCierre, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos de cosecha de la parcela SAF'

class DatosCosechaFincaCierre(models.Model):
    mes = MultiSelectField(choices=CHOICE_MESES)
    productos = models.ForeignKey(ProductosFinca, on_delete=models.CASCADE)
    produccion = models.FloatField('Producción')
    venta = models.FloatField()
    precio = models.FloatField('Precio/Unidad')
    destino = MultiSelectField('Destino',
                            choices=CHOICE_COMPRADOR_DATOS)

    ficha = models.ForeignKey(FichaCierre, on_delete=models.CASCADE)
    total = models.FloatField(editable=False, null=True, blank=True)
    total_produccion = models.FloatField(editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.total = self.venta * self.precio
        self.total_produccion = self.produccion * self.precio
        super(DatosCosechaFincaCierre, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Datos de cosecha de la finca'


class CicloTrabajoCierre(models.Model):
    pregunta1 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA,
                        verbose_name='¿Las visitas que hemos realizado, cómo las valora para aprender nuevas cosas?')
    pregunta2 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA,
                        verbose_name='¿Las visitas que hemos realizado cómo las valora en la observación sobre diferentes aspectos de la parcela de cacao?')
    pregunta3 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO1_RESPUESTA,
                        verbose_name='¿Las observaciones y discusiones cómo valora su utilidad para mejorar el manejo de la parcela de cacao?')
    pregunta4 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO2_RESPUESTA,
                        verbose_name='¿Han podido implementar las acciones que se acordaron a partir de las visitas?')
    pregunta5 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO3_RESPUESTA,
                        verbose_name='¿Qué piensa sobre la frecuencia de las visitas?')
    pregunta6 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO4_RESPUESTA,
                        verbose_name='¿Qué piensa sobre el tiempo que dura cada visita?')
    pregunta7 = models.IntegerField(choices=CHOICE_CIERRE_CICLO_TRABAJO5_RESPUESTA,
                        verbose_name='¿Quiere seguir trabajando con las visitas para el segundo ciclo?')
    pregunta8 = models.IntegerField(choices=((1,'Si'),(2,'No'),),
                        verbose_name='¿Estaría usted interesado organizar un día de campo en su finca para que otras y otros productores vengan a visitar la parcela?')

    ficha = models.ForeignKey(FichaCierre, on_delete=models.CASCADE)

    def __str__(self):
        return u"Cierre ciclo de trabajo"


