from django.conf.urls import url, re_path
from .views import *
from .panel import panel_cierre

urlpatterns =  [
    
    re_path(r'^panel/$', panel_cierre, name='panel_cierre'),
    re_path(r'^consulta-cierre/$', consulta_cierre, name='consulta_cierre'),
    #re_path(r'^api/productor/', get_productor, name='productor-search'),
    #re_path(r'^ajax/load-departamentos/', load_departamentos, name='ajax_load_departamentos'),
    #re_path(r'^ajax/load-municipios/', load_municipios, name='ajax_load_municipios'),
    #re_path(r'^ajax/load-comunidad/', load_comunidades, name='ajax_load_comunidad'),
    #re_path(r'^mapaproductores/', obtener_lista_mapa, name='obtener-lista-mapa'),
    re_path(r'^(?P<vista>\w+)/$', get_view_cierre, name='get-view-cierre'),
]