# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.contrib.auth.models import User

from lugar.models import Pais, Departamento, Municipio, Comunidad
from cooperativas.models import Cooperativa
# Create your models here.

CHOICES_ESCOLARIDAD = (
                    (1, 'No sabe leer'),
                    (2, 'Alfabetizado'),
                    (3, 'Primaria Incompleta'),
                    (4, 'Primaria completa'),
                    (5, 'Secundaria incompleta'),
                    (6, 'Secundaria completa'),
                    (7, 'Técnico'),
                    (8, 'Universitario'),
    )

CHOICE_SEXO = (
                    (1, 'Mujer'),
                    (2, 'Hombre'),
    )

CHOICES_LEGALIDAD = (
                    (1, 'Propia con escritura'),
                    (2, 'Propia con título de reforma agraria'),
                    (3, 'Propia Sin documento'),
                    (4, 'Parcela en tierra comunitaria'),
                    (5, 'Arrendada '),
                    (6, 'Sin Documentos'),
    )

CHOICES_ACTIVO = (
                    (1, 'Activo proyecto Procacao2'),
                    (2, 'Inactivo proyecto Procacao2'),

    )

@python_2_unicode_compatible
class Productor(models.Model):
    nombre = models.CharField('Nombre dueño/a de la finca', max_length=250)
    cedula = models.CharField('Número de cedula', max_length=150, null=True, blank=True)
    escalidad = models.IntegerField('Nivel de escolaridad', choices=CHOICES_ESCOLARIDAD)
    nombre_finca = models.CharField('Nombre de la finca', max_length=250, null=True, blank=True)
    legalidad = models.IntegerField(choices=CHOICES_LEGALIDAD, verbose_name='Legalidad de la propiedad')
    sexo = models.IntegerField(choices=CHOICE_SEXO)
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE,
                                    verbose_name='Departamento/Provincia')
    municipio = models.ForeignKey(Municipio, on_delete=models.CASCADE)
    comunidad = models.ForeignKey(Comunidad, on_delete=models.CASCADE, null=True, blank=True)
    latitud = models.FloatField(null=True, blank=True)
    longitud = models.FloatField(null= True, blank=True)
    altitud = models.FloatField('Altitud de la finca', null=True,blank=True)
    cooperativa = models.ForeignKey(Cooperativa, on_delete=models.CASCADE, null=True, blank=True)
    estatus_productor = models.IntegerField(choices=CHOICES_ACTIVO, null=True, blank=True)
    # maneja = models.IntegerField(choices=CHOICE_MANEJA_FINCA,
    #                                                       verbose_name='Quién maneja la finca')
    # proyectos = models.ManyToManyField(Proyectos, verbose_name='Proyetos donde es beneficiario')
    # organizacion_pertenece = models.ForeignKey('Organizacion', on_delete=models.CASCADE,
    #                                                                             verbose_name='Organización que pertence productor',
    #                                                                             null=True, blank=True)
    # desde_cuando = models.IntegerField(choices=CHOICE_DESDE_CUANDO, null=True, blank=True)
    # organizacion_apoyo = models.ForeignKey(OrganizacionApoyo, on_delete=models.CASCADE,
    #                                                                             verbose_name='Organización de apoyo',
    #                                                                             null=True, blank=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Productor/a'
        verbose_name_plural = 'Productores/as'
        ordering = ['nombre']
        unique_together = ('nombre',)


class UserExtend(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    cooperativa = models.ForeignKey(Cooperativa, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        if self.cooperativa:
            return self.cooperativa.nombre
        return self.user.username

    class Meta:
        verbose_name = 'Cooperativa del usuario'
        verbose_name_plural = 'cooperativas de los usuarios'
