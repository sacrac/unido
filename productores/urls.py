from django.conf.urls import re_path
from .views import *

urlpatterns =  [
    re_path(r'^entrevistados-autocomplete/',
        EntrevistadosAutocomplete.as_view(),
        name='entrevistados-autocomplete',),
    re_path(
        r'^departamento-autocomplete/',
        DepartamentoAutocomplete.as_view(create_field='nombre'),
        name='depart-autocomplete',
    ),
    re_path(
        r'^municipio-autocomplete/',
        MunicipioAutocomplete.as_view(create_field='nombre'),
        name='municipio-autocomplete',
    ),
    re_path(
        r'^comunidad-autocomplete/',
        ComunidadAutocomplete.as_view(create_field='nombre'),
        name='comunidad-autocomplete',
    ),
]
