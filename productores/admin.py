from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import Productor, UserExtend
from .forms import ProductorForm

import import_export
from import_export import resources
from import_export.fields import Field
from import_export.admin import ImportExportModelAdmin


class UserExtendInline(admin.StackedInline):
    model = UserExtend
    can_delete = False
    verbose_name_plural = 'Cooperativa a la que pertenece'

class UserAdmin(BaseUserAdmin):
    inlines = (UserExtendInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

class ProductorResource(resources.ModelResource):
    _choice_fields = [
    'escalidad','legalidad','sexo','estatus_productor'
    ]
    for _field_name in _choice_fields:
        locals()[_field_name] = import_export.fields.Field(
            attribute='get_%s_display' % _field_name,
            column_name=Productor._meta.get_field(_field_name).verbose_name
        )
    pais = Field(attribute='pais', column_name='pais')
    departamento = Field(attribute='departamento', column_name='departamento')
    municipio = Field(attribute='municipio', column_name='municipio')
    comunidad = Field(attribute='comunidad', column_name='comunidad')
    cooperativa = Field(attribute='cooperativa', column_name='cooperativa')

    class Meta:
        model = Productor
        fields = ('id', 'nombre', 'apellido','cedula',
                   'escalidad','nombre_finca','legalidad',
                   'sexo','pais','departamento','municipio',
                   'comunidad','latitud','longitud',
                   'altitud','cooperativa','estatus_productor')

class ProductorAdmin(ImportExportModelAdmin):
    form = ProductorForm
    resource_class = ProductorResource
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super(ProductorAdmin, self).save_model(request, obj, form, change)

    # def get_queryset(self, request):
    #     qs = super(ProductorAdmin, self).get_queryset(request)
    #     if request.user.is_superuser:
    #         return qs
    #     return qs.filter(user=request.user)

    exclude = ('user', )
    list_display = ('nombre', 'sexo', 'pais', 'cooperativa', 'user')
    list_filter = ('pais', 'departamento', 'municipio', 'comunidad')
    search_fields = ['nombre']

admin.site.register(Productor, ProductorAdmin)
