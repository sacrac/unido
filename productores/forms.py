# -*- coding: utf-8 -*-
from django import forms

from .models import Productor
from lugar.models import (Pais, Departamento,
                                                 Municipio, Comunidad)

from dal import autocomplete


class ProductorForm(forms.ModelForm):

    class Meta:
        model = Productor
        fields = '__all__'
        widgets = {
                'departamento': autocomplete.ModelSelect2(url='depart-autocomplete',
                                                           forward=['pais']),
                 'municipio': autocomplete.ModelSelect2(url='municipio-autocomplete',
                                                           forward=['departamento']),
                 'comunidad': autocomplete.ModelSelect2(url='comunidad-autocomplete',
                                                           forward=['municipio']),
            }
