from django.shortcuts import render

from .models import Productor
from lugar.models import Pais, Departamento, Municipio, Comunidad

from dal import autocomplete
# Create your views here.

class EntrevistadosAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return productor.objects.none()

        qs = productor.objects.all()

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

class DepartamentoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
           return Departamento.objects.none()

        qs = Departamento.objects.all()

        pais = self.forwarded.get('pais', None)

        if pais:
            qs = qs.filter(pais=pais)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

class MunicipioAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
           return Municipio.objects.none()

        qs = Municipio.objects.all()

        departamento = self.forwarded.get('departamento', None)

        if departamento:
            qs = qs.filter(departamento=departamento)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

class ComunidadAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
           return Comunidad.objects.none()

        qs = Comunidad.objects.all()

        municipio = self.forwarded.get('municipio', None)

        if municipio:
            qs = qs.filter(municipio=municipio)

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs
