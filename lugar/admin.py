# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

# Register your models here.
class PaisAdmin(admin.ModelAdmin):
    list_display = ['nombre']
    list_filter = ['nombre']
    search_fields = ['nombre']
    ordering = ['nombre']

class DepartamentoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'get_pais']
    list_filter = ['pais']
    search_fields = ['nombre']
    autocomplete_fields = ['pais']

    def get_pais(self, obj):
        return obj.pais.nombre
    get_pais.short_description = 'Pais'
    get_pais.admin_order_field = 'pais__nombre'

class MunicipioAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'departamento', 'get_pais']
    list_filter = ['departamento', 'departamento__pais__nombre']
    search_fields = ['nombre']
    ordering = ['nombre']
    autocomplete_fields = ['departamento']

    def get_pais(self, obj):
        return obj.departamento.pais.nombre
    get_pais.short_description = 'Pais'
    get_pais.admin_order_field = 'departamento__pais__nombre'

class ComunidadAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'municipio','get_departamento','get_pais']
    list_filter = ['municipio','municipio__departamento__pais__nombre']
    search_fields = ['nombre']
    autocomplete_fields = ['municipio']
    ordering = ['nombre']

    def get_departamento(self, obj):
        return obj.municipio.departamento.nombre
    get_departamento.short_description = 'Departamento'
    get_departamento.admin_order_field = 'municipio__departamento__nombre'

    def get_pais(self, obj):
        return obj.municipio.departamento.pais.nombre
    get_pais.short_description = 'Pais'
    get_pais.admin_order_field = 'municipio__departamento__pais__nombre'

admin.site.register(Pais, PaisAdmin)
admin.site.register(Departamento, DepartamentoAdmin)
admin.site.register(Municipio, MunicipioAdmin)
admin.site.register(Comunidad, ComunidadAdmin)
